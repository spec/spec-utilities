static char RcsId[] = "$Header: /segfs/bliss/source/spec/utilities/SPEC_utils/mac2doc/RCS/mac2doc.c,v 1.3 2018/02/09 11:47:17 witsch Exp $";
/***********************************************************************
 *
 *    File:         mac2doc.c
 *
 *    Project:      Self documenting macros
 *
 *    Description:  C-code
 *
 *    Author:       Jorg Klora ( upgraded V.Rey )
 *
 ***********************************************************************/
/*
 *    $Log: mac2doc.c,v $
 *    Revision 1.3  2018/02/09 11:47:17  witsch
 *    get rid of the old bug, where a input filename without "/"
 *    caused a core dump.
 *
 *    Revision 1.2  2014/04/28 11:49:38  witsch
 *    Comments by R. Butzbach:
 *
 *    11/04/2014 Randolf Butzbach
 *
 *       -- Code rearranged to produce the default
 *          lex.yy.c / y.tab.c / y.tab.h files;
 *          makes it easier to follow the various tutorials on lex and yacc.
 *
 *       -- Added stripws() : Whitespaces in the first column create an
 *          implicit line break in troff / manpage output.
 *
 *       -- Fixed: A periode after a word in bold font must appear on the
 *          same line as the bold word -- but will not printed in bold:
 *
 *          This is done by a Q&D hack in order to cope with the present coding.
 *          The hack introduces a new token BOLDP, extracts the
 *          the punctation submitted after "%B%" and calls then the change_state()
 *          function for BOLD but with the punctation submitted as string.
 *
 *           /*
 *              The proper way would have been something like this:
 *              Define a token PUNCT as [.,:;!?]+  and rules as:
 *
 *              BOLD WORD BOLD:       printf(".IR %s\n",$2)
 *              BOLD WORD BOLD PUNCT: printf(".IR %s %s\n",$2,$4)
 *
 *              But this would have required major changes in macparse.y
 *           * /
 *
 *
 *       -- Added ".tr @@" in the troff / manpage output at the beginning.
 *
 *          When reading the generated help pages with man, the "@" was
 *          shown properly. However, when reading them with chelp, the "@"
 *          got converted to a """.
 *
 *          For the sake of convenience of entering double quotes in the SPEC
 *          help files, SPEC converts a "@" to a """ by the troff directive
 *
 *             .tr @"
 *
 *          From the SPEC manual:
 *          http://www.certif.com/spec_help/help_fmt.html
 *
 *                The @ symbol is used in the help file sources to represent
 *                literal double quote characters, which makes it easier
 *                to insert literal double quote characters into formatting
 *                command arguments. To enter a literal @ use both a backslash
 *                and the troff .tr directive, as in:
 *
 *                    .tr @@
 *                    a literal \@ appears
 *                    .tr @"
 *
 *          The applied change undoes this convertion, which might break
 *          the help output. Howver, this is rather unlikely, as folks you
 *          wrote the macro documentation must have been aware that a @
 *          converts to a ".
 *
 *          So, if it breakes the help output, remove the string
 *          ".tr @@" (w/o quotes) from the line
 *
 *                  out(" </H1> \n",".tr @@\n");
 *
 *          in macparse.y and/or do it as documented in the SPEC manual (cf above).
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include "mac2doc.h"

int readarg();
int anaopt();

static ANAOPTSTRUCT gos = {1,"\0",0};
ARGSTRUCT as;

/*
 * Main
 */
int main(argc , argv )
    int  argc;
    char *argv[];
{
    FILE *ifd,*ofd;

    /*
     * defaults
     */
    as.flag      = 0;
    as.ifile[0]  = '\0';
    as.sfile[0]  = '\0';
    as.ofile[0]  = '\0';
    as.mac[0]    = '\0';
    as.lineno    = 0;

    namestr = 0;
    bodystr = 0;

    /*
     * Get default options from name
     */

    if (strcmp(argv[0],"mac2man") == 0)
    as.flag = 0x0001;

    if (strcmp(argv[0],"mac2html") == 0)
    as.flag = 0x0000;

    if (readarg (argc,argv,&as) == TRUE) {
    /* Error or help in command line */
    fprintf (stderr, "Usage: %s [-mh] \n",argv[0]);
    exit(-1);
    }

    /* copy infile name, but if there is no slash, this dumps core */
    if (!strrchr(as.ifile,'/')) {
        strcpy(as.bfile,as.ifile);
    } else {
        strcpy(as.bfile,strrchr(as.ifile,'/')+1);
    }

    /*
     * Open the files
     */
    if ((as.flag&IFILE)&&((ifd = freopen(as.ifile,"r",stdin))==(FILE *)NULL)) {
    fprintf (stderr, "Could not open %s for reading\n",as.ifile);
    exit(-1);
    }

    if ((as.flag&OFILE)&&((ofd = freopen(as.ofile,"w",stdout))==(FILE *)NULL)) {
    fprintf (stderr, "Could not open %s for writing\n",as.ofile);
    exit(-1);
    }

    as.sfd = (FILE*) NULL;
    if ((as.flag&SFILE)&&((as.sfd = fopen(as.sfile,"w"))==(FILE *) NULL)) {
    fprintf (stderr, "Could not open %s for writing\n",as.sfile);
    exit(-1);
    }

    /*
     * Call yacc
     */
    yyparse();

    /*
     * Close files
     */
    if (as.flag&IFILE) fclose(ifd);
    if (as.flag&OFILE) fclose(ofd);
    if (as.flag&SFILE) fclose(as.sfd);
    exit(0);
}

static  char   *letP = NULL;    /* remember next option char's location */
static  char    SW = '-';   /* switch character*/

/*
   Parse the command line options, System V style.

   Standard option syntax is:

   option ::= - [optLetter]* [argLetter space* argument]

   where
   - there is no space before any optLetter or argLetter.
   - opt/arg letters are alphabetic, not punctuation characters.
   - optLetters, if present, must be matched in optionS.
   - argLetters, if present, are found in optionS followed by ':'.
   - argument is any white-space delimited string.  Note that it
   can include the SW character.
   - upper and lower case letters are distinct.

   There may be multiple option clusters on a command line, each
   beginning with a SW, but all must appear before any non-option
   arguments (arguments not introduced by SW).  Opt/arg letters may
   be repeated: it is up to the caller to decide if that is an error.

   The character SW appearing alone as the last argument is an error.
   The lead-in sequence SWSW ("--" ) causes itself and all the
   rest of the line to be ignored (allowing non-options which begin
   with the switch char).

   The string *optionS allows valid opt/arg letters to be recognized.
   argLetters are followed with ':'.  Anaopt () returns the value of
   the option character found, or EOF if no more options are in the
   command line.     If option is an argLetter then the global optarg is
   set to point to the argument string (having skipped any white-space).

   The global optind is initially 1 and is always left as the index
   of the next argument of argv[] which anaopt has not taken.  Note
   that if "--" or "//" are used then optind is stepped to the next
   argument before anaopt() returns EOF.

   If an error occurs, that is an SW char precedes an unknown letter,
   then anaopt() will return a '?' character and normally prints an
   error message via perror().  If the global variable opterr is set
   to false (zero) before calling anaopt() then the error message is
   not printed.

   For example,

 *optionS == "A:F:PuU:wXZ:"

 then 'P', 'u', 'w', and 'X' are option letters and 'F', 'U', 'Z'
 are followed by arguments.  A valid command line may be:

 aCommand  -uPFPi -X -A L someFile

where:
- 'u' and 'P' will be returned as isolated option letters.
- 'F' will return with "Pi" as its argument string.
- 'X' is an isolated option.
- 'A' will return with "L" as its argument.
- "someFile" is not an option, and terminates anaopt.  The
caller may collect remaining arguments using argv pointers.
 */

int anaopt(argc, argv, optionS, pgos)
    int argc;
    char *argv[];
    char *optionS;
    ANAOPTSTRUCT *pgos;
{
    unsigned char ch;
    char *optP;

    if (argc > pgos->optind) {
    if (letP == NULL) {
        if ((letP = argv[pgos->optind]) == NULL ||
            *(letP++) != SW)  goto gopEOF;
        if (*letP == SW) {
        (pgos->optind)++;  goto gopEOF;
        }
    }
    if (0 == (ch = *(letP++))) {
        pgos->optind++;  goto gopEOF;
    }
    if (':' == ch  ||  (optP = strchr(optionS, ch)) == NULL)
        goto gopError;
    if (':' == *(++optP)) {
        pgos->optind++;
        if (0 == *letP) {
        if (argc <= pgos->optind)  goto  gopError;
        letP = argv[pgos->optind++];
        }
        pgos->optarg = letP;
        letP = NULL;
    } else {
        if (0 == *letP) {
        (pgos->optind)++;
        letP = NULL;
        }
        pgos->optarg = NULL;
    }
    return ch;
    }
gopEOF:
    pgos->optarg = letP = NULL;
    return EOF;

gopError:
    pgos->optarg = NULL;
    if (pgos->opterr)
    perror ("get command line option");
    return ('?');
}

int readarg (argc, argv, as)
    int argc;
    char * argv[];
    ARGSTRUCT *as;
{
    int option;

    while ((option = anaopt(argc,argv,"o:i:s:mhed",&gos)) != EOF) {
    switch (option) { /*Put flag option with parameter in memory*/
        case 'e':
        as->flag |= IGNERR;
        break;
        case 'd':
        as->flag |= DFLAG;
        break;
        case 'm':
        as->flag |= MFLAG;
        break;
        case 'h':
        as->flag &= ~MFLAG;
        break;
        case 'i':
        as->flag |= IFILE;
        strcpy(as->ifile,gos.optarg);
        break;
        case 'o':
        as->flag |= OFILE;
        strcpy(as->ofile,gos.optarg);
        break;
        case 's':
        as->flag |= SFILE;
        strcpy(as->sfile,gos.optarg);
        break;
        default:
        return (TRUE);
    } /* End switch option */
    } /* End while option not equal EOF */
    if (gos.optind < argc) {
    as->flag |= IFILE;
    strcpy(as->ifile,argv[gos.optind++]);
    }
    if (gos.optind < argc) {
    as->flag |= OFILE;
    strcpy(as->ofile,argv[gos.optind++]);
    }
    if (gos.optind < argc) {
    as->flag |= SFILE;
    strcpy(as->sfile,argv[gos.optind]);
    }
    return (FALSE);
} /* end function readargs */


int yyerror(s)
    char *s;
{
    if (! (as.flag&IGNERR)) {
    fprintf(stderr,"%s\n",s);
    fprintf(stderr,"Near Comment line: %d (Macro %s)\n",as.lineno,as.mac);
    }
}
