/* static char RcsID[]= $Header: /segfs/bliss/source/spec/utilities/SPEC_utils/mac2doc/RCS/mac2doc.h,v 1.2 2014/04/28 11:54:27 witsch Exp $; */
/***************************************************************************
 *
 *  File:            mac2doc.h
 *
 *  Description:     Include file for mac2doc project
 *
 *  Author:          Jorg Klora (upgraded Vicente Rey)
 *
 *  $Date: 2014/04/28 11:54:27 $
 *
 ***************************************************************************/
/*
 *  $Log: mac2doc.h,v $
 *  Revision 1.2  2014/04/28 11:54:27  witsch
 *  Changes by R. Butzbach
 *
 */
#ifndef MAC2DOC_H
#define MAC2DOC_H

#define MAXLLEN       400
#define MAXLINES     2000
#define MAXELEMENTS    50
#define MAXMOT        100
#define MAXNAME        30
#define MAXPERLINE      6

#define TRUE         1
#define FALSE        0

#define MFLAG    1
#define IFILE    2
#define OFILE    4
#define SFILE    8
#define DFLAG   16
#define IGNERR  32

typedef struct {
  int   optind;       /* index of which argument is next    */
  char   *optarg;     /* pointer to argument of current option */
  int   opterr;       /* allow error message    */
} ANAOPTSTRUCT;

typedef struct {
  unsigned int  flag;
  char ifile[MAXLLEN];
  char ofile[MAXLLEN];
  char sfile[MAXLLEN];
  char mac[MAXLLEN];
  char bfile[MAXLLEN];
  int lineno;
  FILE *sfd;
} ARGSTRUCT;

char *bodystr;
char *namestr;

int yylex (void);
int yyparse (void);
int yyerror(char *);
int header(char *);

#endif   /*  MAC2DOC_H  */
