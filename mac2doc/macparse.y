/* static RcsId[]= $Header: /segfs/bliss/source/spec/utilities/SPEC_utils/mac2doc/RCS/macparse.y,v 1.3 2015/07/15 14:15:06 guilloud Exp witsch $
/***************************************************************************
 *
 *  File:            macparse.y
 *
 *  Description:     yacc file, parser, for mac2doc project
 *
 *  Author:          Jorg Klora ( upgraded Vicente Rey, Randolf Butzbach )
 *
 *  $Date: 2015/07/15 14:15:06 $
 *
 ***************************************************************************/
/*
 *  $Log: macparse.y,v $
 *  Revision 1.3  2015/07/15 14:15:06  guilloud
 *  added a newline marker to be under 80 cols. (C.G.)
 *
 *  Revision 1.2  2014/04/28 11:54:27  witsch
 *  Changes by R. Butzbach
 *
 */

%{
/*
 * Include
 */
#include <stdio.h>
#include <string.h>
#include "mac2doc.h"
#include "y.tab.h"

/*
 * Define
 */
#define BASEDIR      "//www.esrf.fr/computing/bliss"
#define MANPAGE      (as.flag&MFLAG)
#define IFILENAME    (as.ifile)
#define BFILENAME    (as.bfile)
#define OFILENAME    (as.ofile)
#define FILENAME     (as.sfile)
#define SFD          (as.sfd)

/*
 * Variable declaration.
 *    - init all to 0
 */
static  int     oldstate=0,
                state=0,
                start=0;
static  char    buf[10000]={'\0'};
static  char   *mname[10000],
               *mdesc[10000],
               *musage[10000];
static  char    tocbuf[10000]={'\0'};
/* static  char *bodystr=0, *namestr=0; */
static  int     mflag[10000];
static  int     mno=0;
static  int     hl=0,
                pmode=0,
                attr=0,
                done=1;

extern ARGSTRUCT as;

/*
 * Function declaration
 */
int    change_state();
char  *filter();
char  *stripws();
int    state_print();
int    out();

%}

%union {
  int cmd;
  char *string;
}

%type  <cmd>    onecmd change
%token <cmd>    DEF NAME DESC END BREAK INTERN USER TOC MACRO MDESC
%token <cmd>    BOLD AUTHOR EXAMPLE BUGS ATTENTION TITLE OVIEW
%token <cmd>    SETUP INTER DEP OPENB CLOSEB KOMMA LESS OPENQ CLOSEQ
%token <cmd>    IMACRO MDL MDT MDD MXDL MUL MLI MXUL PRE
%token <string> COMMENT BODY WORD LITERAL

%%
defs:
 | defs def
 | defs WORD
 ;
 | defs COMMENT
 { change_state(oldstate,state,filter($2),0); }
 | defs change {
      oldstate = state;
      state    = $2 ;
      change_state(oldstate,state,(char*)NULL,1);
      oldstate = state;
  }
 | defs onecmd {
      change_state($2,state,(char*)NULL,0);
  }
 | defs LITERAL {
      change_state(LITERAL,state,$2,0);
   }
 | defs BODY {
      change_state(oldstate,state,bodystr,0);
  };

change: NAME |
        DESC |
        END |
        INTERN |
        USER |
        MDESC |
        ATTENTION |
        BUGS |
        AUTHOR |
        DEP |
        INTER |
        SETUP |
        EXAMPLE |
        OVIEW |
        TITLE
 {$$ = $1;}

onecmd: BREAK |
        TOC |
        MACRO |
        IMACRO |
        BOLD |
        MDL |
        MDT |
        MDD |
        MXDL |
        MUL |
        MLI |
        OPENQ |
        CLOSEQ |
        LESS |
        MXUL |
        PRE
 {$$ = $1;}

def:  defhead optargs
  {
      oldstate = state;
      state    = BODY ;
      change_state(oldstate,state,(char *)NULL,1);
      oldstate = state;
  }

defhead: DEF WORD
  {
      oldstate = state;
      state = DEF ;
      change_state(oldstate,state,namestr,1);
      oldstate = state;
  }

optargs:
 | args
  ;

args: OPENB arglist CLOSEB
  ;

arglist:
 ;
 | WORD
 | arglist KOMMA WORD
 ;

%%
/* include <lex.yy.c> */

int change_state(oldstate,state,str,changed)
int   oldstate,
      state,
      changed;
char *str;
{

  if (changed) {
    switch(oldstate) {

       case MDESC:
             mdesc[mno]=(char*)strdup(buf);
            *buf='\0';
            break;

       case TITLE:
            /* undo the .tr @" in chelp */
            out(" </H1> \n",".tr @@\n");
            /* out(" </H1> \n","\n"); */

            if (!MANPAGE) {
                printf(" <HR> \n");
                printf(" <FONT COLOR=\"Blue\">");
                printf(" [ ");
                printf(" <A HREF=\"#TOC\">Contents</A>");
                printf(" | ");
                printf(" <A HREF=\"http:%s/spec/local/%s\">Load</A>", \
                            BASEDIR,BFILENAME);
                printf(" | ");
                printf(" <A HREF=\"http:%s/spec/local/index.html\">Index</A>", \
                            BASEDIR);
                printf(" ]\n<BR><BR> ");
                printf(" </FONT> ");
            }
            break;

    }

    switch(state) {
       case NAME:
            if (!MANPAGE) {
                printf("\n<BR><A NAME=\"BRIEF\"></A>");
            } else {
                printf("\n.SH BRIEF\n");
            }
            break;

        case AUTHOR:
            header("AUTHOR");
            break;

        case BUGS:
            header("BUGS");
            break;

        case ATTENTION:
            header("ATTENTION");
            break;

        case OVIEW:
            header("OVERVIEW");
            break;

        case SETUP:
            header("SETUP");
            break;

        case INTER:
            header("INTERNALS");
            break;

        case DEP:
            header("DEPENDENCIES");
            break;

        case EXAMPLE:
            header("EXAMPLE");
            break;

        case TITLE:
            if (!MANPAGE) {
               printf("<BODY BGCOLOR=#ffffff TEXT=#000000 LINK=#0000EE VLINK=#551A8B ALINK=#FF0000>\n<UL>\n");
               printf("<TITLE> BLISS: Spec Local Man Pages: %s</TITLE>\n",BFILENAME);
               printf("<A HREF=http://www.esrf.fr>");
               printf("<IMG SRC=http:%s/gifs/ESRFlogo80.gif BORDER=0 ALIGN=RIGHT></A>\n",BASEDIR);
               printf("<H3>\n");
               printf("<FONT COLOR=#FF0000>\n");
               printf("Beamline Instrument Software Support\n");
               printf("</FONT>\n");
               printf("</H3>\n ");
               printf("<HR>\n");
            }
            if (SFD) {
               fprintf(SFD,"<BODY BGCOLOR=#ffffff TEXT=#000000 LINK=#0000EE VLINK=#551A8B ALINK=#FF0000>\n<UL>\n");
               fprintf(SFD,"<TITLE>BLISS: Spec Macros. Source of %s </TITLE>\n",BFILENAME);
               fprintf(SFD,"<A HREF=http://www.esrf.fr>");
               fprintf(SFD,"<IMG SRC=http:%s/gifs/ESRFlogo80.gif BORDER=0 ALIGN=RIGHT></A>\n",BASEDIR);
               fprintf(SFD,"<BR>\n");
               fprintf(SFD,"<H3>\n");
               fprintf(SFD,"<FONT COLOR=#FF0000>\n");
               fprintf(SFD,"Beamline Instrument Software Support\n");
               fprintf(SFD,"</FONT>\n");
               fprintf(SFD,"</H3>\n ");
               fprintf(SFD,"<HR>\n");
               fprintf(SFD,"Source code for: <A HREF=\"%s\">\n",OFILENAME);
               fprintf(SFD,"<EM>%s</EM></A><BR><UL>\n",BFILENAME);
            }

            out("\n<H1> ","\n.TH ");
            break;

        case DESC:
            header("DESCRIPTION");
            break;

        case DEF:
            mname[mno++] = (char*)strdup(str);
            if (SFD) {
               fprintf(SFD,"\n<BR><BR><LI><A NAME=\"%s\" HREF=\"#TOC\">" ,str);
               fprintf(SFD,"<B>%s</B></A>\n",str);
            }
            break;


    }
 } else {
    /* MANPAGE && changed to pre in html */
    if (MANPAGE && (pmode == PRE) && str && (*str=='\n'))
           state_print(oldstate,"<BR>","\n.br\n");

    switch (oldstate) {

        case INTERN:
            mflag[mno] = 0;
            if (*str != '\n')
                 musage[mno] = (char*)strdup(str);
            break;

        case USER:
            mflag[mno] = 1;
            if (*str != '\n')
                musage[mno] = (char*)strdup(str);
            break;

        case MDESC:
            strcat(buf,str);
            break;

        case TOC:
            if (SFD) {
               int ii;
               fprintf(SFD,"<UL>\n");
               fprintf(SFD,"<H2>\n");
               fprintf(SFD,"<a name=\"TOC\">Table of Contents</a>\n");
               fprintf(SFD,"</H2>\n</UL>\n");
               for (ii=0;ii<mno;ii++) {
                  fprintf(SFD,"<LI><a href=\"#%s\">%s</a>\n", \
                           mname[ii],mname[ii]);
               }
               fprintf(SFD,"</UL>\n");
               fprintf(SFD,"</UL>\n");
            }
            if (!MANPAGE) {
               printf("\n<H2>\n");
               printf("<A NAME=TOC HREF=\"#TOC\">TABLE OF CONTENTS</A>\n");
               printf("</H2>\n<UL>\n");
               printf("%s",tocbuf);
               printf("\n</UL>");
               printf("<BR><BR>\n");
               printf("\n<HR NOSHADE SIZE=2>\n");
               printf("<TABLE WIDTH=92%% BORDER=0>\n");
               printf("<TR>\n");
               printf("<TD WIDTH=50%%  VALIGN=top ALIGN=left>\n");
               printf("<FONT SIZE=-1>\n");
               printf("<A HREF=\"http:%s/bliss.html\">\n",BASEDIR);
               printf("<IMG SRC=\"http:%s/gifs/blisshome.gif\" ",BASEDIR);
               printf(" align=baseline border=0></A>\n"); //,BASEDIR);
               printf("</FONT>\n");
               printf("<TD WIDTH=50%%  ALIGN=right>\n");
               printf("<FONT SIZE=-1>\n");
               printf("<I>\n");
               printf("Comments to: ");
               printf("<A HREF=\"mailto:rey@esrf.fr\">rey@esrf.fr</A>\n");
               printf("</I>\n");
               printf("</FONT>\n");
               printf("</TR>\n");
               printf("</TABLE>\n");
            }
            break;

        case MACRO:
            header("MACROS");
            out("The following macros can be called by the user.\n<DL>",
                "The following macros can be called by the user.\n");
            {
             int ii;
             char tmp[500];
             for (ii=0;ii<mno;ii++) {
               if (mflag[ii]) {
                  sprintf(tmp,"<DT><B><a href=\"%s#%s\">",FILENAME,mname[ii]);
                  out(tmp,"\n.HP\n.C\n");
                  sprintf(tmp,"%s</a>",mname[ii]);
                  out(tmp,mname[ii]);
                  if (musage[ii]) out(musage[ii],musage[ii]);
                     out("</B>\n<DD>","\n.br\n");
                  if (mdesc[ii])
                     out(mdesc[ii],mdesc[ii]);
               }
             }
            }
            out("</UL>\n","");
            break;

        case IMACRO:
            header("INTERNAL MACROS");
            out("The following macros are used from other macros.\n",
                "The following macros are used from other macros.\n");
            out("", ".br\n");
            out("They are normally not called by the user.\n<DL>",
                "They are normally not called by the user.\n");
            {
              int ii;
              char tmp[500];
              for (ii=0;ii<mno;ii++) {
                 if (!mflag[ii]) {
                    sprintf(tmp,"<DT><B><a href=\"%s#%s\">",FILENAME,mname[ii]);
                    out(tmp,"\n.HP\n.C\n");
                    sprintf(tmp,"%s</a>",mname[ii]);
                    out(tmp,mname[ii]);
                    if (musage[ii]) out(musage[ii],musage[ii]);
                        out("</B>\n<DD>","\n.br\n");
                    if (mdesc[ii])
                        out(mdesc[ii],mdesc[ii]);
                 }
              }
            }
            out("</UL>\n","");
            break;

        case DESC:
        case NAME:
        case AUTHOR:
        case EXAMPLE:
        case BUGS:
        case SETUP:
        case ATTENTION:
        case INTER:
        case DEP:
        case OVIEW:
        case TITLE:
            out(str,str);
            break;

        case MUL:
            hl++;
            state_print(state," <UL>\n","");
            break;

        case MXUL:
            hl--;
            state_print(state,"\n</UL>\n","\n.PP\n");
            break;

        case MLI:
            {
              char tmp[50];
              sprintf(tmp,"\n.HP %d\n.C *\n",hl-1);
              state_print(state,"\n<LI>   ",tmp);
            }
            break;

        case MDL:
            hl++;
            state_print(state," <DL>\n","");
            break;

        case MXDL:
            hl--;
            state_print(state,"\n</DL>\n","\n.PP\n");
            break;

        case MDT:
            {
              char tmp[50];
              sprintf(tmp,"\n.HP %d\n.C\n",hl-1);
              state_print(state,"\n<DT><B> ",tmp);
            }
            break;

        case MDD:
            state_print(state," </B>\n<DD> ","\n.br\n");
            break;

        case BREAK:
            state_print(state,"<BR>\n","\n.br\n");
            break;

        case LITERAL:
            *(str+strlen(str)-3)='\0';
            state_print(state,str+3,"");
            break;

        case BOLD:

            if (attr == BOLD) {

                char buffh[10000];
                char buffm[10000];

                strcpy(buffm, " ");
                strcpy(buffh, "</B>");
                if (str != NULL) {
                    strcat(buffm, (char *)str);
                    strcat(buffh, (char *)str);
                }
                strcat(buffm, "\n");

                /* state_print(state," </B> ","\n"); */
                state_print(state, buffh, buffm);
                attr = 0;
            } else {
                state_print(state,"<B>","\n.IR ");
                attr = BOLD;
            }
            break;

        case PRE:
            if (pmode == PRE) {
                pmode = 0;
                state_print(state,"</PRE>","");
            } else {
                pmode = PRE;
                state_print(state,"<PRE>","");
            }
            break;

        case BODY:
            if (SFD) fprintf(SFD,"%s",str);
            break;

        case OPENQ:
            if (SFD) fprintf(SFD,"<PRE>");
            break;

        case CLOSEQ:
            if (SFD) fprintf(SFD,"</PRE>");
            break;

        case LESS:
            if (SFD) fprintf(SFD,"&#60;");
            break;
     }
  }
}

char *
filter (char * inp)
{
  char        *cptr,
              *optr;
  static char buff[10000];

  if (MANPAGE) return (inp);

  for (cptr=inp,optr=buff;*cptr;cptr++,optr++) {
      switch (*cptr) {
         case '>':
            strcpy (optr,"&gt;");
            optr +=3;
            break;

         case '<':
            strcpy (optr,"&lt;");
            optr +=3;
            break;
         default:
            *optr = *cptr;
      }
  }

  *optr = *cptr;

  return (buff);
}

char *
stripws (char * s)
{
   char cpy;
   char buf1[100000];
   static char buf2[100000];
   static char lc = '\n';
   unsigned int idx1, idx2;
   size_t size;


       /* strip whitespaces after \n which cause an unintended
          line break in troff */
       while ((lc == '\n') && *s && (*s == ' '))
             s++;

       /* Convert "\n" to '\n' */
       sprintf(buf1,"%s",s);

       /* Copy buf1 to buf2 but omit ' ' after '\n' */

       idx1 = 0;
       idx2 = 0;
       cpy  = 1; /* Flag, whether to copy the next char */

       while (buf1[idx1]) {

          if (!cpy && (buf1[idx1] != ' '))
              cpy = 1;

          if (cpy)
              buf2[idx2++] = buf1[idx1];

          if (buf1[idx1] == '\n')
              cpy = 0;

          idx1++;
       }
       buf2[idx2] = '\0';

       /* remember the last output char to check next time whether it was a '\n' */
       size = strlen(buf2);
       if (size)
          lc = *(buf2 + size - 1);

  return (buf2);
}

int
state_print (state,topr,topr2)
int state;
char *topr,*topr2;
{
    switch (state) {
      case MDESC:
        if (MANPAGE) {
             strcat (buf,topr2);
        } else {
             strcat (buf,topr);
        }
        break;

      default:
        out(topr,topr2);
        break;
    }
}

int
header(s)
char * s;
{
    if (MANPAGE) {
        printf("\n.SH %s\n",s);
    } else {
        printf("\n<H2><A NAME=%s HREF=\"#TOC\">%s</A></H2>\n",s,s);
        strcat(tocbuf,"<LI><A HREF=#");
        strcat(tocbuf,s);
        strcat(tocbuf," >");
        strcat(tocbuf,s);
        strcat(tocbuf," </A>");
    }
}

int
out (s1,s2)
char *s1,*s2;
{

    if (MANPAGE) {
       printf("%s", stripws(s2));
    } else {
       printf("%s",s1);
    }
}
