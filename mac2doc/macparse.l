/* static char RcsId[] = $Header: /segfs/bliss/source/spec/utilities/SPEC_utils/mac2doc/RCS/macparse.l,v 1.2 2014/04/28 11:54:27 witsch Exp $; */
/***************************************************************************
/*
/*  File:            macparse.l
/*
/*  Description:     lexical analyzer for mac2doc project
/*
/*  Author:          Jorg Klora ( upgraded Vicente Rey, Randolf Butzbach)
/*
/*  $Date: 2014/04/28 11:54:27 $
/*
/***************************************************************************/
/*
/*  $Log: macparse.l,v $
 *  Revision 1.2  2014/04/28 11:54:27  witsch
 *  Changes by R. Butzbach
 *
/*/

%{

#undef YYLMAX
#define YYLMAX 20000
#define dp(bb) if (as.flag & DFLAG) fprintf(stderr,"LEX:%s\n",bb);
/* char *bodystr=0, *namestr=0; */
#include "mac2doc.h"
#include "y.tab.h"

extern ARGSTRUCT as;

%}

less (\<)        { yylval.cmd = LESS; dp("LESS"); return (LESS); }
more (\>)        { yylval.cmd = MORE; dp("MORE"); return (MORE); }
word ([a-zA-Z0-9_-]+)
ws [ \t]
sstring (\\'|[^'^<]*)*
qstring \"(\\\"|[^\"]*)*\"
literal %\{%([^%]|(%[^\}])|(%\}[^%]))*%\}%
%s COMLINE
%s MDEF
%s SSTRING
%%
^{ws}*#        { BEGIN COMLINE; }

<COMLINE>%NAME% { yylval.cmd = NAME; dp ("NAME"); return (yylval.cmd) ; }
<COMLINE>%DESCRIPTION% { yylval.cmd=DESC; dp ("DESC"); return (yylval.cmd) ; }
<COMLINE>%AUTHOR% { yylval.cmd = AUTHOR; dp ("AUTHOR"); return (yylval.cmd) ; }
<COMLINE>%TITLE% { yylval.cmd = TITLE; dp ("TITLE"); return (yylval.cmd) ; }
<COMLINE>%EXAMPLE% { yylval.cmd=EXAMPLE; dp ("TITLE"); return (yylval.cmd) ; }
<COMLINE>%ATTENTION% { yylval.cmd=ATTENTION; dp ("ATTENTION");
                       return (yylval.cmd) ; }
<COMLINE>%OVERVIEW% { yylval.cmd=OVIEW; dp ("OVERVIEW"); return(yylval.cmd); }
<COMLINE>%SETUP% { yylval.cmd=SETUP; dp ("SETUP"); return(yylval.cmd); }
<COMLINE>%DEPENDENCIES% { yylval.cmd=DEP; dp("DEPENDENCIES");
                          return(yylval.cmd); }
<COMLINE>%INTERNALS% { yylval.cmd=INTER; dp("INTERNALS"); return(yylval.cmd); }
<COMLINE>%BUGS% { yylval.cmd = BUGS; dp ("BUGS"); return (yylval.cmd) ; }
<COMLINE>%END% { yylval.cmd = END; dp ("END"); return (yylval.cmd) ; }
<COMLINE>%MACROS% { yylval.cmd = MACRO; dp ("MACROS"); return (yylval.cmd) ; }
<COMLINE>%IMACROS% { yylval.cmd=IMACRO; dp ("IMACROS");return (yylval.cmd) ; }
<COMLINE>%IU% { yylval.cmd = INTERN; return (yylval.cmd) ; }
<COMLINE>%UU% { yylval.cmd = USER; return (yylval.cmd) ; }
<COMLINE>%MDESC% { yylval.cmd = MDESC; return (yylval.cmd) ; }
<COMLINE>%TOC% { yylval.cmd = TOC; return (yylval.cmd) ; }
<COMLINE>%BR% { yylval.cmd = BREAK; return (yylval.cmd) ; }
<COMLINE>%B% { yylval.cmd = BOLD; return (yylval.cmd) ; }
<COMLINE>%DL% { yylval.cmd = MDL; return (yylval.cmd) ; }
<COMLINE>%DT% { yylval.cmd = MDT; return (yylval.cmd) ; }
<COMLINE>%DD% { yylval.cmd = MDD; return (yylval.cmd) ; }
<COMLINE>%XDL% { yylval.cmd = MXDL; return (yylval.cmd) ; }
<COMLINE>%UL% { yylval.cmd = MUL; return (yylval.cmd) ; }
<COMLINE>%LI% { yylval.cmd = MLI; return (yylval.cmd) ; }
<COMLINE>%XUL% { yylval.cmd = MXUL; return (yylval.cmd) ; }
<COMLINE>%PRE% { yylval.cmd = PRE; return (yylval.cmd) ; }
<COMLINE>{literal} { yylval.string = (char*)yytext; return (LITERAL) ; }
<COMLINE>[^#%\n][^\n%]* { yylval.string = (char*)yytext; return (COMMENT) ; }
<COMLINE>\n  { BEGIN 0; as.lineno++; yylval.string = "\n" ; return (COMMENT); }

<MDEF>\' { yylval.cmd = OPENQ; dp("OPENQ"); BEGIN SSTRING ; return (OPENQ) ;}
^{ws}*def       { BEGIN MDEF; dp("def") ; return (DEF); }
<MDEF>\(        { dp("OPENB"); return (OPENB); }
<MDEF>\)        { dp("CLOSEB"); return (CLOSEB); }
<MDEF>\,        { dp("KOMMA") ; return (KOMMA); }
<MDEF>{word}    { strncpy(as.mac,(char*) yytext,300) ; dp("WORD=") ; dp(yytext);
            if (namestr) free(namestr) ;
            namestr =  (char *)strdup(yytext); return (WORD) ; }

<SSTRING>\< { yylval.cmd = LESS; dp("LESS") ; return(LESS); }
<SSTRING>{sstring} { if (bodystr) free(bodystr) ;
            bodystr = (char *)strdup(yytext); dp("BODY"); return (BODY) ;}
<SSTRING>\' { yylval.cmd = CLOSEQ; dp("CLOSEQ") ; BEGIN 0 ; return(CLOSEQ); }

.         {; }
\n        { ;}





