#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>
#include <pwd.h>
#include <fcntl.h>
#include "chkspecver.h"


#define isexec(file, fstat) (!stat((file), &(fstat)) &&                  \
                             (fstat.st_mode & S_IFMT) == S_IFREG &&      \
                             fstat.st_mode & (S_IXUSR| S_IXGRP | S_IXOTH))
      


int version_list_init(specvlist *list,
                      const char *liststr,     const char *specd_name,
                      const char *bindir_name, const char *term_name) {
   DIR           *specd;
   char          *partpath;
   char          *verstr;
   struct stat    fstat;
   struct dirent *direntry;

   if (list->list)
      version_list_free(list);
   list->nver = 0;

   /* Prepare string for specd */
   if (!(specd = opendir(specd_name))) {
      fprintf(stderr, "Error: Can't open directory `%s'.\n", specd_name);
      return(-1);
   }

   list->pathlen = strlen(specd_name);
   list->path = malloc(list->pathlen + 1 + SV_MAX_NAME_LEN +
                                           sizeof("/userfiles/hdw_lock"));
   if (!list->path) {
      fprintf(stderr, "Error: Can't allocate memory.\n");
      return(-1);
   }
   strcpy(list->path, specd_name);
   if (list->path[list->pathlen - 1] != '/')
      list->path[list->pathlen++] = '/';
   partpath = list->path + list->pathlen;


   /* Prepare path string for executables */
   if (bindir_name && *bindir_name) {
      list->binpathlen = strlen(bindir_name);
      if (!(list->binpath = malloc(list->binpathlen + 1 + SV_MAX_NAME_LEN + 1))){
         fprintf(stderr, "Error: Can't allocate memory.\n");
         return(-1);
      }
      strcpy(list->binpath, bindir_name);
      if (list->binpath[list->binpathlen - 1] != '/') {
         strcat(list->binpath, "/");
         list->binpathlen++;
      }
   } else 
      list->binpath = NULL;

   /* Prepare term string */
   if (term_name && *term_name) {
      list->termstr[0] = '_';
      strncpy(list->termstr + 1, term_name, 6);
      strcat(list->termstr, "L");
   } else {
      list->termstr[0] = '\0';
   }

   /*
    *
    */
   if (liststr) {
      version_list_add(list, liststr);
   } else {
      while(direntry = readdir(specd)) {
         /* direntry->d_namlen does not exist in solaris! */
         int d_namlen;
         d_namlen = strlen(direntry->d_name);
         if (d_namlen <= SV_MAX_NAME_LEN) {
            strcpy(partpath, direntry->d_name);
            strcpy(partpath + d_namlen, "/settings");
            if(!stat(list->path, &fstat)) {
               version_list_add(list, direntry->d_name);
            }
         }
      }
   }
   closedir(specd);

   version_list_update(list);
   return(list->nver);
}

int version_list_add(specvlist *list, const char *namelist) {
   char    *nlist, *name;
   int      length;
   int      oldnver = list->nver;
   specver **sver;

   if (namelist && *namelist && (nlist = name = malloc(strlen(namelist) + 1))) {
      strcpy(nlist, namelist);
      while(name = strtok(name, " ,;:\t")) {       /* Extract tokens */
         for (sver=&list->list; *sver; sver=&((*sver)->next))
                                                   /* Check if repeated */
            if (!strcmp(name, (*sver)->specname)) break;

         if(!*sver) {                              /* If not add to the list */
            if (*sver = malloc(sizeof(specver))) {
               if ((length = strlen(name)) <= SV_MAX_NAME_LEN && 
                   ((*sver)->specname = malloc(length + 1))) {
                  strcpy((*sver)->specname, name);
                  (*sver)->next = NULL;
                  (*sver)->status = -1;
                  (*sver)->user = NULL;
                  list->nver++;
               } else
                  *sver = NULL;
            }
         }
         name = NULL;
      }
      free(nlist);
   }
   return(list->nver - oldnver);
} 

int version_list_update(specvlist *list) {
   int            fd;
   int            pathlen;
   char          *partpath;
   struct stat    fstat;
   int            status, changed = 0;
   char           userlockf[15];
   char           *buf, buffer[128] = {0};
   int            len;
   specver       *sver;

   userlockf[6]=0;
   strncpy(userlockf, getpwuid(getuid())->pw_name, 6);
   if (*(list->termstr))
      strcat(userlockf, list->termstr);

   for (sver=list->list; sver; sver=sver->next) {
      strcpy(list->path + list->pathlen, sver->specname);
      pathlen = strlen(list->path);

      status = SV_AVAILABLE;

      strcpy(list->path + pathlen, "/settings");
      if(stat(list->path, &fstat)) status += SV_NOT_FOUND;

      strcpy(list->path + pathlen, "/userfiles/hdw_lock");
      if (sver->user) {
         free(sver->user);
         sver->user = NULL;
      }
      if((fd = open(list->path, O_RDONLY)) != -1) {
#ifdef linux
         if (flock(fd, LOCK_EX|LOCK_NB) < 0) {
#else
         if (lockf(fd, F_TEST, 0)) {
#endif
            status += SV_HDW_LOCKED;
            if (read(fd, buffer, sizeof(buffer)) > 0           &&
                (len = strlen(buf = strtok(buffer, " "))) > 0  &&
                (sver->user = malloc(len + 1)))
                  strcpy(sver->user, buf);
         }
         close(fd);
      }

      if (*(list->termstr)){
         strcpy(list->path + pathlen + 11, userlockf);
         if((fd = open(list->path, O_RDONLY)) != -1) {
#ifdef linux
            if (flock(fd, LOCK_EX|LOCK_NB) < 0) 
#else
            if (lockf(fd, F_TEST, 0)) 
#endif
               status += SV_USR_LOCKED;

            close(fd);
         }
      }

      if (list->binpath) {
         strcpy(list->binpath + list->binpathlen, sver->specname);
         if (!isexec(list->binpath, fstat)) status += SV_NO_BINARY;
      } else {
         strcpy(list->path + list->pathlen, "../bin/");
         strcpy(list->path + list->pathlen + 7, sver->specname);
         if (!isexec(list->path, fstat)){
            strcpy(list->path + list->pathlen, "../../bin/");
            strcpy(list->path + list->pathlen + 10, sver->specname);
            if (!isexec(list->path, fstat)) status += SV_NO_BINARY;
         }
      }
      if (sver->status != status) {
         sver->status = status;
         changed = 1;
      }
   }
   return(changed);
}


int version_list_free(specvlist *list) {
   specver       *sver0, *sver;

   free(list->path);
   free(list->binpath);
   for (sver = list->list; sver;) {
      sver0 = sver;
      free(sver0->specname);
      free(sver0->user);
      sver = sver0->next;
      free(sver0);
   }
   list->list = NULL;
}

