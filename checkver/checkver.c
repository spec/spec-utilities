
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "chkspecver.h"

void show_version_list(specvlist *list);

void main(int argc, char **argv) {
   extern char *optarg;
   extern int   optind, optopt;
   char         errflg=0;
   char        *basename;
   char        *specd    = NULL;
   char        *bind     = NULL;
   char        *verstr   = NULL;
   char        *termstr  = "esrf";
   specvlist    speclist = {NULL};

   while (argc > 1 && optind < argc) {
      switch (getopt(argc, argv, ":D:T:v:e:h")) {
         case 'D':
            specd = optarg;
            break;
         case 'T':
            termstr = optarg;
            break;
         case 'v':
            verstr = optarg;
            break;
         case 'e':
            bind = optarg;
            break;
         case 'h':
            errflg++;
            break;
         case ':':        /* missing argument */
            errflg++;
            fprintf(stderr, "Option -%c requires an argument.\n",optopt);
            break;
         case '?':        /* unrecognized option */ 
            errflg++;
            fprintf(stderr, "Unrecognized option: -%c.\n",optopt);
            break;
         case -1:
            errflg++;
            fprintf(stderr, "parameter `%s' not recognized.\n", argv[optind]);
            optind++;
            break;
         default:
            errflg++;
            fprintf(stderr, "Internal error.\n");
            break;
       }
   }
   if (errflg) {
      if (basename = strrchr(argv[0],'/'))
         basename++;
      else
         basename = argv[0];
      fprintf(stderr, "usage: %s [-h] [-v verlist] [-D specd] [-e bindir] [-T term]\n", basename);
      fprintf(stderr, "    -h : Show help.\n");
      fprintf(stderr, "    -v : Check only the versions of spec listed in `verlist'.\n");
      fprintf(stderr, "    -D : Use `specd' as SPECD.\n");
      fprintf(stderr, "    -e : Path to executable files.\n");
      fprintf(stderr, "    -T : Use `term' as terminal string.\n");
      fprintf(stderr, "\n");
      exit (1);
   }
   if (termstr) printf("Terminal: %s\n", termstr);

   if (!specd && !(specd = getenv("SPECD"))) {
      fprintf(stderr, "Error: Environment variable SPECD not set.\n");
      exit (1);
   } 
   printf("SPECD: %s\n", specd);

   if (!verstr) verstr = getenv("SPECVERLIST");
   printf("Versions: %s\n", verstr ? verstr : "<all found>");

   if (bind) printf("Bindir: %s\n", bind);

   switch (version_list_init(&speclist, verstr, specd, bind, termstr)) {
      case 0:
         fprintf(stderr, "No spec versions specified.\n");
         break;
 
      case -1:
         fprintf(stderr, "Error creating list.\n");
         break;
 
      default:
         show_version_list(&speclist);
         while(1) {
            sleep(1);
            if(version_list_update(&speclist))
               show_version_list(&speclist);
         }
         break;
   }

   printf("Done.\n");
}

void show_version_list(specvlist *list) {
   int         i;
   int         status;
   specver    *sver;
 
   printf("\nVersions of spec, current status:\n");

   for (sver=list->list; sver; sver=sver->next) {
      status = sver->status;
      printf("%10s - ", sver->specname);
      if (status == SV_AVAILABLE)
         printf("%s\n", "Available");
      else {
         if (status & SV_NOT_FOUND)  printf("Not Found + ");
         if (status & SV_HDW_LOCKED) printf("Hardware Locked (by %s) + ", sver->user);
         if (status & SV_USR_LOCKED) printf("User Locked + ");
         if (status & SV_NO_BINARY)  printf("No Binary + ");
         printf("\b\b  \n");
      }
   }
   printf("\n(use ^C to stop)\n");
}

