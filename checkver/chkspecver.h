#ifndef _CHKSPECVER
#define _CHKSPECVER

#ifdef SPECGUI
#include <Xm/Xm.h>
#endif

#define SV_MAX_NAME_LEN   50

#define SV_AVAILABLE   0x00
#define SV_HDW_LOCKED  0x02
#define SV_USR_LOCKED  0x04
#define SV_NOT_FOUND   0x08
#define SV_NO_BINARY   0x10

typedef struct _specver{
   char            *specname;
   int              status;
   char            *user;
#ifdef SPECGUI
   Widget          wopen;
#endif
   struct _specver *next;
} specver;

typedef struct {
   specver *list;
   int      nver;
   char    *path;
   int      pathlen;
   char    *binpath;
   int      binpathlen;
   char     termstr[9];
} specvlist;

int version_list_init(specvlist *list,
                      const char *liststr, const char *specd,
                      const char *bindir,  const char *term);
int version_list_add(specvlist *list, const char *liststr);
int version_list_update(specvlist *list);
int version_list_free(specvlist *list);

#endif /* _CHKSPECVER */

