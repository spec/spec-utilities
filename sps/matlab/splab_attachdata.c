#include "mex.h"
#include "sps.h"

static int conv[][2] = {{mxINT8_CLASS, SPS_CHAR},
			{mxINT16_CLASS, SPS_SHORT},
			{mxINT32_CLASS, SPS_LONG},
			{mxSINGLE_CLASS, SPS_FLOAT},
			{mxDOUBLE_CLASS, SPS_DOUBLE}, 
			{mxUINT8_CLASS, SPS_UCHAR},
			{mxUINT16_CLASS, SPS_USHORT}, 
			{mxUINT32_CLASS, SPS_ULONG},     
			{mxCHAR_CLASS, SPS_STRING}     
};

static int MATLAB_to_Spec_type (int mat_type)
{
  int i;
  for (i=0; i < sizeof(conv)/sizeof(int)/2; i++)
    if (mat_type == conv[i][0])
      return conv[i][1];
  return -1;
}

static int Spec_to_MATLAB_type (int spec_type)
{
  int i;
  for (i=0; i < sizeof(conv)/sizeof(int)/2; i++)
    if (spec_type == conv[i][1])
      return conv[i][0];
  return -1;
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  int rows, cols, type;
  double *matptr;
  char *spec_version, vbuf[512], array_name[512];
  char eb[512];
  void *data;
  int ds[2];

  if (nrhs != 2) 
    mexErrMsgTxt ("Usage: splab_attachdata (<specversion>, <array_name>)");
  
  if (nlhs > 1) 
    mexErrMsgTxt ("Only one output array allowed");
  
  if (mxIsChar(prhs[0]) != 1 || mxIsChar(prhs[1]) != 1 ||
      mxGetM(prhs[0]) != 1 || mxGetM(prhs[1]) != 1 )
    mexErrMsgTxt ("Input parameters must be row vectors of type string!");
  
  mxGetString(prhs[0], vbuf, 512);
  if (strcmp(vbuf,"*") == 0)
    spec_version = NULL;
  else
    spec_version = vbuf;
  mxGetString(prhs[1], array_name, 512);
  
  if (SPS_GetArrayInfo(spec_version, array_name, &rows, &cols, &type, NULL)) {
    sprintf(eb, "Can not get info from: %s %s\n", spec_version, array_name);
    mexErrMsgTxt (eb);
  }

  if ((data = SPS_GetDataPointer(spec_version, array_name, 1)) == NULL) {
    sprintf(eb, "Can not attach to array: %s %s\n", spec_version, array_name);
    mexErrMsgTxt (eb);
  }
  
  if (type == SPS_DOUBLE) {
    plhs[0] = mxCreateDoubleMatrix(1, 1, mxREAL);
  } else {
    ds[0]=1; ds[1]=1;
    plhs[0] = mxCreateNumericArray(2, ds, Spec_to_MATLAB_type(type), mxREAL);
  }
  
  mxSetM (plhs[0], cols);
  mxSetN (plhs[0], rows);
  mxSetPr(plhs[0], data);
  mexMakeArrayPersistent (plhs[0]);
  return;
}
  
  

