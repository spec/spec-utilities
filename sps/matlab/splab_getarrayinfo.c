#include "mex.h"
#include "sps.h"

static int conv[][2] = {{mxINT8_CLASS, SPS_CHAR},
			{mxINT16_CLASS, SPS_SHORT},
			{mxINT32_CLASS, SPS_LONG},
			{mxSINGLE_CLASS, SPS_FLOAT},
			{mxDOUBLE_CLASS, SPS_DOUBLE}, 
			{mxUINT8_CLASS, SPS_UCHAR},
			{mxUINT16_CLASS, SPS_USHORT}, 
			{mxUINT32_CLASS, SPS_ULONG},     
			{mxCHAR_CLASS, SPS_STRING}     
};

static int MATLAB_to_Spec_type (int mat_type)
{
  int i;
  for (i=0; i < sizeof(conv)/sizeof(int)/2; i++)
    if (mat_type == conv[i][0])
      return conv[i][1];
  return -1;
}

static int Spec_to_MATLAB_type (int spec_type)
{
  int i;
  for (i=0; i < sizeof(conv)/sizeof(int)/2; i++)
    if (spec_type == conv[i][1])
      return conv[i][0];
  return -1;
}


static void set_val (mxArray *structure, char* field, double value)
{
  mxArray *tmparray;
  double *tmpdptr;
  tmparray = mxCreateDoubleMatrix(1, 1, mxREAL);
  tmpdptr = mxGetPr(tmparray);
  *tmpdptr = value;
  mxSetField (structure, 0, field, tmparray);
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  int rows, cols, type, flag;
  char *spec_version, vbuf[512], array_name[512];
  char eb[512];
  char *fields[] = {"rows", "cols", "datatype", "flags"};
  int dims[2];

  if (nrhs != 2) 
    mexErrMsgTxt ("Usage: splab_getarrayinfo <specversion> <array_name>");

  if (nlhs > 1) 
    mexErrMsgTxt ("Only one output cell array allowed");
    
  if (mxIsChar(prhs[0]) != 1 || mxIsChar(prhs[1]) != 1 ||
      mxGetM(prhs[0]) != 1 || mxGetM(prhs[1]) != 1 )
    mexErrMsgTxt ("Input parameters must be row vectors of type string!");
  
  mxGetString(prhs[0], vbuf, 512);
  if (strcmp(vbuf,"*") == 0)
    spec_version = NULL;
  else
    spec_version = vbuf;
  mxGetString(prhs[1], array_name, 512);

  if (SPS_GetArrayInfo(spec_version, array_name, &rows, &cols, &type, &flag)){
    sprintf(eb, "Can not get info for: %s %s\n", spec_version, array_name);
    mexErrMsgTxt (eb);
  }
  
  dims[0] = 1; dims[1] = 1;
  plhs[0] = mxCreateStructArray (2, dims, 4, fields); 
  
  set_val (plhs[0], "rows", (double) cols);
  set_val (plhs[0], "cols", (double) rows);
  set_val (plhs[0], "datatype", (double) Spec_to_MATLAB_type(type));
  set_val (plhs[0], "flags", (double) flag);

  return;
}

