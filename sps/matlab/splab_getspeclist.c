#include "mex.h"
#include "sps.h"

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  char *spec_version;
  int i, dims[1], dim;
  mxArray *tmp[512];

  if (nlhs > 1) 
    mexErrMsgTxt ("Only one output array allowed");
    
  if (nrhs > 0)
    mexErrMsgTxt ("Usage: splab_getspeclist");
    
  for (i = 0, dim = 0; spec_version = SPS_GetNextSpec (i) ; dim++, i++) {
    if (i >= 512) 
      mexErrMsgTxt ("More than 512 SPEC versions - can't be");
    tmp[i] = mxCreateString(spec_version);
  }
  
  dims[0] = dim;
  plhs[0] = mxCreateCellArray (1,dims); 
  for (i = 0; i < dim; i++)  
    mxSetCell (plhs[0], i, tmp[i]);
	       
  return;
}
  
  
