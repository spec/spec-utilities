#include "mex.h"
#include "sps.h"

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  char *spec_version, vbuf[512], array_name[512];
  double *tmpdptr;
  int ret;

  if (nrhs != 2) 
    mexErrMsgTxt ("Usage: splab_updatecounter (<specversion>,<array_name>)");

  if (nlhs > 1) 
    mexErrMsgTxt ("Only one output allowed");
    
  if (mxIsChar(prhs[0]) != 1 || mxIsChar(prhs[1]) != 1 ||
      mxGetM(prhs[0]) != 1 || mxGetM(prhs[1]) != 1 )
    mexErrMsgTxt ("Input parameters must be row vectors of type string!");
  
  mxGetString(prhs[0], vbuf, 512);
  if (strcmp(vbuf,"*") == 0)
    spec_version = NULL;
  else
    spec_version = vbuf;
  mxGetString(prhs[1], array_name, 512);
  
  ret = SPS_UpdateCounter(spec_version, array_name);
  
  plhs[0] = mxCreateDoubleMatrix(1, 1, mxREAL);
  tmpdptr = mxGetPr(plhs[0]);
  *tmpdptr = (double) ret;

  return;
}

