#!/bin/sh
#
# usage:        arch.sh
#
# abstract:     This Bourne Shell script determines the architecture
#		of the the current machine.
#
#		ARCH	  - Machine architecture
#
# note(s):	1. This routine must be called using a . (period)
#
# Copyright (c) 1986-1997 by The MathWorks, Inc.
# $Revision: 1.8 $  $Date: 1997/11/06 23:02:48 $
#----------------------------------------------------------------------------
#
#=======================================================================
# Functions:
#=======================================================================
	matlab5_arch () { # Determine the architecture for MATLAB 5
			  # and outputs it to standard output
                          #
                          # Always returns a 0 status.
                    	  #
                    	  # usage: matlab5_arch
                    	  #
	Arch="unknown"
#
	if [ -f /bin/uname ]; then
   	    case "`/bin/uname`" in
	        SunOS)					# sun4 and sol2
	            if [ -d /dev/pts ]; then
		        Arch="sol2"
	            else
		        Arch="sun4"
	            fi
	            ;;
	        HP-UX)					# hp700, hp800
	            if [ -f /bin/hp9000s700 ]; then
		        (/bin/hp9000s700) > /dev/null 2>&1
		        if [ $? -eq 0 ]; then
	    	            Arch="hp700"
		        fi
	            fi
	            if [ -f /bin/hp9000s800 ]; then
		        (/bin/hp9000s800) > /dev/null 2>&1
		        if [ $? -eq 0 ]; then
	    	            Arch="hp700"
		        fi
	            fi
	            ;;
	        IRIX)					# sgi
    	            if [ -f /bin/4d ]; then
		        (/bin/4d) > /dev/null 2>&1
		        if [ $? -eq 0 ]; then
	    	            Arch="sgi"
		        fi
	            fi
	            ;;
	        IRIX64)		# Want this to mean MIPS 4/64 bit OS sgi
    	            if [ -f /bin/4d ]; then
		        (/bin/4d) > /dev/null 2>&1
		        if [ $? -eq 0 ]; then
	    	            Arch="sgi64"
		        fi
	            fi
#
# Watch out! Some machines are not MIPS 4 (R4400)
#
		    if [ -f /bin/hinv ]; then
			cpu=`/bin/hinv | awk '
#-----------------------------------------------------------------------
	$1 == "CPU:" { print substr($3,1,2) }'`
#-----------------------------------------------------------------------
			if [ "$cpu" = "R4" ]; then
			    Arch=sgi
			fi
		    fi
	            ;;
	        OSF1)					# alpha
	    	    Arch="alpha"
	            ;;
	        AIX*)					# ibm_rs
#
# With AIX 4.1, uname can return more than just 'AIX'
#
	            Arch="ibm_rs"
	            ;;
	        Linux)
		    Arch="lnx86"
		    ;;
	        *)
		    :
	            ;;
	    esac
	elif [ -f /bin/arch ]; then				# early sun4
	    Arch="`/bin/arch`"
	fi
#
        echo $Arch
#
	return 0
    }
#
    if [ "$ARCH_CHECK" = "" -a "$ARCH_LIST" != "" ]; then
#
# Check input arguments $1, ..., $$# for -ARCH
#
	i=1
	while [ $i -le $# ]
	do
	    value=`eval echo '$'"$i"`
	    ARCH=`echo "$ARCH_LIST EOF $value" | awk '
#-----------------------------------------------------------------------
	{ for (i = 1; i <= NF; i = i + 1)
	      if ($i == "EOF") {
		  narch = i - 1
		  if (NF != i + 1) exit
	          break
	      }
	  for (i = 1; i <= narch; i = i + 1)
		if ("-" $i == $NF) {
		    print $i
		    exit
		}
	}'`
#-----------------------------------------------------------------------
	    if [ "$ARCH" != "" ]; then
		break
	    fi
	    i=`expr $i + 1`
	done
    fi
#
    if [ "$ARCH" = "" ]; then
	ARCH=`matlab5_arch`
    fi
    Arch=$ARCH
