#!/bin/sh
#set -x
#
# usage:        oscheck.sh
#
# abstract:     This Bourne Shell script determines the OS level of the
#		system and responds according to what it finds. There
#		are four numbers of interest:
#
#		    minimum_ver - major release number of minimum
#				  version to run
#		    version     - current version
#		    built_ver   - built version
#		    future_ver  - future version that we know doesn't work
#				  "-" means infinite
#
#		if version >= future_ver 
#		     new error message
#		     request to stop
#		If built_ver <= version < future_ver
#		     no message
#		If minimum_ver <= version < built_ver 
#		     warning message only
#		If version < minimum_ver
#		     old error message
#		     request to stop
#
#		Arch	  - Machine architecture variable must be set
#
# note(s):	1. This routine must be called using a . (period)
#
# 		2. token is the version in N.N... form
#
#		   sun4:   uname -r     -> token
#		   sol2:   uname -r     -> token
#		   hp700:  uname -r     -> Letter.token
#		   alpha:  uname -r     -> Vtoken
#		   ibm_rs: uname -v -r  -> token(2).token(1) = token
#		   sgi:    uname -r     -> token
#		   sgi64:  uname -r     -> token
#		   lnx86:  uname -r     -> token		   
#		   
#		3. Cases to watch out for
#
#		   SunOS:  4.1.3_U1     - non number at some point
#		   OSF1:   V4.0		- starts with v or V 
#
#		4. This requires at least the System V Bourne Shell since
#		   internal functions are used.
#
#		5. Algorithm:
#
#		   a. write output to a temporary file
#		   b. If empty cleanup and continue
#		   c. If not ERROR
#			  print the file
#		          cleanup and continue
#		      else
#			  print the file
#			  output request to quit
#			  get input (force quite in batch)
#			  continue if n (default is to quit)
#			  cleanup and continue if not quit
#
#		6. Assume that this routine is called using the
#		   unused temporary file defined by
#
#		      $temp_file
#
#		7. The calling routine will exit if the variable,
#		   oscheck_status, is set as follows:		 
#
#		       oscheck_status=1
#
#		8. Also, any cleanup associated with of an interrupt
#		   will be handled by the calling routine.
#
# Copyright (c) 1996-1997 by The MathWorks, Inc.
# $Revision: 1.9 $  $Date: 1997/11/30 21:28:38 $
#----------------------------------------------------------------------------
#
   oscheck_status=0
#
#                  arch   OSprefix built_ver minimum_ver future_ver
#
   oslist="        sun4   SunOS    4.1.4     4           -"
   oslist="$oslist sol2   SunOS    5.5       5           -"
   oslist="$oslist hp700  HP-UX    10.20     10          -"
   oslist="$oslist alpha  OSF1     4.0       4           -"
   oslist="$oslist ibm_rs AIX      4.2       4           -"
   oslist="$oslist sgi    IRIX     6.3       6           -"
   oslist="$oslist sgi64  IRIX     6.4       6           -"
   oslist="$oslist lnx86  Linux    2.0.18    1           -"
#
   if [ -f /bin/uname ]; then
   	case $Arch in
	    sun4|sol2|sgi|sgi64|lnx86)
                ver=`/bin/uname -r`
		;;
	    hp700)
		ver=`/bin/uname -r | awk '{print substr($1,3)}'`
		;;
	    alpha)
		ver=`/bin/uname -r | awk '{print substr($1,2)}'`
		;;
	    ibm_rs) # I think there are only 2 digits even though the 'real'
		    # version can have 3
		ver=`/bin/uname -v -r | awk '{print $2 "." $1}'`
		;;
	    *)
                ver=`/bin/uname -r | awk '{print $1}'`
		;;
	esac
   fi
#
   echo $Arch $ver "$oslist"| awk '
#---------------------------------------------------------------------------
	BEGIN { str = "0123456789"; strp = str "." }
	{ arch = $1;
	  origver = $2;
#
# remove any non-numeric prefix from the version like V, etc.
#
	  ver = ""
	  for (j = 1; j <= length(origver); j = j + 1)
	      if (index(strp,substr(origver,j,1)) != 0) {
		  ver = substr(origver,i)
		  break
	      }
          lver = length(ver)
	  for (i = 3; i <= NF; i = i + 5) {
	      n[$i] = $(i+1) ""
	      v[$i] = $(i+2) ""
	      m[$i] = $(i+3) ""
	      f[$i] = $(i+4) ""
	  }
	  rver = v[arch]
	  if (rver == "") exit 
          mver = m[arch]
	  fver = f[arch]  
#
# split the release version
#
	  nrver = split(rver,rv,".")
	  for (i = 1; i <= nrver; i = i + 1)
	      if (rv[i] == "") rv[i] = 0
#
# split the minimum version
#
	  nmver = split(mver,mv,".")
	  for (i = 1; i <= nmver; i = i + 1)
	      if (mv[i] == "") mv[i] = 0
#
# split the future version if there is one
#
	  if (fver != "-") {
	      nfver = split(fver,fv,".")
	      for (i = 1; i <= nfver; i = i + 1)
	          if (fv[i] == "") fv[i] = 0
	  }
#
# split the actual version
# remove everything in a word after a nondigit
#
          nver = split(ver,v,".")
	  for (i = 1; i <= nver; i = i + 1) {
	      new = ""
	      for (j = 1; j <= length(v[i]); j = j + 1)
	          if (index(str,substr(v[i],j,1)) != 0)
		      new = new substr(v[i],j,1)
		  else
		      break
	      v[i] = new
	      if (v[i] == "") v[i] = 0
	  }
#
# errmode: 0 - ok
#          1 - too old error
#          2 - warning
#          3 - too new error
#
          errmode = 0
#
# check against the minimum
#
	  if (nmver < nver)
	      ncd = nmver
	  else
	      ncd = nver
	  for (i = 1; i <= ncd; i = i + 1)
	      if (mv[i] + 0 > v[i] + 0)
		  errmode = 1
	      else if (mv[i] + 0 < v[i] + 0)
		  break
	  if (fail == 0 && i == ncd + 1 && ncd == nver)
	      for (i = ncd + 1; i <= nmver; i = i + 1)
	          if (mv[i] + 0 == 0 )
		      continue
	          else
		      errmode = 1
#
# check against the built version
#
	  if (errmode == 0) {
	      if (nrver < nver)
	          ncd = nrver
	      else
	          ncd = nver
	      for (i = 1; i <= ncd; i = i + 1)
	          if (rv[i] + 0 > v[i] + 0)
		      errmode = 2
	          else if (rv[i] + 0 < v[i] + 0)
		      break
	      if (errmode == 0 && i == ncd + 1 && ncd == nver)
	          for (i = ncd + 1; i <= nrver; i = i + 1)
	              if (rv[i] + 0 == 0)
		          continue
	              else
		          errmode = 2
          }
#
# check against the future version
#
	  if (errmode == 0 && fver != "-") {
	      if (nfver < nver)
	          ncd = nfver
	      else
	          ncd = nver
	      for (i = 1; i <= ncd; i = i + 1)
	          if (fv[i] + 0 <= v[i] + 0)
		      errmode = 3
	          else if (fv[i] + 0 > v[i] + 0)
		      break
	      if (errmode == 0 && i == ncd + 1 && ncd == nver)
	          for (i = ncd + 1; i <= nfver; i = i + 1)
	              if (fv[i] + 0 == 0)
		          continue
	              else
		          errmode = 3
          }
	  if (errmode == 1) {
print ""   
print "---------------------------------------------------------------------------"
printf "Warning: %s %-10s - Unsupported Operating System\n", n[arch], ver
printf "         %s %-10s - MATLAB built using this Operating System\n", n[arch], rver
print "---------------------------------------------------------------------------"
print ""
          }
          else if (errmode == 2) {
print ""   
print "---------------------------------------------------------------------------"
printf "Warning: %s %-10s - Your Operating System\n", n[arch], ver
printf "         %s %-10s - MATLAB built using this Operating System\n", n[arch], rver
print "---------------------------------------------------------------------------"
print ""
          }
          else if (errmode == 3) {
print ""   
print "---------------------------------------------------------------------------"
printf "Warning: %s %-10s - Unqualified Operating System\n", n[arch], ver
printf "         %s %-10s - MATLAB built using this Operating System\n", n[arch], rver
print "---------------------------------------------------------------------------"
print ""
	  }
	}' > $temp_file
#---------------------------------------------------------------------------
#=======================================================================
# Functions:
#=======================================================================
    echon ()    { # echo without a newline - UNIX dependent
                  #
                  # usage: echon
                  #
        if [ "`echo -n`" != "" ]; then
            echo "$1\c"
        else
            echo -n "$1"
        fi
    }
#=======================================================================
    if [ -s $temp_file ]; then
	cat $temp_file
	errmode=`cat $temp_file | awk 'NR == 3 {print $2}'`
	case "$errmode" in
	    'OLD):')
#---------------------------------------------------------------------------
echo 'Your Operating System APPEARS to be too OLD to install or run MATLAB!'
echo '------------------------------------------------------------------------'
echo 'Please contact The MathWorks, Inc. if you have any questions ...'
echo '       phone: (508) 647-7000'
echo '       email: support@mathworks.com'
echo '       web:   http://www.mathworks.com'
echo 'Press the <return> key at the prompt to quit ...'
echo ' '
echon 'Please quit now ([y]/n) > '
#---------------------------------------------------------------------------
		if [ "$batch" != "1" ]; then
		    read ans
		    if [ `expr "//$ans" : '//[Nn].*'` -gt 0 ]; then
		        :
		    else
			oscheck_status=1
		    fi
		else
#---------------------------------------------------------------------------
     echo 'n'		    
#---------------------------------------------------------------------------
		fi
		;;
	    'NEW):')
#---------------------------------------------------------------------------
echo 'Your Operating System APPEARS to be too NEW to install or run MATLAB!'
echo '------------------------------------------------------------------------'
echo 'Please contact The MathWorks, Inc. if you have any questions ...'
echo '       phone: (508) 647-7000'
echo '       email: support@mathworks.com'
echo '       web:   http://www.mathworks.com'
echo 'Press the <return> key at the prompt to quit ...'
echo ' '
echon 'Please quit now ([y]/n) > '
#---------------------------------------------------------------------------
		if [ "$batch" != "1" ]; then
		    read ans
		    if [ `expr "//$ans" : '//[Nn].*'` -gt 0 ]; then
		        :
		    else
			oscheck_status=1
		    fi
		else
#---------------------------------------------------------------------------
     echo 'n'		    
#---------------------------------------------------------------------------
		fi
		;;
	     *)
		:
		;;
	esac
    fi
    rm -f $temp_file
