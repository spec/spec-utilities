#if defined(WITH_COMMENTS)
/*
 * fintrf.h	- MATLAB/FORTRAN interface header file. This file
 *		  contains the declaration of the pointer type needed
 *		  by the MATLAB/FORTRAN interface.
 *
 * Copyright (c) 1984-1998 by The MathWorks, Inc.
 * All Rights Reserved.
 * $Revision: 1.9 $  $Date: 1997/12/05 21:01:01 $
 */
#endif
#if defined(__alpha) || (defined(__sgi) && _MIPS_SZPTR==64)
#define mwpointer integer*8
#define MWPOINTER INTEGER*8
#else
#define mwpointer integer
#define MWPOINTER INTEGER
#define mxCopyPtrToPtrArray mxCopyPtrToInteger4
#endif

#if defined(WITH_COMMENTS)
C For backward compatibility, keep the definition of pointer/POINTER 
C except for FORTRAN 90. 
C If you use FORTRAN 90 compiler, do pass the flag like this:
C           mex -DFORTRAN_90 myfile.F
#endif

#if !defined(FORTRAN_90)

#if defined(__alpha) || (defined(__sgi) && _MIPS_SZPTR==64)
#define pointer integer*8
#define POINTER INTEGER*8
#else
#define pointer integer
#define POINTER INTEGER
#endif

#endif
