/* This file contains an initialization routine used in the 
 * PowerMacintosh CodeWarrior Mbuild project to set up 
 * the default print handler to be CodeWarrior's printf 
 * function.
 *
 * SCF 9/10/97
 *
 * $Revision: 1.3 $
 */

#include <CodeFragments.h>
#include <Types.h>
#include "matlab.h"
#undef printf /* matlab.h is still #defining printf incorrectly */
#include <stdio.h>
#include <string.h>

#ifdef __cplusplus
extern "C" {
#endif
void __sinit(void);
OSErr __mw_initialize(InitBlockPtr ignoredParameter);

static void print_handler(const char *string)
{
	char * pstr;
	int	   len, i;
	
	/* linefeed characters from libmmfile are Carriage Returns.  CodeWarrior
	   expects Newlines.  So this loop maps the CR's to NL's.  Note that we are 
	   casting away constness of "string" here.  There are conceivable cases where this
	   could be bad, in which case you would have to duplicate the string and map the 
	   linefeeds in the copy. */
	pstr = (char *) string;
	len = strlen(pstr);
	for (i = 0; i < len; i++)
		if (pstr[i] == '\r')
			pstr[i] = '\n';
	
	printf("%s", pstr);
}

OSErr __mw_initialize(InitBlockPtr ignoredParameter)
{
	__sinit();
	
	mlfSetPrintHandler(print_handler);

	return(noErr);
}

#ifdef __cplusplus
}
#endif
