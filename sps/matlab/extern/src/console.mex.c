/* $Revision: 1.1 $ */
/************************************************************************/
/*	Project...:	Macintosh MATLAB PowerPC MEX-function      				*/
/*	Name......:	console.mex.c											*/
/*	Purpose...:	map console functions to MATLAB's command window		*/
/*  Copyright.: ęCopyright 1997 by The MathWorks, Inc.                  */
/* 																		*/
/* Modified from console.stubs.c, ęCopyright 1994 Metrowerks, Inc.		*/
/************************************************************************/

#ifndef __CONSOLE__
#include <console.h>
#endif

#include <stdio.h>
#include <string.h>

#include "mex.h"

/*
 *	extern short InstallConsole(short fd);
 *
 *	Installs the Console package, this function will be called right
 *	before any read or write to one of the standard streams.
 *
 *	short fd:		The stream which we are reading/writing to/from.
 *	returns short:	0 no error occurred, anything else error.
 */

short InstallConsole(short fd)
{
#pragma unused (fd)

	return 0;
}

/*
 *	extern void RemoveConsole(void);
 *
 *	Removes the console package.  It is called after all other streams
 *	are closed and exit functions (installed by either atexit or _atexit)
 *	have been called.  Since there is no way to recover from an error,
 *	this function doesn't need to return any.
 */

void RemoveConsole(void)
{
}

/*
 *	extern long WriteCharsToConsole(char *buffer, long n);
 *
 *	Writes a stream of output to the Console window.  This function is
 *	called by write.
 *
 *	char *buffer:	Pointer to the buffer to be written.
 *	long n:			The length of the buffer to be written.
 *	returns short:	Actual number of characters written to the stream,
 *					-1 if an error occurred.
 */

long WriteCharsToConsole(char *buffer, long n)
{
	char formatstr[32];
	
	/* CodeWarrior Pro 1.0 does not ship MSL libraries that have the 
	   Map newlines to CR option turned on.  So anything printed with
	   ANSI C or C++ functions (eg fprintf(stdout,...) or cout) has
	   its line endings messed up.  This code unmesses them up before 
	   passing buffer to MATLAB */
	{
		int i;
		
		for (i = 0; i < n; i++)
		{
			if (buffer[i] == 0x0A)
				buffer[i] = 0x0D;
			else if (buffer[i] == 0x0D)
				buffer[i] = 0x0A;
		}
	}

	/* Make a format string that looks like this (given n==5): "%.5s"
	   this specifies a 5 character string, and removes the need for a 
	   null byte at the end of the buffer */
	sprintf(formatstr, "%%.%ds", n);
	mexPrintf(formatstr, buffer);
	
	return n;
}

/*
 *	extern long ReadCharsFromConsole(char *buffer, long n);
 *
 *	Reads from the Console into a buffer.  This function is called by
 *	read.
 *
 *	char *buffer:	Pointer to the buffer which will recieve the input.
 *	long n:			The maximum amount of characters to be read (size of
 *					buffer).
 *	returns short:	Actual number of characters read from the stream,
 *					-1 if an error occurred.
 */
long ReadCharsFromConsole(char *buffer, long n)
{
	mxArray * prhs[2];
	mxArray * plhs[1];
	
	int thelen;
	
	/* mimic data = input('>', 's');
	   the '>' can be changed to anything but an empty string, because
	   the input function doesn't seem to like an empty string prompt.
	 */
	prhs[0] = mxCreateString(">");
	prhs[1] = mxCreateString("s");
	
	mexCallMATLAB(1,plhs,2,prhs,"input");
	
	mxDestroyArray(prhs[0]);
	mxDestroyArray(prhs[1]);
	
	mxGetString(plhs[0], buffer, n);
	
	thelen = strlen(buffer);
	
	/* MATLAB's input doesn't append a newline.  The following replaces the 
	   NULL byte that mxGetString appends, which is not necessary for
	   this function, to a newline, which is. */
	buffer[thelen] = '\n';
	
	return thelen + 1;
}

/*
 *	extern char *__ttyname(long fildes);
 *
 *	Return the name of the current terminal (only valid terminals are
 *	the standard stream (ie stdin, stdout, stderr).
 *
 *	long fildes:	The stream to query.
 *
 *	returns char*:	A pointer to static global data which contains a C string
 *					or NULL if the stream is not valid.
 */

extern char *__ttyname(long fildes)
{
#pragma unused (fildes)
	/* all streams have the same name */
	static char *__devicename = "null device";

	if (fildes >= 0 && fildes <= 2)
		return (__devicename);

	return (0L);
}

