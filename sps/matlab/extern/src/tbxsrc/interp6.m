function F = interp6(arg1,arg2,arg3,arg4,arg5)
%INTERP6 2-D Nearest neighbor interpolation.
%
%   See INTERP2.

%   Copyright (c) 1984-1998 by The MathWorks, Inc.
%   $Revision: 1.2 $  $Date: 1997/12/12 20:18:30 $

warning(sprintf(['INTERP6 is obsolete and will be eliminated in future' ...
    ' versions.\n         Please use INTERP2(...,''*nearest'') instead.']));

n = nargin;
error(nargchk(1,5,n));

if (n == 1)
  F = interp2(arg1,'*nearest');
elseif (n == 2)
  F = interp2(arg1,arg2,'*nearest');
elseif (n == 3)
  F = interp2(arg1,arg2,arg3,'*nearest');
elseif (n == 4)
  F = interp2(arg1,arg2,arg3,arg4,'*nearest');
elseif (n == 5)
  F = interp2(arg1,arg2,arg3,arg4,arg5,'*nearest');
end
