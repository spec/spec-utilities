function tf = ismember(a,s,flag)
%ISMEMBER True for set member.
%   ISMEMBER(A,S) when A is a vector returns a vector containing 1 where
%   the elements of A are in the set S and 0 otherwise.
%
%   ISMEMBER(A,S,'rows') when A and S are matrices with the same
%   number of columns returns a vector containing 1 where the rows of
%   A are also rows of S and 0 otherwise.
%
%   See also UNIQUE, INTERSECT, SETDIFF, SETXOR, UNION.

%   Copyright (c) 1984-1998 by The MathWorks, Inc.
%   $Revision: 1.2 $  $Date: 1997/12/12 20:18:30 $

error(nargchk(2,3,nargin));

if nargin==2
  rowvec = size(a,1)==1;
  if isempty(a) | isempty(s),
    if prod(size(a))==length(a)
      tf = logical(zeros(size(a)));
    else
      tf = logical(zeros(prod(size(a)),1));
    end
    return
  end
  a = a(:);
  if prod(size(a))<100,
    [s,a] = meshgrid(s,a);
    tf = any(a == s,2);
  else
    tf = [];
    while ~isempty(a),
      n = min(100,length(a));
      [ss,aa] = meshgrid(s,a(1:n));
      tf = [tf;any(aa == ss,2)];
      a(1:n) = [];
    end
  end
  if rowvec, tf = tf.'; end
else
  tf = logical(zeros(size(a,1),1));
  if isempty(a) | isempty(s), return, end
  for i=size(a,1):-1:1,
    tf(i) = any(v4strmatch(a(i,:),s,'exact'));
  end
end



function i = v4strmatch(str,strs,flag)
%STRMATCH Find possible matches for string.
%   I = STRMATCH(STR,STRS) looks through the rows of the character
%   array               of strings STRS to find strings that begin
%   with string STR, returning the matching row indices.  STRMATCH is
%   fastest when STRS is a character array.
%
%   I = STRMATCH(STR,STRS,'exact') returns only the indices of the
%   strings in STRS matching STR exactly.
%
%   Examples
%     i = strmatch('max',strvcat('max','minimax','maximum'))
%   returns i = [1; 3] since rows 1 and 3 begin with 'max', and
%     i = strmatch('max',strvcat('max','minimax','maximum'),'exact')
%   returns i = 1, since only row 1 matches 'max' exactly.
%   
%   See also FINDSTR, STRVCAT, STRCMP, STRNCMP.

%   Mark W. Reichelt, 8-29-94
%   Copyright (c) 1984-1998 by The MathWorks, Inc.
%   $Revision: 1.2 $  $Date: 1997/12/12 20:18:30 $

% The cell array implementation is not available

[m,n] = size(strs);
len = length(str);
null = setstr(0); space = ' ';
if (nargin < 3)
  exactFlag = 0;
else
  exactFlag = 1;
end

% Protect against empty STR.
if (len == 0)
  if (~exactFlag)
    i = (1:m)';
  else
    i = [];
  end
  return;
end
  
if len > n
  i = [];
else
  if exactFlag  % if 'exact' flag, pad str with blanks or nulls
    % Use nulls if anything in the last column is a null.
    if any(strs(:,1:n)==null), 
      str = [str null(ones(1,n-len))];
    else
      str = [str space(ones(1,n-len))];
    end
    len = n;
  end
  if len > 1
    i = find(~sum((strs(:,1:len) ~= str(ones(m,1),:))'));
  else 					                % we have to avoid summing a vector
    i = find(~(strs(:,1:len) ~= str(ones(m,1),:)'));
  end  
end
