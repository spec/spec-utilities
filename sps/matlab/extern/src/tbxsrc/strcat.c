/*
 * String concatenation.  If MATLAB_MEX_FILE is defined, str2mat.c is
 * built as a MEX-File, otherwise it's built as a standalone, usable
 * by the C Math Library.
 */

/*********************************************************************/
/*                        R C S  information                         */
/*********************************************************************/

/*
 * CONFIDENTIAL AND CONTAINING PROPRIETARY TRADE SECRETS
 * Copyright 1995 - 1998 The MathWorks, Inc.  All Rights Reserved.
 * The source code contained in this listing contains proprietary and
 * confidential trade secrets of The MathWorks, Inc.   The use, modification,
 * or development of derivative work based on the code or ideas obtained
 * from the code is prohibited without the express written permission of The
 * MathWorks, Inc.  The disclosure of this code to any party not authorized
 * by The MathWorks, Inc. is strictly forbidden.
 * CONFIDENTIAL AND CONTAINING PROPRIETARY TRADE SECRETS
 */

/* $Revision: 1.8 $ */

#ifdef MATLAB_MEX_FILE

#include "mex.h"

#define ERROR_MSG           mexErrMsgTxt
#define OUT_OF_MEMORY()     mexErrMsgTxt("A memory allocation request failed")
#define STATIC_OR_EXTERN

#else

void mlfInitFcn(void);
void mlfCleanupFcn(void);

#include <stdarg.h>

#include "matrix.h"
#include "matlab.h"

#define ERROR_MSG           mxErrMsgTxt
#define OUT_OF_MEMORY()     mxErrMsgTxt("A memory allocation request failed")
#define STATIC_OR_EXTERN    static

extern mxArray * mxCreateConvertedCopy(const mxArray *pa, mxClassID classid);
extern void mxErrMsgTxt(const char *fmt, ...);
STATIC_OR_EXTERN void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]);

/* Concatenate up to MAXARGS number of string matrices. The argument list MUST 
 * be terminated by a zero. For example:
 *
 *    result = mlfStrcat(a, b, c, 0);
 */
mxArray * mlfStrcat(mxArray *a1, ...)
{
    const mxArray **prhs;
    mxArray *array, *result;
    int nrhs = 1, count = 1;
    va_list ap;

    mlfInitFcn();
    /* Count the number of arguments */

    va_start(ap, a1);
    while (va_arg(ap, mxArray *) != (mxArray *)NULL) count++;
    va_end(ap);

    /* Allocate space for the right hand side (input) arguments */

    prhs = mxMalloc(count*sizeof(mxArray *));

    prhs[0] = a1;
    /* Pull the arguments off the stack, and put them into an array */
    va_start(ap, a1);
    while ((array = va_arg(ap, mxArray*)) != (mxArray *)NULL)
    {
        prhs[nrhs++] = array; 
    } 
    va_end(ap);

    mexFunction(1, &result, nrhs, prhs);

    mxFree((mxArray**)prhs);
    mlfCleanupOutputArray(result);
    mlfCleanupFcn();
    return(result);
}
#endif

STATIC_OR_EXTERN void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhsin[])
{
    mxChar *s,*t;
    int     siz[2];
    int     i,j,k,m,n,r,M,N;
    const mxArray **prhs;
    prhs = mxCalloc(nrhs, sizeof(mxArray*));

    /* Check to make sure all inputs are character arrays */
    /* and determine size of the output */
    M = 0;
    N = 0;
    for (j=0; j<nrhs; j++)
    {
        if (!mxIsChar(prhsin[j])) 
        {
            prhs[j] = mxCreateConvertedCopy(prhsin[j], mxCHAR_CLASS);
            if (prhs[j] == NULL) {
                OUT_OF_MEMORY();
            }
        } else {
            prhs[j] = prhsin[j];
        }

        m = mxGetM(prhs[j]);
        if (M == 0 || (M == 1 && m > 1))
          M = m;
        if (m > 1 && m != M)
          ERROR_MSG("Input arguments have same number of rows");
        n = mxGetN(prhs[j]);
        r = 0;
        for (i=0; i<m; i++)
        {
            s = (mxChar *)mxGetData(prhs[j]) + (n-1)*m + i;
            for (k=n; k>r; k--)
            {
                if ((*s!=32) && (*s!=0))
                  break;
                s -= m;
            }
            r = k>r ? k : r;
        }
        N += r;
    }
    
    /* Initialize result to spaces (32) */
    siz[0] = M;
    siz[1] = N;
    plhs[0] = mxCreateCharArray(2,siz);
    t = (mxChar *)mxGetData(plhs[0]);
    for (k=0; k<M*N; k++)
    	*t++ = 32;
    
    for (i=0; i<M; i++)
    {
        t = (mxChar *)mxGetData(plhs[0]) + i;
        for (j=0; j<nrhs; j++)
        {
            m = mxGetM(prhs[j]);
            n = mxGetN(prhs[j]);
            s = (mxChar *)mxGetData(prhs[j]) + (n-1)*m + (m > 1 ? i : 0);
            for (k=n; k>0; k--)
            {
                if ((*s!=32) && (*s!=0))
                  break;
                s -= m;
            }
            s = (mxChar *)mxGetData(prhs[j]) + (m > 1 ? i : 0);
            for (r=0; r<k; r++)
            {
                *t = *s;
                s += m;
                t += M;
            }
        }
    }
    for (j=0; j<nrhs;j++)
    {
        if (prhs[j] != prhsin[j]){
            mxDestroyArray((mxArray*)prhs[j]);
        }
    }
    mxFree((mxArray**)prhs);    
}

void mxStrcat(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    int i;
    mlfInitFcn();
    mexFunction(nlhs, plhs, nrhs, prhs);
    mlfCleanupFcn();
    for (i=0; i<nlhs; i++) {
        mlfCleanupOutputArray(plhs[i]);
    }
}
