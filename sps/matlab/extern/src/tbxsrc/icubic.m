function F=icubic(arg1, arg2, arg3)
%ICUBIC 1-D cubic Interpolation.
%
%   See INTERP1.

%   Copyright (c) 1984-1998 by The MathWorks, Inc.
%   $Revision: 1.3 $  $Date: 1997/12/12 20:18:26 $

warning(sprintf(['ICUBIC is obsolete and will be eliminated in future' ...
    ' versions.\n         Please use INTERP1(...,''*cubic'') instead.']));


error(nargchk(2,3,nargin));

if (nargin==2)
    F = interp1(arg1, arg2, '*cubic');
else
    F = interp1(arg1, arg2, arg3, '*cubic');
end
