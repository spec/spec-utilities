function B = repmat(A,M,N)
%REPMAT Replicate and tile an array.
%   B = REPMAT(A,M,N) replicates and tiles the matrix A to produce the
%   M-by-N block matrix B.
%   
%   B = REPMAT(A,[M N]) produces the same thing.
%
%   REPMAT(A,M,N) when A is a scalar is commonly used to produce
%   an M-by-N matrix filled with A's value.  This can be much faster
%   than A*ONES(M,N) when M and/or N are large.
%   
%   Example:
%       repmat(magic(2),2,3)
%       repmat(NaN,2,3)
%
%   See also MESHGRID.

%   Copyright (c) 1984-1998 by The MathWorks, Inc.
%   $Revision: 1.2 $  $Date: 1997/12/12 20:18:42 $

if nargin == 2
  if prod(size(M))==1
     siz = [M M];
  else
     siz = M;
  end
else
  siz = [M N];
end

if length(A)==1
  % This produces the same answer as B = A(ones(siz)); but uses less memory.
  B = reshape(A(ones(1,siz(1)),ones(1,siz(2))),siz);
else
  [m,n] = size(A);
  mind = (1:m)';
  nind = (1:n)';
  mind = mind(:,ones(1,siz(1)));
  nind = nind(:,ones(1,siz(2)));
  B = A(mind,nind);
end
