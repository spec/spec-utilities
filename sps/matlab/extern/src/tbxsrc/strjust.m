function t = strjust(s)
%STRJUST Justify character array.
%   STRJUST(S) returns a right justified version of the character
%   array S.

%   Copyright (c) 1984-1998 by The MathWorks, Inc.
%   $Revision: 1.2 $  $Date: 1997/12/12 20:18:43 $

if isempty(s==' ' | s==0), t = s; return, end

[m,n] = size(s);
t = repmat(' ',size(s));
for i=1:m,
  ss = deblank(s(i,:));
  t(i,n-length(ss)+1:end) = ss;
end
