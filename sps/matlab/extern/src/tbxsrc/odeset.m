function options = odeset(arg1,arg2,arg3,arg4,arg5,arg6,arg7,arg8,arg9, ...
    arg10,arg11,arg12,arg13,arg14,arg15,arg16,arg17,arg18,arg19, ...
    arg20,arg21,arg22,arg23,arg24,arg25,arg26,arg27,arg28,arg29, ...
    arg30,arg31)
%ODESET Create/alter ODE OPTIONS argument.
%   OPTIONS = ODESET('NAME1',VALUE1,'NAME2',VALUE2,...) creates an
%   integrator options argument OPTIONS in which the named properties have
%   the specified values.  Any unspecified properties have default values.
%   It is sufficient to type only the leading characters that uniquely
%   identify the property.  Case is ignored for property names.
%   
%   OPTIONS = ODESET(OLDOPTS,'NAME1',VALUE1,...) alters an existing options
%   argument OLDOPTS.
%   
%   OPTIONS = ODESET(OLDOPTS,NEWOPTS) combines an existing options argument
%   OLDOPTS with a new options argument NEWOPTS.  Any new properties
%   overwrite corresponding old properties.
%   
%   ODESET with no input arguments displays all property names and their
%   possible values.
%   
%ODESET PROPERTIES
%   
%RelTol - Relative error tolerance  [ positive scalar {1e-3} ]
%   This scalar applies to all components of the solution vector, and
%   defaults to 1e-3 (0.1% accuracy) in all solvers.  The estimated error in
%   each integration step satisfies e(i) <= max(RelTol*abs(y(i)),AbsTol(i)).
%
%AbsTol - Absolute error tolerance  [ positive scalar or vector {1e-6} ]
%   A scalar tolerance applies to all components of the solution vector.
%   Elements of a vector of tolerances apply to corresponding components of
%   the solution vector.  AbsTol defaults to 1e-6 in all solvers.
%   
%Refine - Output refinement factor  [ positive integer ]
%   This property increases the number of output points by the specified
%   factor producing smoother output.  Refine defaults to 1 in all solvers 
%   except ODE45, where it is 4.  Refine doesn't apply if length(TSPAN) > 2.
%   
%OutputFcn - Name of installable output function  [ string ]
%   This output function is called by the solver after each time step.  When
%   a solver is called with no output arguments, OutputFcn defaults to
%   'odeplot'.  Otherwise, OutputFcn defaults to ''.
%   
%OutputSel - Output selection indices  [ vector of integers ]
%   This vector of indices specifies which components of the solution vector
%   are passed to the OutputFcn.  OutputSel defaults to all components.
%   
%Stats - Display computational cost statistics  [ on | {off} ]
%   
%Jacobian - Jacobian available from ODE file  [ on | {off} ]
%   Set this property 'on' if the ODE file is coded so that
%   F(t,y,'jacobian') returns dF/dy.
%   
%JConstant - Constant Jacobian matrix dF/dy  [ on | {off} ]
%   Set this property 'on' if the Jacobian matrix dF/dy is constant.
%   
%JPattern - Jacobian sparsity pattern available from ODE file  [ on | {off} ]
%   Set this property 'on' if the ODE file is coded so F([],[],'jpattern')
%   returns a sparse matrix with 1's showing nonzeros of dF/dy.
%   
%Vectorized - Vectorized ODE file  [ on | {off} ]
%   Set this property 'on' if the ODE file is coded so that F(t,[y1 y2 ...])
%   returns [F(t,y1) F(t,y2) ...].
%   
%Events - Locate events  [ on | {off} ]
%   Set this property 'on' if the ODE file is coded so that F(t,y,'events')
%   returns the values of the event functions.  See ODEFILE.
%   
%Mass - Mass matrix available from ODE file  [ on | {off} ]
%   Set this property 'on' if the ODE file is coded so that F(t,[],'mass')
%   returns M(t).
%   
%MassConstant - Constant mass matrix available from ODE file  [ on | {off} ]
%   Set this property 'on' if the ODE file is coded so that F(t,[],'mass')
%   returns constant mass matrix M.
%   
%MaxStep - Upper bound on step size  [ positive scalar ]
%   MaxStep defaults to one-tenth of the tspan interval in all solvers.
%
%InitialStep - Suggested initial step size  [ positive scalar ]
%   The solver will try this first.  By default the solvers determine an
%   initial step size automatically.
%   
%MaxOrder - Maximum order of ODE15S  [ 1 | 2 | 3 | 4 | {5} ]
%   
%BDF - Use Backward Differentiation Formulas in ODE15S  [ on | {off} ]
%   This property specifies whether the Backward Differentiation Formulas
%   (Gear's methods) are to be used in ODE15S instead of the default
%   Numerical Differentiation Formulas.
%   
%NormControl -  Control error relative to norm of solution  [ on | {off} ]
%   Set this property 'on' to request that the solvers control the error in
%   each integration step with norm(e) <= max(RelTol*norm(y),AbsTol).  By
%   default the solvers use a more stringent component-wise error control.
%   
%   See also ODEGET, ODEFILE, ODE45, ODE23, ODE113, ODE15S, ODE23S.

%   Mark W. Reichelt and Lawrence F. Shampine, 5/6/94
%   Copyright (c) 1984-1998 by The MathWorks, Inc.
%   $Revision: 1.3 $  $Date: 1997/12/12 20:18:38 $

% Print out possible values of properties.
if (nargin == 0) & (nargout == 0)
  fprintf('          AbsTol: [ positive scalar or vector {1e-6} ]\n');
  fprintf('             BDF: [ on | {off} ]\n');
  fprintf('          Events: [ on | {off} ]\n');
  fprintf('     InitialStep: [ positive scalar ]\n');
  fprintf('        Jacobian: [ on | {off} ]\n');
  fprintf('       JConstant: [ on | {off} ]\n');
  fprintf('        JPattern: [ on | {off} ]\n');
  fprintf('            Mass: [ on | {off} ]\n');
  fprintf('    MassConstant: [ on | off ]\n');
  fprintf('        MaxOrder: [ 1 | 2 | 3 | 4 | {5} ]\n');
  fprintf('         MaxStep: [ positive scalar ]\n');
  fprintf('     NormControl: [ on | {off} ]\n');
  fprintf('       OutputFcn: [ string ]\n');
  fprintf('       OutputSel: [ vector of integers ]\n');
  fprintf('          Refine: [ positive integer ]\n');
  fprintf('          RelTol: [ positive scalar {1e-3} ]\n');
  fprintf('           Stats: [ on | {off} ]\n');
  fprintf('      Vectorized: [ on | {off} ]\n');
  fprintf('\n');
  return;
end

Names = [
    'AbsTol      '
    'BDF         '
    'Events      '
    'InitialStep '
    'Jacobian    '
    'JConstant   '
    'JPattern    '
    'Mass        '
    'MassConstant'
    'MaxOrder    '
    'MaxStep     '
    'NormControl '
    'OutputFcn   '
    'OutputSel   '
    'Refine      '
    'RelTol      '
    'Stats       '
    'Vectorized  '
    ];
[m,n] = size(Names);
names = lower(Names);

% Combine all leading options arguments o1, o2, ... in odeset(o1,o2,...).
options = [];
i = 1;
while i <= nargin
  % eval(['arg = arg' num2str(i) ';']);
  % This nasty if, elseif, elseif, ... avoids the eval function.
  if i == 1, arg = arg1;
  elseif i == 2, arg = arg2;
  elseif i == 3, arg = arg3;
  elseif i == 4, arg = arg4;
  elseif i == 5, arg = arg5;
  elseif i == 6, arg = arg6;
  elseif i == 7, arg = arg7;
  elseif i == 8, arg = arg8;
  elseif i == 9, arg = arg9;
  elseif i == 10, arg = arg10;
  elseif i == 11, arg = arg11;
  elseif i == 12, arg = arg12;
  elseif i == 13, arg = arg13;
  elseif i == 14, arg = arg14;
  elseif i == 15, arg = arg15;
  elseif i == 16, arg = arg16;
  elseif i == 17, arg = arg17;
  elseif i == 18, arg = arg18;
  elseif i == 19, arg = arg19;
  elseif i == 20, arg = arg20;
  elseif i == 21, arg = arg21;
  elseif i == 22, arg = arg22;
  elseif i == 23, arg = arg23;
  elseif i == 24, arg = arg24;
  elseif i == 25, arg = arg25;
  elseif i == 26, arg = arg26;
  elseif i == 27, arg = arg27;
  elseif i == 28, arg = arg28;
  elseif i == 29, arg = arg29;
  elseif i == 30, arg = arg30;
  elseif i == 31, arg = arg31;
  end
  
  if isstr(arg)                         % arg is an option name
    break;
  end
  if ~isempty(arg)                      % [] is a valid options argument
    if isempty(options)
      options = arg;
    else
      for j = 1:m
        jnans = [0 find(isnan(arg))];
        val = arg((jnans(j)+1):(jnans(j+1)-1));
        if ~isempty(val)
          inans = [0 find(isnan(options))];
          before = options(1:inans(j));
          after = options(inans(j+1):length(options));
          options = [before val after];
        end
      end
    end
  end
  
  i = i + 1;
end
if isempty(options)
  options = NaN + zeros(1,m);
end

% A finite state machine to parse name-value pairs.
if rem(nargin-i+1,2) ~= 0
  error('Arguments must occur in name-value pairs.');
end
expectval = 0;                          % start expecting a name, not a value
while i <= nargin
  % eval(['arg = arg' num2str(i) ';']);
  % This nasty if, elseif, elseif, ... avoids the eval function.
  if i == 1, arg = arg1;
  elseif i == 2, arg = arg2;
  elseif i == 3, arg = arg3;
  elseif i == 4, arg = arg4;
  elseif i == 5, arg = arg5;
  elseif i == 6, arg = arg6;
  elseif i == 7, arg = arg7;
  elseif i == 8, arg = arg8;
  elseif i == 9, arg = arg9;
  elseif i == 10, arg = arg10;
  elseif i == 11, arg = arg11;
  elseif i == 12, arg = arg12;
  elseif i == 13, arg = arg13;
  elseif i == 14, arg = arg14;
  elseif i == 15, arg = arg15;
  elseif i == 16, arg = arg16;
  elseif i == 17, arg = arg17;
  elseif i == 18, arg = arg18;
  elseif i == 19, arg = arg19;
  elseif i == 20, arg = arg20;
  elseif i == 21, arg = arg21;
  elseif i == 22, arg = arg22;
  elseif i == 23, arg = arg23;
  elseif i == 24, arg = arg24;
  elseif i == 25, arg = arg25;
  elseif i == 26, arg = arg26;
  elseif i == 27, arg = arg27;
  elseif i == 28, arg = arg28;
  elseif i == 29, arg = arg29;
  elseif i == 30, arg = arg30;
  elseif i == 31, arg = arg31;
  end
  
  if ~expectval
    if ~isstr(arg)
      error(sprintf('Expected argument %d to be a string property name.', i));
    end
    mbcharvector(arg);
    lowArg = lower(arg);
    j = v4strmatch(lowArg,names);
    if isempty(j)                       % if no matches
      error(sprintf('Unrecognized property name ''%s''.', arg));
    elseif length(j) > 1                % if more than one match
      % Check for any exact matches (in case any names are subsets of others)
      k = v4strmatch(lowArg,names,'exact');
      if length(k) == 1
        j = k;
      else
        msg = sprintf('Ambiguous property name ''%s'' ', arg);
        msg = [msg '(' deblank(Names(j(1),:))];
        for k = j(2:length(j))'
          msg = [msg ', ' deblank(Names(k,:))];
        end
        msg = sprintf('%s).', msg);
        error(msg);
      end
    end
    inans = [0 find(isnan(options))];
    before = options(1:inans(j));
    after = options(inans(j+1):length(options));
    
    expectval = 1;                      % we expect a value next
    
  else
    if isstr(arg)
      arg = real(arg);
    end
    
    options = [before arg after];
    expectval = 0;
      
  end
  i = i + 1;
end

if expectval
  error(sprintf('Expected value for property ''%s''.', arg));
end





function i = v4strmatch(str,strs,flag)
%STRMATCH Find possible matches for string.
%   I = STRMATCH(STR,STRS) looks through the rows of the character
%   array               of strings STRS to find strings that begin
%   with string STR, returning the matching row indices.  STRMATCH is
%   fastest when STRS is a character array.
%
%   I = STRMATCH(STR,STRS,'exact') returns only the indices of the
%   strings in STRS matching STR exactly.
%
%   Examples
%     i = strmatch('max',strvcat('max','minimax','maximum'))
%   returns i = [1; 3] since rows 1 and 3 begin with 'max', and
%     i = strmatch('max',strvcat('max','minimax','maximum'),'exact')
%   returns i = 1, since only row 1 matches 'max' exactly.
%   
%   See also FINDSTR, STRVCAT, STRCMP, STRNCMP.

%   Mark W. Reichelt, 8-29-94
%   Copyright (c) 1984-1998 by The MathWorks, Inc.
%   $Revision: 1.3 $  $Date: 1997/12/12 20:18:38 $

% The cell array implementation is not available
mbcharvector(str);
mbchar(strs);
[m,n] = size(strs);
len = length(str);
null = setstr(0); space = ' ';
if (nargin < 3)
  exactFlag = 0;
else
  exactFlag = 1;
end

% Protect against empty STR.
if (len == 0)
  if (~exactFlag)
    i = (1:m)';
  else
    i = [];
  end
  return;
end
  
if len > n
  i = [];
else
  if exactFlag  % if 'exact' flag, pad str with blanks or nulls
    % Use nulls if anything in the last column is a null.
    if any(strs(:,1:n)==null), 
      str = [str null(ones(1,n-len))];
    else
      str = [str space(ones(1,n-len))];
    end
    len = n;
  end
  if len > 1
    i = find(~sum((strs(:,1:len) ~= str(ones(m,1),:))'));
  else 					                % we have to avoid summing a vector
    i = find(~(strs(:,1:len) ~= str(ones(m,1),:)'));
  end  
end
