function yinterp = ntrp23(tinterp,t,y,tnew,ynew,h,f,p3)
%NTRP23 Interpolation helper function for ODE23.
%   YINTERP = NTRP23(TINTERP,T,Y,TNEW,YNEW,H,F) uses data computed in ODE23
%   to approximate the solution at time TINTERP.
%   
%   See also ODE23.

%   Mark W. Reichelt and Lawrence F. Shampine, 6-13-94
%   Copyright (c) 1984-1998 by The MathWorks, Inc.
%   $Revision: 1.2 $  $Date: 1997/12/12 20:18:33 $

BI = [
    1       -4/3        5/9
    0       1       -2/3
    0       4/3     -8/9
    0       -1      1
    ];

s = ((tinterp - t) / h)';       % may be a row vector

yinterp = y(:,ones(length(tinterp),1)) + f*(h*BI)*cumprod(s(ones(3,1),:));
