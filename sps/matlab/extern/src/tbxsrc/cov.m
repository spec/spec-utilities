function xy = cov(x,y,flag)
%COV Covariance matrix.
%   COV(X), if X is a vector, returns the variance.  For matrices,
%   where each row is an observation, and each column a variable,
%   COV(X) is the covariance matrix.  DIAG(COV(X)) is a vector of
%   variances for each column, and SQRT(DIAG(COV(X))) is a vector
%   of standard deviations. COV(X,Y), where X and Y are
%   vectors of equal length, is equivalent to COV([X(:) Y(:)]). 
%   
%   COV(X) or COV(X,Y) normalizes by (N-1) where N is the number of
%   observations.  This makes COV(X) the best unbiased estimate of the
%   covariance matrix if the observations are from a normal distribution.
%
%   COV(X,1) or COV(X,Y,1) normalizes by N and produces the second
%   moment matrix of the observations about their mean.  COV(X,Y,0) is
%   the same as COV(X,Y) and COV(X,0) is the same as COV(X).
%
%   The mean is removed from each column before calculating the
%   result.
%
%   See also CORRCOEF, STD, MEAN.

%   J. Little 5-5-86
%   Revised 6-9-88 LS 3-10-94 BJ
%   Copyright (c) 1984-1998 by The MathWorks, Inc.
%   $Revision: 1.2 $  $Date: 1997/12/12 20:18:19 $

nin = nargin;

% Check for cov(x,flag) or cov(x,y,flag)
if nin==3
  % flag is third arg
  nin = 2;
elseif nin==2 & length(y)==1
  flag = y;
  nin = 1;
else
  flag = 0;
end

if nin == 2,
  x = x(:);
  y = y(:);
  if length(x) ~= length(y), 
    error('The lengths of x and y must match.');
  end
  x = [x y];
end

if length(x)==prod(size(x))
  x = x(:);
end

[m,n] = size(x);

if m==1,  % Handle special case
  xy = 0;

else
  xc = x - repmat(sum(x)/m,m,1);  % Remove mean
  if flag
    xy = xc' * xc / m;
  else
    xy = xc' * xc / (m-1);
  end
end

