function x = ifft2(f, mrows, ncols)
%IFFT2 Two-dimensional inverse discrete Fourier transform.
%   IFFT2(F) returns the two-dimensional inverse Fourier transform
%   of matrix F.  If F is a vector, the result will have the same
%   orientation.
%
%   IFFT2(F,MROWS,NCOLS) pads matrix F with zeros to size MROWS-by-NCOLS
%   before transforming.
%
%   See also FFT2, IFFT, FFTSHIFT.

%   J.N. Little 12-18-85
%   Revised 4-15-87 JNL
%   Revised 5-3-90 CRD
%   Copyright (c) 1984-1998 by The MathWorks, Inc.
%   $Revision: 1.2 $  $Date: 1997/12/12 20:18:27 $

[m, n] = size(f);
if nargin < 3, ncols = n; end
if nargin < 2, mrows = m; end

% Transform.
x = conj(fft(fft(conj(f),ncols,2),mrows,1));
x = x/prod(size(x));




