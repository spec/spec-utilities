function f = fft2(x, mrows, ncols)
%FFT2 Two-dimensional discrete Fourier Transform.
%   FFT2(X) returns the two-dimensional Fourier transform of matrix X.
%   If X is a vector, the result will have the same orientation.
%
%   FFT2(X,MROWS,NCOLS) pads matrix X with zeros to size MROWS-by-NCOLS
%   before transforming.
%
%   See also IFFT2, FFT, FFTSHIFT.

%   J.N. Little 12-18-85
%   Revised 4-15-87 JNL
%   Revised 5-3-90 CRD
%   Copyright (c) 1984-1998 by The MathWorks, Inc.
%   $Revision: 1.2 $  $Date: 1997/12/12 20:18:22 $

[m, n] = size(x);
if nargin < 3, ncols = n; end
if nargin < 2, mrows = m; end

% Transform.
f = fft(fft(x,ncols,2),mrows,1);
