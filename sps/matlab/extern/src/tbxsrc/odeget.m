function o = odeget(options,name,default)
%ODEGET Get ODE OPTIONS parameters.
%   VAL = ODEGET(OPTIONS,'NAME') extracts the value of the named property
%   from integrator options argument OPTIONS, returning an empty matrix if
%   the property value is not specified in OPTIONS.  It is sufficient to
%   type only the leading characters that uniquely identify the
%   property.  Case is ignored for property names.  [] is a valid OPTIONS
%   argument.
%   
%   VAL = ODEGET(OPTIONS,'NAME',DEFAULT) extracts the named property as
%   above, but returns VAL = DEFAULT if the named property is not specified
%   in OPTIONS.  For example
%   
%     val = odeget(opts,'RelTol',1e-4);
%   
%   returns val = 1e-4 if the RelTol property is not specified in opts.
%   
%   See also ODESET, ODE45, ODE23, ODE113, ODE15S, ODE23S.

%   Mark W. Reichelt and Lawrence F. Shampine, 3/1/94
%   Copyright (c) 1984-1998 by The MathWorks, Inc.
%   $Revision: 1.3 $  $Date: 1997/12/12 20:18:37 $

if nargin < 2
  error('Not enough input arguments.');
end

if isempty(options)
  if nargin == 3
    o = default;
  else
    o = [];
  end
  return;
end

Names = [
    'AbsTol      '                      % vector
    'BDF         '                      % string
    'Events      '                      % string
    'InitialStep '                      % scalar
    'Jacobian    '                      % string
    'JConstant   '                      % string
    'JPattern    '                      % string
    'Mass        '                      % string
    'MassConstant'                      % string
    'MaxOrder    '                      % scalar
    'MaxStep     '                      % scalar
    'NormControl '                      % string
    'OutputFcn   '                      % string
    'OutputSel   '                      % vector
    'Refine      '                      % scalar
    'RelTol      '                      % scalar
    'Stats       '                      % string
    'Vectorized  '                      % string
    ];
[m,n] = size(Names);
names = lower(Names);

lowName = lower(name);
j = v4strmatch(lowName,names);
if isempty(j)               % if no matches
  error(sprintf(['Unrecognized property name ''%s''.  ' ...
                 'See ODESET for possibilities.'], name));
elseif length(j) > 1            % if more than one match
  % Check for any exact matches (in case any names are subsets of others)
  k = v4strmatch(lowName,names,'exact');
  if length(k) == 1
    j = k;
  else
    msg = sprintf('Ambiguous property name ''%s'' ', name);
    msg = [msg '(' deblank(Names(j(1),:))];
    for k = j(2:length(j))'
      msg = [msg ', ' deblank(Names(k,:))];
    end
    msg = sprintf('%s).', msg);
    error(msg);
  end
end

inans = [0 find(isnan(options))];
o = options(inans(j)+1:inans(j+1)-1);

% scalars, vectors, strings
if any(j == [1 4 10 11 14 15 16])       % scalar or vector
  o = o(:)';
else
  o = setstr(o);
end

if (nargin == 3) & isempty(o)
  o = default;
end



function i = v4strmatch(str,strs,flag)
%STRMATCH Find possible matches for string.
%   I = STRMATCH(STR,STRS) looks through the rows of the character
%   array               of strings STRS to find strings that begin
%   with string STR, returning the matching row indices.  STRMATCH is
%   fastest when STRS is a character array.
%
%   I = STRMATCH(STR,STRS,'exact') returns only the indices of the
%   strings in STRS matching STR exactly.
%
%   Examples
%     i = strmatch('max',strvcat('max','minimax','maximum'))
%   returns i = [1; 3] since rows 1 and 3 begin with 'max', and
%     i = strmatch('max',strvcat('max','minimax','maximum'),'exact')
%   returns i = 1, since only row 1 matches 'max' exactly.
%   
%   See also FINDSTR, STRVCAT, STRCMP, STRNCMP.

%   Mark W. Reichelt, 8-29-94
%   Copyright (c) 1984-1998 by The MathWorks, Inc.
%   $Revision: 1.3 $  $Date: 1997/12/12 20:18:37 $

% The cell array implementation is not available

[m,n] = size(strs);
len = length(str);
null = setstr(0); space = ' ';
if (nargin < 3)
  exactFlag = 0;
else
  exactFlag = 1;
end

% Protect against empty STR.
if (len == 0)
  if (~exactFlag)
    i = (1:m)';
  else
    i = [];
  end
  return;
end
  
if len > n
  i = [];
else
  if exactFlag  % if 'exact' flag, pad str with blanks or nulls
    % Use nulls if anything in the last column is a null.
    if any(strs(:,1:n)==null), 
      str = [str null(ones(1,n-len))];
    else
      str = [str space(ones(1,n-len))];
    end
    len = n;
  end
  if len > 1
    i = find(~sum((strs(:,1:len) ~= str(ones(m,1),:))'));
  else 					                % we have to avoid summing a vector
    i = find(~(strs(:,1:len) ~= str(ones(m,1),:)'));
  end  
end
