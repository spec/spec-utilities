/*
 * Vertically concatenate of strings.  If MATLAB_MEX_FILE is defined, 
 * str2mat.c is built as a MEX-File, otherwise it's built as a standalone, 
 * usable by the C Math Library.
 */

/*********************************************************************/
/*                        R C S  information                         */
/*********************************************************************/

/*
 * CONFIDENTIAL AND CONTAINING PROPRIETARY TRADE SECRETS
 * Copyright 1995 - 1998 The MathWorks, Inc.  All Rights Reserved.
 * The source code contained in this listing contains proprietary and
 * confidential trade secrets of The MathWorks, Inc.   The use, modification,
 * or development of derivative work based on the code or ideas obtained
 * from the code is prohibited without the express written permission of The
 * MathWorks, Inc.  The disclosure of this code to any party not authorized
 * by The MathWorks, Inc. is strictly forbidden.
 * CONFIDENTIAL AND CONTAINING PROPRIETARY TRADE SECRETS
 */

/* $Revision: 1.8 $ */


#ifdef MATLAB_MEX_FILE

#include "mex.h"

#define ERROR_MSG           mexErrMsgTxt
#define OUT_OF_MEMORY()     mexErrMsgTxt("A memory allocation request failed")
#define STATIC_OR_EXTERN

#ifdef PUBLIC
#undef PUBLIC
#endif
#define PUBLIC

#else

void mlfInitFcn(void);
void mlfCleanupFcn(void);

#include <stdarg.h>

#include "matrix.h"
#include "matlab.h"

#define ERROR_MSG           mxErrMsgTxt
#define OUT_OF_MEMORY()     mxErrMsgTxt("A memory allocation request failed")
#define STATIC_OR_EXTERN    static

extern mxArray * mxCreateConvertedCopy(const mxArray *pa, mxClassID classid);
extern void mxErrMsgTxt(const char *fmt, ...);
STATIC_OR_EXTERN void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]);

/*
 * Pull all of the arguments into the argument list and
 * pass it on to the local version of the mexFunction (above)
 *
 * You might think that you could omit the nrhs argument and put the code
 * that counts the number of arguments in the va_list here, but you can't.
 * The WATCOM compiler does not allow you to make copies of va_alist
 * variables (they are implemented as arrays).
 */
static mxArray * vmlfStrvcat(mxArray *RI1, va_list ap, int nrhs)
{
    const mxArray **prhs;
    mxArray *plhs[1];
    int i;
    
    mlfInitFcn();

    prhs = (const mxArray **) mxCalloc( sizeof(mxArray *), nrhs );

    prhs[0] = RI1;
    
    for (i = 1; i<nrhs; i++)
    {
        prhs[i] = va_arg(ap, mxArray *);
    }
    
    mexFunction(1,plhs,nrhs,prhs);

    mxFree((mxArray**)prhs);
    
    mlfCleanupOutputArray(plhs[0]);
    mlfCleanupFcn();

    return(plhs[0]);
}

/*
 * Retrieve the argument list and pass it to vmlfStrvcat
 */
extern mxArray * mlfStrvcat(mxArray *RI1, ...)
{
    va_list ap;
    mxArray *retval;
    int nrhs=1; 

    /* Count the number of input arguments */
    va_start (ap, RI1);
    while(va_arg(ap, mxArray *) != 0) nrhs++; 
    va_end(ap);

    va_start(ap, RI1);
    retval = vmlfStrvcat( RI1, ap, nrhs );
    va_end (ap);

    return(retval);
}

/*
 * Retrieve the argument list and pass it to vmlfStrvcat
 */
extern mxArray * mlfChar(mxArray *RI1, ...)
{
    va_list ap;
    mxArray *retval;
    int nrhs=1; 

    /* Count the number of input arguments */
    va_start (ap, RI1);
    while(va_arg(ap, mxArray *) != 0) nrhs++; 
    va_end(ap);

    va_start(ap, RI1);
    retval = vmlfStrvcat( RI1, ap, nrhs );
    va_end (ap);

    return(retval);
}

#endif

STATIC_OR_EXTERN void mexFunction(int nlhs,mxArray *plhs[],int nrhs,const mxArray *prhs[])
{
    mxChar *s,*t;
    const mxArray *src;
    int     siz[2];
    int     i,j,k,m,r,M,N;
    
    /* Check to make sure all inputs are character arrays */
    /* and determine size of the output */
    M = 0;
    N = 0;
    for (j=0; j<nrhs; j++)
    {
        M += mxGetM(prhs[j]);
        N = mxGetN(prhs[j]) > N ? mxGetN(prhs[j]) : N;
    }
    
    /* Initialize result to spaces (32) */
    siz[0] = M;
    siz[1] = N;
    plhs[0] = mxCreateCharArray(2,siz);
    t = (mxChar *)mxGetData(plhs[0]);
    for (k=0; k<M*N; k++)
    	*t++ = 32;
    	
    r = 0;
    for (j=0; j<nrhs; j++)
    {
        /* If input matrix isn't char, make temporary converted copy */
        src = prhs[j];
        if (!mxIsChar(src))
        {
            src = mxCreateConvertedCopy(src, mxCHAR_CLASS);
            if (src == NULL) 
            {
                OUT_OF_MEMORY();
            }
        }
        m = mxGetM(src);
        for (i=0; i<m; i++)
        {
            s = (mxChar *)mxGetData(src) + i; /* beginning of source row */
            t = (mxChar *)mxGetData(plhs[0]) + r; /* beginning of dest row */
            for (k=0; k<mxGetN(src); k++)
            {
                *t = *s;
                s += m; /* Go to the next character in the source row */
                t += M; /* Go to the next character in the dest row */
                
            }
            r++;
        }
        /* Clean up temporary converted copy */
        if (src != prhs[j])
        {
            mxDestroyArray((mxArray*)src);
        }
    }
}

PUBLIC void mxStrvcat(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    int i;
    mlfInitFcn();
    mexFunction(nlhs, plhs, nrhs, prhs);
    mlfCleanupFcn();
    for (i=0; i<nlhs; i++) {
        mlfCleanupOutputArray(plhs[i]);
    }
}
