function x = str2num(str)
%STR2NUM String to number conversion
%	This is a replacement for the STR2NUM distributed with MATLAB 4.2c.
%	It does not use EVAL.
%
% 	X = STR2NUM(S)  converts the string S, which should be an
% 	ASCII character representation of a numeric value, to MATLAB's
% 	numeric representation.  The string may contain digits, a decimal
% 	point, a leading + or - sign, an 'e' preceeding a power of 10 scale
% 	factor, and an 'i' for a complex unit.
% 
% 	STR2NUM also converts matrix expressions formed from numeric
%	expressions, commas, blanks, semicolons and square brackets.
% 
% 	If the string S does not represent a valid number or matrix,
% 	STR2NUM(S) returns the empty matrix.
%
% 	See also NUM2STR, HEX2NUM.

%	C. Moler, 5/27/95.
%	Copyright (c) 1984-1998 by The MathWorks, Inc.
% $Revision: 1.8 $

mbchar(str);
x = [];
if (~isempty(str)) 
   kmax = 0;
   rowsize = 0;
   sig = 1;
   i = 1;
   r = 0;
   t = 0;
   dec = 0;
   ten = 10.;
   v = 0;
   braces = 0;   % number of unmatched braces
   siz = size(str);
   err = 0;
   for ind = 1:siz(1),
      if (err) break; end
      s = str(ind,:);
      s = [s ' '];
      kmax = length(s);
      j = 1;	
      k = 1;
      pm = 0;
      inrow = 0;  % have started reading a row
      imag = 0;   % number is imaginary
      exp = 0;    % reading an exponent
      while k <= kmax 
         c = s(k);
         if ('0'<=c) & (c<='9')
            t = ten*t + (c-'0');
            v = 1;
            inrow = 1;
            if dec
               dec = ten*dec;
            end
         elseif c == '.'
            dec = 1;
         elseif (v == 0) & (imag == 0) & (exp ==0) & ...
                ((c == 'n') | (c == 'N') | ((c == 'i') & (s(k+1) == 'n')) |...
                 (c == 'I') | (c == 'p') | ((c == 'e') & (s(k+1) == 'p')))
            % pi, inf, Inf, nan, NaN, eps
            cmax = kmax - k + 1;
            vc = 0;
            if (cmax >= 3)
                const = s(k:k+1);
                if (strcmp(const, 'pi') == 1 & ~isletter(s(k+2)))
                    t = t+pi;
                    vc = 1;
                    k = k+1;
                end
            end
            if (~vc & cmax >= 4)
                const = s(k:k+2);
                if (~isletter(s(k+3)))
                    vc = 1;
                    if strcmp(const, 'inf') | strcmp(const, 'Inf')
                        t = t+inf;
                    elseif strcmp(const, 'NaN') | strcmp(const, 'nan')
                        t = t+nan;
                    elseif strcmp(const, 'eps')
                        t = t+eps;
                    else
                        vc = 0;
                    end
                end
                if (vc) k = k+2; end
            end
            if (~vc)
                x = [];
                err = 1;
                break;   
            else
                v = 1;
                inrow = 1;
            end         
         elseif (c == 'i') | (c == 'j') 
            if imag
               x = [];
               err = 1;
               break;
            end
            imag = 1;
            inrow = 1;
            sig = sig*1i;
            if (t == 0) 
               v = 1;
               t = 1;
            end
         elseif ((c == 'e') | (c == 'E') | (c == 'd') | (c == 'D')) & (v == 1)
            if dec, t = sig*t/dec; else, t = sig*t; end
            dec = 0;
            sig = 1;
            if s(k+1) == '-'
               sig = -1;
               k = k+1;
            elseif s(k+1) == '+'
               sig = 1;
               k = k+1;
            elseif (s(k+1) < '0' | s(k+1) > '9')               
               x = [];
               err = 1;
               break;
            end
            if (s(k+1) < '0' | s(k+1) > '9')
                x = [];
                err = 1;
                break;
            end
            while ('0'<=s(k+1) & s(k+1)<='9')
               dec = ten*dec + (s(k+1)-'0');
               k = k+1;
            end
            t = t*realpow(10,sig*dec);
            sig = 1;
            dec = 0;
            exp = 1;
         elseif (c == '-') | (c == '+')
            if dec, r = sig*t/dec; else, r = sig*t; end
            t = 0;
            dec = 0;
            f = 1;
            sig = (c == '+') - (c == '-');
            pm = 1;
            v = 0;
         elseif (c == ' ') | (c == ',') | (c == ';') |...
                (double(c) >=0 & double(c) < ' ' )
            [nt,ntpos] = next_token(s, k);
            if ((nt == '+' | nt == '-') & s(ntpos+1) ~= ' ') nt = ' '; end
            if v 
               if (c ~= ' ' | (nt ~= '+') & (nt ~= '-'))
                  if dec, t = sig*t/dec; else, t = sig*t; end
                  if (rowsize > 0 & j > rowsize) 
                     x = [];
                     err = 1;
                     break;
                  end
                  x(i,j) = r + t;
                  r = 0;
                  t = 0;
                  dec = 0;
                  sig = 1;
                  exp = 0;
                  imag = 0;
               end
               v = 0;
               pm = 0;
            end
            if inrow & ((nt ~= '+') & (nt ~= '-')) & (pm == 0)
               if c == ';'
                  i = i+1;
                  if (rowsize == 0) rowsize = j-1; end
                  j = 1;
                  inrow = 0;
               else
                  j = j+1;
               end
            end
            % Ensure matching braces
         elseif (c == '[') | (c == ']')
            if (c == '[') braces = braces+1;
            elseif (c == ']') braces = braces-1; end
            if (braces < 0) 
               x = [];
               err = 1;
               break;
            end
      
         else
            x = [];
            err = 1;
            break;       
         end %if ('0' <= c...
           
         k = k + 1;
      end %while  
      if (braces ~= 0) 
        x = []; 
        break;
      end
      i = i+1;
   end % for
   if (k < kmax) x = []; end
   
   if (rowsize & j <= rowsize)
      x = [];
   end
end % !isempty(str)

function [n,j] = next_token(s,i)
    n = ' ';
    for j = (i+1):length(s),
       n = s(j);
       if (n ~= ' ') break;
    end
end


