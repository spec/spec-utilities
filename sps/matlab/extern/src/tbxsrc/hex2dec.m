function d = hex2dec(h)
%HEX2DEC Convert hexadecimal string to decimal integer.
%   HEX2DEC(H) interprets the hexadecimal string H and returns the
%   equivalent decimal number.  
%  
%   If H is a character array, each row is interpreted as a hexadecimal string.
%
%   Examples
%       hex2dec('12B') and hex2dec('12b') both return 299
%
%   See also DEC2HEX, HEX2NUM, BIN2DEC, BASE2DEC.

%   Author: L. Shure, Revised: 12-23-91, CBM.
%   Copyright (c) 1984-1998 by The MathWorks, Inc.
%   $Revision: 1.3 $  $Date: 1997/12/12 20:18:25 $

% if iscellstr(h), h = char(h); end
if isempty(h), d = []; return, end

if ~isempty(find(h==' ' | h==0)), 
  h = strjust(h);
  h(h==' ' | h==0) = '0';
end

[m,n]=size(h);

sixteen = 16;
p = fliplr(cumprod([1 sixteen(ones(1,n-1))]));
p = p(ones(m,1),:);

d = h <= 64; % Numbers
h(d) = h(d) - 48;

d =  h > 64 & h <= 96; % Uppercase letters
h(d) = h(d) - 55;

d = h > 96; % Lowercase letters
h(d) = h(d) - 87;

d = sum(h.*p,2);
