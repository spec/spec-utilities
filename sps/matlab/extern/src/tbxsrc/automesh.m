function tf = automesh(arg1,arg2,arg3)
%AUTOMESH True if the inputs should be automatically meshgridded.
%    AUTOMESH(X,Y) returns true if X and Y are vectors of
%    different orientations.
%
%    AUTOMESH(X,Y,Z) returns true if X,Y,Z are vectors of
%    different orientations.
%
%    AUTOMESH(...) returns true if all the inputs are vectors of
%    different orientations.

%   Copyright (c) 1984-1998 by The MathWorks, Inc.
%   $Revision: 1.2 $ $Date: 1997/12/12 20:18:17 $

% True if inputs are all vectors, and their non-singleton
% dimensions aren't along the same dimension.

if nargin==1
  tf = (min(size(arg1)) == 1);
elseif nargin==2
  tf = (min(size(arg1)) == 1) & ...
       (min(size(arg2)) == 1) & ...
       (~isequal(size(arg1),size(arg2)));
else
  tf = (min(size(arg1)) == 1) & ...
       (min(size(arg2)) == 2) & ...
       (min(size(arg3)) == 1) & ...
       (~isequal(size(arg1),size(arg2))) & ...
       (~isequal(size(arg1),size(arg3))) & ...
       (~isequal(size(arg2),size(arg3)));
end
