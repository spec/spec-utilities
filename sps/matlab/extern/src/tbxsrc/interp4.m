function F = interp4(arg1,arg2,arg3,arg4,arg5)
%INTERP4 2-D bilinear data interpolation.
%
%   See INTERP2.

%   Copyright (c) 1984-1998 by The MathWorks, Inc.
%   $Revision: 1.2 $  $Date: 1997/12/12 20:18:29 $

warning(sprintf(['INTERP4 is obsolete and will be eliminated in future' ...
    ' versions.\n         Please use INTERP2(...,''*linear'') instead.']));

n = nargin;
error(nargchk(1,5,n));

if (n == 1)
  F = interp2(arg1,'*linear');
elseif (n == 2)
  F = interp2(arg1,arg2,'*linear');
elseif (n == 3)
  F = interp2(arg1,arg2,arg3,'*linear');
elseif (n == 4)
  F = interp2(arg1,arg2,arg3,arg4,'*linear');
elseif (n == 5)
  F = interp2(arg1,arg2,arg3,arg4,arg5,'*linear');
end
