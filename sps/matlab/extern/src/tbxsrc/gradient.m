function [fx,fy] = gradient(f,hx,hy)
%GRADIENT Approximate gradient.
%   [FX,FY] = GRADIENT(F) returns the numerical gradient of the
%   matrix F. FX corresponds to dF/dx, the differences in the
%   x (column) direction. FY corresponds to dF/dy, the differences
%   in the y (row) direction. The spacing between points in each
%   direction is assumed to be one. When F is a vector, DF = GRADIENT(F)
%   is the 1-D gradient.
%
%   [FX,FY] = GRADIENT(F,H), where H is a scalar, uses H as the
%   spacing between points in each direction.
%
%   [FX,FY] = GRADIENT(F,HX,HY), when F is 2-D, uses the spacing
%   specified by HX and HY. HX and HY can either be scalars to specify
%   the spacing between coordinates or vectors to specify the
%   coordinates of the points.  If HX and HY are vectors, their length
%   must match the cooresponding dimension of F.
%
%   Examples:
%       [x,y] = meshgrid(-2:.2:2, -2:.2:2);
%       z = x .* exp(-x.^2 - y.^2);
%       [px,py] = gradient(z,.2,.2);
%       contour(z),hold on, quiver(px,py), hold off
%
%   See also DIFF, DEL2.

%   D. Chen, 16 March 95
%   Copyright (c) 1984-1998 by The MathWorks, Inc.
%   $Revision: 1.3 $  $Date: 1997/12/12 20:18:24 $

% Handle the column vector case
if nargout<=1 & size(f,2)==1,
  ndim = 1;
else
  ndim = 2;
end

for k = 1:ndim
   [n,p] = size(f);
   if k==1 & nargin==3
      h = hy(:);
   elseif nargin>=2
      h = hx(:);
   else
      h = 1;
   end
   if length(h)==1
      h = h*(1:n)';
   end
   g  = zeros(size(f)); % case of singleton dimension

   % Take forward differences on left and right edges
   if n > 1
      g(1,:) = (f(2,:) - f(1,:))/(h(2)-h(1));
      g(n,:) = (f(n,:) - f(n-1,:))/(h(end)-h(end-1));
   end

   % Take centered differences on interior points
   if n > 2
      h = h(3:n) - h(1:n-2);
      g(2:n-1,:) = (f(3:n,:)-f(1:n-2,:))./h(:,ones(p,1));
   end

   if k==1, fy=g; else, fx=g'; end

   % Set up for next pass through the loop
   if k==1, f = f'; end
end 

if ndim==1, fx = fy; end
