function [e,cnt] = normest(S,tol)
%NORMEST Estimate the matrix 2-norm.
%   NORMEST(S) is an estimate of the 2-norm of the matrix S.
%   NORMEST(S,tol) uses relative error tol instead of 1.e-6.
%   [nrm,cnt] = NORMEST(..) also gives the number of iterations used.
%
%   This function is intended primarily for sparse matrices,
%   although it works correctly and may be useful for large, full
%   matrices as well.  Use NORMEST when your problem is large
%   enough that NORM takes too long to compute and an approximate
%   norm is acceptable.
%
%   See also NORM, COND, CONDEST.

%   C. Moler, 4-30-91/92, 6-27-95.
%   Copyright (c) 1984-1998 by The MathWorks, Inc.
%   $Revision: 1.2 $  $Date: 1997/12/12 20:18:31 $

if nargin < 2, tol = 1.e-6; end
x = sum(abs(S))';
e = norm(x);
x = x/e;
cnt = 0;
e0 = 0;
while abs(e-e0) > tol*e
   e0 = e;
   Sx = S*x;
   if norm(Sx,1) == 0
      Sx = rand(size(x));
   end
   e = norm(Sx);
   x = S'*Sx;
   x = x/norm(x);
   cnt = cnt+1;
end
