/*
 * String-to-matrix.  If MATLAB_MEX_FILE is defined, str2mat.c is
 * built as a MEX-File, otherwise it's built as a standalone, usable
 * by the C Math Library
 */

/*********************************************************************/
/*                        R C S  information                         */
/*********************************************************************/

/*
 * CONFIDENTIAL AND CONTAINING PROPRIETARY TRADE SECRETS
 * Copyright 1995 - 1998 The MathWorks, Inc.  All Rights Reserved.
 * The source code contained in this listing contains proprietary and
 * confidential trade secrets of The MathWorks, Inc.   The use, modification,
 * or development of derivative work based on the code or ideas obtained
 * from the code is prohibited without the express written permission of The
 * MathWorks, Inc.  The disclosure of this code to any party not authorized
 * by The MathWorks, Inc. is strictly forbidden.
 * CONFIDENTIAL AND CONTAINING PROPRIETARY TRADE SECRETS
 */

/* $Revision: 1.6 $ */

#ifdef MATLAB_MEX_FILE

#include "mex.h"

#define ERROR_MSG           mexErrMsgTxt
#define OUT_OF_MEMORY()     mexErrMsgTxt("A memory allocation request failed")
#define STATIC_OR_EXTERN    

#else

void mlfInitFcn(void);
void mlfCleanupFcn(void);

#include <stdarg.h>

#include "matrix.h"
#include "matlab.h"

#define ERROR_MSG           mxErrMsgTxt
#define OUT_OF_MEMORY()     mxErrMsgTxt("A memory allocation request failed")
#define STATIC_OR_EXTERN    static

extern void mxErrMsgTxt(const char *fmt, ...);
extern mxArray * mxCreateConvertedCopy(const mxArray *pa, mxClassID classid);
STATIC_OR_EXTERN void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]);

/* Concatenate up to MAXARGS number of string matrices. The argument list MUST 
 * be terminated by a zero. For example:
 *
 *    result = mlfStrcat(a, b, c, 0);
 */
extern mxArray * mlfStr2mat(mxArray *a1, ...)
{
    const mxArray **prhs;
    mxArray *array, *result;
    int nrhs = 1, count = 1;  /* always have at least one argument */
    va_list ap;

    mlfInitFcn();
    /* Count the number of arguments */

    va_start(ap, a1);
    while (va_arg(ap, mxArray *) != (mxArray *)NULL) count++;
    va_end(ap);

    /* Allocate space for the right hand side (input) arguments */

    prhs = mxMalloc(count*sizeof(mxArray *));

    prhs[0] = a1;

    /* Pull the arguments off the stack, and put them into an array */
    va_start(ap, a1);
    while ((array = va_arg(ap, mxArray*)) != (mxArray *)NULL)
    {
        prhs[nrhs++] = array; 
    } 
    va_end(ap);

    mexFunction(1, &result, nrhs, prhs);

    mxFree((mxArray**)prhs);
    mlfCleanupOutputArray(result);
    mlfCleanupFcn();
    return(result);
}

#endif

STATIC_OR_EXTERN void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    mxChar *s,*t;
    mxArray *src;
    int     siz[2];
    int     i,j,k,m,r,M,N;
    
    /* Determine size of the output */
    M = 0;
    N = 0;
    for (j=0; j<nrhs; j++)
    {
        M += mxGetM(prhs[j]) > 0 ? mxGetM(prhs[j]) : 1;
        N = mxGetN(prhs[j]) > N ? mxGetN(prhs[j]) : N;
    }
    
    /* Initialize result to spaces (32) */
    siz[0] = M;
    siz[1] = N;
    plhs[0] = mxCreateCharArray(2,siz);
    t = (mxChar *)mxGetData(plhs[0]);
    for (k=0; k<M*N; k++)
    	*t++ = 32;
    	
    r = 0;
    for (j=0; j<nrhs; j++)
    {
        /* if input isn't char, make temporary converted copy */
        src = (mxArray*)prhs[j];
        if (!mxIsChar(src)) 
        {
            src = mxCreateConvertedCopy(src, mxCHAR_CLASS);
            if (src == NULL) 
            {
                OUT_OF_MEMORY();
            }
        }
        m = mxGetM(src);
        for (i=0; i<m; i++)
        {
            s = (mxChar *)mxGetData(src) + i; /* beginning of source row */
            t = (mxChar *)mxGetData(plhs[0]) + r; /* beginning of dest row */
            for (k=0; k<mxGetN(src); k++)
            {
                *t = *s;
                s += m; /* Go to the next character in the source row */
                t += M; /* Go to the next character in the dest row */
            }
            r++;
        }
        if (m==0)
          r++;

        /* Clean up temporary copy */
        if (src != prhs[j]) 
        {
            mxDestroyArray(src);
        }
    }
}
