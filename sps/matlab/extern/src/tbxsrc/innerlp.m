function Q=innerlp(outvar, p) 
%INNERLP Used with DBLQUAD to evaluate inner loop of integral.
%   Do not call this function directly; instead, call DBLQUAD, which then
%   calls this function. 
%
%   Q = INNERLP(OUTVAR,[INMIN,INMAX,TOL,length(METHOD), ...
%               double(INTFCN),double(METHOD)])
%   OUTVAR is the value(s) of the outer variable at which 
%   evaluation is desired, passed directly by QUAD. 
%   INTFCN is the name of the object function, passed indirectly 
%   from DBLQUAD. INMIN and INMAX are the integration limits for 
%   the inner variable, passed indirectly from DBLQUAD. 
%   TOL is passed to QUAD (QUAD8) when evaluating the inner 
%   loop, passed indirectly from DBLQUAD. The string METHOD determines 
%   what quadrature method is used, such as QUAD, QUAD8 or some
%   user-defined method.

%   Copyright (c) 1984-1998 by The MathWorks, Inc.
%   $Revision: 1.2 $  $Date: 1997/12/12 20:18:27 $

% Preallocate memory for the output variable. 
Q = ones(size(outvar)); 

% Evaluate the integrand function at each specified value of the 
% outer variable. 

inmin = p(1);
inmax = p(2);
if isfinite(p(3))
   tol = p(3);
else
   tol = [];
end
lenf = p(4);
intfcn = setstr(p(5:lenf+4));
method = setstr(p(lenf+5:end));
trace = [];     % No trace for dblquad

if isequal(method, 'quad')
  for i=1:length(outvar) 
    Q(i)=quad(intfcn, inmin, inmax, tol, trace, outvar(i)); 
  end 
elseif isequal(method, 'quad8')
  for i=1:length(outvar) 
    Q(i)=quad8(intfcn, inmin, inmax, tol, trace, outvar(i)); 
  end 
else % call other user-defined quadrature method
  for i=1:length(outvar) 
    Q(i)=feval(method, intfcn, inmin, inmax, tol, trace, outvar(i)); 
  end 
end
