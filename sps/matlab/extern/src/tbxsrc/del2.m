function v = del2(f,hx,hy)
%DEL2 Discrete Laplacian.
%   L = DEL2(U) when U is a matrix, is an discrete approximation of
%   0.25*del^2 u = (d^2u/dx^2 + d^2/dy^2)/4.  The matrix L is the same
%   size as U with each element equal to the difference between an 
%   element of U and the average of its four neighbors.
%
%   L = DEL2(U,H), where H is a scalar, uses H as the spacing between
%   points in each direction (H=1 by default).
%
%   L = DEL2(U,HX,HY) when U is 2-D, uses the spacing specified by HX
%   and HY. If HX is a scalar, it gives the spacing between points in
%   the x-direction. If HX is a vector, it must be of length SIZE(U,2)
%   and specifies the x-coordinates of the points.  Similarly, if HY
%   is a scalar, it gives the spacing between points in the
%   y-direction. If HY is a vector, it must be of length SIZE(U,1) and
%   specifies the y-coordinates of the points.
%
%   See also GRADIENT, DIFF.

%   D. Chen, 16 March 95
%   Copyright (c) 1984-1998 by The MathWorks, Inc.
%   $Revision: 1.2 $  $Date: 1997/12/12 20:18:21 $

% Loop over each dimension. Permute so that the del2 is always taken along
% the columns.

v = zeros(size(f));
for k = 1:2
   [n,p] = size(f);
   if k==1 & nargin==3
      h = hy(:);
   elseif nargin>=2
      h = hx(:);
   else
      h = 1;
   end
   if length(h)==1
      h = h*(1:n)';
   end
   g  = zeros(size(f)); % case of singleton dimension

   % Take forward second differences on left and right edges (based on
   % a third order approximation)
   if n > 2
      g(1,:) = (f(1,:)-2*f(2,:)+f(3,:))./(h(3)-h(1));
      g(n,:) = (f(n,:)-2*f(n-1,:)+f(n-2,:))./(h(n)-h(n-2));
   end

   % Take centered second differences on interior points
   if n > 2
      h = h(3:n) - h(1:n-2);
      g(2:n-1,:) = (f(3:n,:)-2*f(2:n-1,:)+f(1:n-2,:))./h(:,ones(p,1));
   end

   if k==1
     v = v + g;
   else
     v = v + g';
   end

   % Set up for next pass through the loop
   if k==1, f = f'; end
end 
v = v/2;
