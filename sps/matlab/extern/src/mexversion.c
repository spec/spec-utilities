/* $Revision: 1.5 $ */
#ifdef __cplusplus
extern "C" {
#endif

#ifndef VERSION

#ifdef V4_COMPAT
#define VERSION "MATLAB 4 compatible"
#else
#define VERSION "MATLAB 5.2 native"
#endif /* V4_COMPAT */

#endif /* VERSION */

#ifdef ARRAY_ACCESS_INLINING
#define INLINE " (inlined)"
#else
#define INLINE
#endif /* ARRAY_ACCESS_INLINING */

static char *version = VERSION INLINE;

char *mexVersion () {
/* mex version information */
return version;
}

#ifdef __cplusplus
}
#endif

