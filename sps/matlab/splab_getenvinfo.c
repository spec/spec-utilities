#include "mex.h"
#include "sps.h"

static void set_val (mxArray *structure, char* field, double value)
{
  mxArray *tmparray;
  double *tmpdptr;
  tmparray = mxCreateDoubleMatrix(1, 1, mxREAL);
  tmpdptr = mxGetPr(tmparray);
  *tmpdptr = value;
  mxSetField (structure, 0, field, tmparray);
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  int rows, cols;
  char *spec_version, vbuf[512], array_name[512], *array;
  char eb[512];
  char *fields[] = {"axistitles", "nopts", "title", "xlabel", "xmax", "xmin",
		    "ylabel" };
  int dims[2];
  char* value;
  double dtmp, xmin;
  char tempbuf [700];
  int i;

  
  if (nrhs != 2) 
    mexErrMsgTxt ("Usage: splab_getenvinfo <specversion> <array_name>");

  if (nlhs > 1) 
    mexErrMsgTxt ("Only one output cell array allowed");
    
  if (mxIsChar(prhs[0]) != 1 || mxIsChar(prhs[1]) != 1 ||
      mxGetM(prhs[0]) != 1 || mxGetM(prhs[1]) != 1 )
    mexErrMsgTxt ("Input parameters must be row vectors of type string!");
  
  mxGetString(prhs[0], vbuf, 512);
  if (strcmp(vbuf,"*") == 0)
    spec_version = NULL;
  else
    spec_version = vbuf;
  mxGetString(prhs[1], array_name, 512);

  /* Append _ENV to the array name to get the name of the env shared array */
  if ((array = (char *) malloc (strlen (array_name) + 1 + strlen ("_ENV"))) 
      == 0) 
    mexErrMsgTxt ("No memory left < 100!!");
  strcpy (array, array_name);
  strcat (array, "_ENV");

  dims[0] = 1; dims[1] = 1;
  plhs[0] = mxCreateStructArray (2, dims, 7, fields); 

  if ((value = SPS_GetEnvStr (spec_version, array, "nopts")) != NULL) 
    set_val(plhs[0], "nopts", (double) atoi (value)); 
  else {
    if (SPS_GetArrayInfo (spec_version, array_name, &rows, NULL,NULL,NULL))
      set_val(plhs[0], "nopts", (double) -1); 
    else
      set_val(plhs[0], "nopts", (double) rows); 
  }
  
  if ((value = SPS_GetEnvStr (spec_version, array, "xmin")) != NULL) 
    sscanf(value, "%lf", &dtmp);
  else 
    dtmp = 0.;
  set_val(plhs[0], "xmin", dtmp); 
  xmin = dtmp;

  if ((value = SPS_GetEnvStr (spec_version, array, "xmax")) != NULL) 
    sscanf(value, "%lf", &dtmp);
  else {
    dtmp = xmin;
  }
  set_val(plhs[0], "xmax", dtmp); 

  if ((value = SPS_GetEnvStr (spec_version, array, "title")) != NULL) 
    mxSetField (plhs[0], 0, "title", mxCreateString(value));
  else 
    mxSetField (plhs[0], 0, "title", mxCreateString(array_name));

  if ((value = SPS_GetEnvStr (spec_version, array, "xlabel")) != NULL) 
    mxSetField (plhs[0], 0, "xlabel", mxCreateString(value));
  else 
    mxSetField (plhs[0], 0, "xlabel", mxCreateString("X"));

  if ((value = SPS_GetEnvStr (spec_version, array, "ylabel")) != NULL) 
    mxSetField (plhs[0], 0, "ylabel", mxCreateString(value));
  else 
    mxSetField (plhs[0], 0, "ylabel", mxCreateString("Y"));

  if ((value = SPS_GetEnvStr (spec_version, array, "axistitles")) != NULL){ 
    mxSetField (plhs[0], 0, "axistitles", mxCreateString(value));
  } else { 
    if (SPS_GetArrayInfo (spec_version, array_name, NULL, &cols, NULL,NULL))
      mxSetField (plhs[0], 0, "axistitles", mxCreateString(""));
    else {
      strcpy(tempbuf,"Col1");
      for (i = 1; i < cols && i < 99; i++) 
	sprintf(tempbuf, "%s  Col%d", tempbuf, i+1);
      mxSetField (plhs[0], 0, "axistitles", mxCreateString(tempbuf));
    }
  }
  
  return;
}

