#include "mex.h"
#include "sps.h"

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  char buf[512];
  char *spec_version, *array_name;
  int i, dims[1], dim;
  mxArray *tmp[512];

  if (nlhs > 1) 
    mexErrMsgTxt ("Only one output array allowed");
    
  if (nrhs != 1)
    mexErrMsgTxt ("Usage: splab_getarraylist (<spec_version or *>)");
    
  if (mxIsChar(prhs[0]) != 1 || mxGetM(prhs[0]) != 1 )
    mexErrMsgTxt ("Input parameter must be a row vector of type string!");
  
  mxGetString(prhs[0], buf, 512);

  if (strcmp(buf, "*") == 0)
    spec_version = NULL;
  else
    spec_version = buf;
  
  for (i = 0, dim = 0; array_name = SPS_GetNextArray (spec_version, i); 
       dim++, i++) {
    if (i >= 512) 
      mexErrMsgTxt ("More than 512 Arrays - can't be");
    tmp[i] = mxCreateString(array_name);
  }
  
  dims[0] = dim;
  plhs[0] = mxCreateCellArray (1,dims); 
  for (i = 0; i < dim; i++)  
    mxSetCell (plhs[0], i, tmp[i]);
	       
  return;
}
  
  
