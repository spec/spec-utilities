#include "mex.h"
#include "sps.h"

void cleanup(void) 
{
  mexPrintf("SPLAB exiting - cleaning up shared memory\n");
  SPS_CleanUpAll();
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  int rows, cols;
  double *matptr;
  char spec_version[512], array_name[512];
  char eb[512];

  if (nrhs != 3) 
    mexErrMsgTxt 
      ("Usage: splab_createarray (<specversion>, <array_name>, array)");

  if (nlhs > 1) 
    mexErrMsgTxt ("Only one output array allowed");
    
  if (mxIsChar(prhs[0]) != 1 || mxIsChar(prhs[1]) != 1 ||
      mxGetM(prhs[0]) != 1 || mxGetM(prhs[1]) != 1 )
    mexErrMsgTxt ("Input parameters must be row vectors of type string!");
  
  mxGetString(prhs[0], spec_version, 512);
  mxGetString(prhs[1], array_name, 512);

  if (!mxIsNumeric(prhs[2]) || !mxIsDouble(prhs[2]) || mxIsComplex(prhs[2]) ||
      mxIsEmpty(prhs[2]))
    mexErrMsgTxt ("Input array must be a real double array");
  
  rows = mxGetM(prhs[2]);
  cols = mxGetN(prhs[2]);
  matptr = mxGetPr(prhs[2]);
  
  if (SPS_CreateArray(spec_version, array_name, rows, cols, SPS_DOUBLE, 2)) {
    sprintf(eb, "Can not create array: %s %s\n", spec_version, array_name);
    mexErrMsgTxt (eb);
  }
  
  mexAtExit(cleanup);
  if (SPS_CopyToShared(spec_version, array_name, matptr, SPS_DOUBLE, 
			 rows * cols )) {
    sprintf(eb, "Can not copy data: %s %s\n", spec_version, array_name);
    mexErrMsgTxt (eb);
  }

  return;
}
  
  
