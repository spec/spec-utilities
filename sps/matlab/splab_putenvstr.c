#include "mex.h"
#include "sps.h"

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  int rows, cols, type, flag;
  char *spec_version, vbuf[512], array_name[512], id[512], value[4096];
  double *tmpdptr;
  int ret;
  
  if (nrhs != 4) 
    mexErrMsgTxt ("Usage: splab_getarrayinfo <spec_version> <array_name> <id> <value>");

  if (nlhs > 1) 
    mexErrMsgTxt ("Only one output cell array allowed");
    
  if (mxIsChar(prhs[0]) != 1 || mxIsChar(prhs[1]) != 1 ||
      mxIsChar(prhs[2]) != 1 || mxIsChar(prhs[3]) != 1 ||
      mxGetM(prhs[0]) != 1 || mxGetM(prhs[1]) != 1 ||
      mxGetM(prhs[2]) != 1 || mxGetM(prhs[3]) != 1 )
    mexErrMsgTxt ("Input parameters must be row vectors of type string!");
  
  mxGetString(prhs[0], vbuf, 512);
  if (strcmp(vbuf,"*") == 0)
    spec_version = NULL;
  else
    spec_version = vbuf;
  mxGetString(prhs[1], array_name, 512);
  mxGetString(prhs[2], id, 512);
  mxGetString(prhs[3], value, 512);
  
  ret = SPS_PutEnvStr(spec_version, array_name, id, value);
  
  plhs[0] = mxCreateDoubleMatrix(1, 1, mxREAL);
  tmpdptr = mxGetPr(plhs[0]);
  *tmpdptr = (double) ret;

  return;
}

