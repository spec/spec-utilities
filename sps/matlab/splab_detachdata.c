#include "mex.h"
#include "sps.h"

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  void *data;
  double *tmpdptr;
  int ret;

  if (nrhs != 1) 
    mexErrMsgTxt ("Usage: splab_detachdata (matrix)");
  
  if (nlhs > 1) 
    mexErrMsgTxt ("Only one output array allowed");
  
  if (mxIsNumeric(prhs[0]) != 1)
    mexErrMsgTxt ("Input parameters must be matrix from splab_attachdata");
  

  data = mxGetPr(prhs[0]);
  ret = SPS_ReturnDataPointer (data);

  /* Delete the input matrix */
  mxDestroyArray((mxArray *)prhs[0]);

  plhs[0] = mxCreateDoubleMatrix(1, 1, mxREAL);
  tmpdptr = mxGetPr(plhs[0]);
  *tmpdptr = (double) ret;

  return;
}
  
  
