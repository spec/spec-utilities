#include "mex.h"
#include "sps.h"

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  int rows, cols;
  double *matptr;
  char *spec_version, vbuf[512], array_name[512];
  char eb[512];

  if (nrhs != 2) 
    mexErrMsgTxt ("Usage: getdata <specversion> <array_name>");

  if (nlhs > 1) 
    mexErrMsgTxt ("Only one output array allowed");
    
  if (mxIsChar(prhs[0]) != 1 || mxIsChar(prhs[1]) != 1 ||
      mxGetM(prhs[0]) != 1 || mxGetM(prhs[1]) != 1 )
    mexErrMsgTxt ("Input parameters must be row vectors of type string!");
  
  mxGetString(prhs[0], vbuf, 512);
  if (strcmp(vbuf,"*") == 0)
    spec_version = NULL;
  else
    spec_version = vbuf;
  mxGetString(prhs[1], array_name, 512);

  
  if (SPS_GetArrayInfo(spec_version, array_name, &rows, &cols, NULL, NULL)) {
    sprintf(eb, "Can not attach to array: %s %s\n", spec_version, array_name);
    mexErrMsgTxt (eb);
  }
  
  plhs[0] = mxCreateDoubleMatrix(cols, rows, mxREAL);
  matptr = mxGetPr(plhs[0]);

  if (SPS_CopyFromShared(spec_version, array_name, matptr, SPS_DOUBLE, 
			 rows * cols )) {
    sprintf(eb, "Can not get data from array: %s %s\n", spec_version, 
	    array_name);
    mexErrMsgTxt (eb);
  }

  return;
}
  
  
