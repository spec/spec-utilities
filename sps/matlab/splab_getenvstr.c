#include "mex.h"
#include "sps.h"

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  char *spec_version, vbuf[512], array_name[512], id[512], *str;

  
  if (nrhs != 3) 
    mexErrMsgTxt ("Usage: splab_getenvstr <spec_version> <array_name> <id>");

  if (nlhs > 1) 
    mexErrMsgTxt ("Only one output cell array allowed");
    
  if (mxIsChar(prhs[0]) != 1 || mxIsChar(prhs[1]) != 1 ||
      mxIsChar(prhs[2]) != 1 || 
      mxGetM(prhs[0]) != 1 || mxGetM(prhs[1]) != 1 ||
      mxGetM(prhs[2]) != 1 )
    mexErrMsgTxt ("Input parameters must be row vectors of type string!");
  
  mxGetString(prhs[0], vbuf, 512);
  if (strcmp(vbuf,"*") == 0)
    spec_version = NULL;
  else
    spec_version = vbuf;
  mxGetString(prhs[1], array_name, 512);
  mxGetString(prhs[2], id, 512);
  
  if ((str = SPS_GetEnvStr(spec_version, array_name, id)) == NULL) 
    plhs[0] = mxCreateString("<NONE>");
  else
    plhs[0] = mxCreateString(str);
  
  return;
}

