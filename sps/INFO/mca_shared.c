/******************************************************************************

 Name mca_shared.c

 Description: contains functions for dealing with shared memory

******************************************************************************/

#include <stdio.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/time.h>
#include <unistd.h>

#include <math.h>
#include <string.h>
#include <tk.h>
#include "mca.h"
#include "spec_shm.h"

#ifndef IPCS
#define IPCS "ipcs -m"  /* let the system find the path. */
#endif

int getShmPtr(int id );

#define debug
/*
*/

/*--------------------------------------------------------------------------
 Name   	: tcl_getshmid
 Description  	: find out a spec shared memory id for the mca
 Input 		: nothing
 Output 	: The shared memory ids in spec for the mca data
 ............................................................................*/

int tcl_get_shmid(ClientData clientData, 
			Tcl_Interp *interp, int argc, char *argv[])
{
  int id,i,shaddr,use_m;
  struct shm_header *shm;
  struct shmid_ds info;
  char buf[256];
  FILE *pd;
 
 /*
  * define string to put return a list element to tcltk
  */
  static char str_prep_out[1024];	/* define temp string to put numbers */
  Tcl_DString str_out;

  Tcl_DStringInit(&str_out);

  if (argc != 1) {
    interp->result = "wrong # args: should be get_shmid";
    return TCL_ERROR;
  }

  pd = (FILE*) popen(IPCS,"r");

  if (!pd) {
    interp->result = "Could not get ipcs running";
    return TCL_ERROR;
  }


 /*
  * now read the shared memory ids and select those which have 
  * the right magic number (i.e they were created by spec)
  */
  use_m = 1;    /* expect an "m" in the first column */
  for (i=0;!feof(pd);) {
    if (fgets(buf,sizeof(buf)-1,pd) == NULL)
      break;
    if (!strncmp(buf, "key", 3))
	use_m = 0;      /* On linux, "key" is label of first column */

    if (use_m) {
      if (buf[0]=='m') {
	if (sscanf(buf+1,"%d",&id) != 1)
	  continue;
      } else
	continue;
    } else {
      if (sscanf(buf,"%d",&id) != 1)
	continue;
    }
    printf("attach\n",id);
    if ((shaddr = (int) shmat(id,(char*) 0,SHM_RDONLY)) ==  -1) {
      continue;
    }
#ifdef debug
    printf("attached\n");
#endif
    shm = (struct shm_header *)shaddr;
    if (shm->head.head.magic != SHM_MAGIC) {
      shmdt((void*)shm);
      continue;
    }
    shmctl(id,IPC_STAT, &info);
    if (info.shm_nattch == 1) {
      shmdt((void*)shm);
      continue;
    }
    sprintf(str_prep_out,"%d %s %s %d %d",id, shm->head.head.name,
	shm->head.head.spec_version, shm->head.head.rows,shm->head.head.cols);
    Tcl_DStringAppendElement(&str_out,str_prep_out);
    shmdt((void*)shm);
  }
  pclose(pd);

 /*
  * Now build a list of the shared ids
  */
  Tcl_DStringResult(interp,&str_out);
  return TCL_OK;
}

int hasChanged(struct shm_header *shm,int id)
{
  struct shmid_ds info;
  static utime = 0;

  shmctl(id,IPC_STAT, &info);
  if (info.shm_nattch == 1) {
    return 2;
  }

  if (utime == shm->head.head.utime)
    return 0;
  utime = shm->head.head.utime;
    return 1;
}

int tcl_read_shm(ClientData clientData, 
			Tcl_Interp *interp, int argc, char *argv[])
{
  int shaddr;
  unsigned long i,id,ncols;
  long tcl_long, idx, force_read;
  void *sh_data;
  struct shm_header *shm;
  extern struct mca_data_type *mca_d;    /* memory in application for plot */

  if (argc != 3) {
    interp->result = "wrong # args: should be read_shm <id> <force read>";
    return TCL_ERROR;
  }

  if (Tcl_ExprLong(interp,argv[2],&force_read) == TCL_ERROR) {
    return TCL_ERROR;
  }

  if (Tcl_ExprLong(interp,argv[1],&tcl_long) == TCL_ERROR) {
    return TCL_ERROR;
  }
  id = tcl_long;

  if (!(shaddr = getShmPtr(id))) {
    interp->result = "Could not attach to shared memory\n";
    return TCL_ERROR;
  }
  shm = (struct shm_header *)shaddr;
  if (hasChanged(shm,id) == 1 || force_read) {
    ncols = shm->head.head.cols ;
#ifdef debug
    printf("shm->head.head.cols %d\n",ncols);
#endif
    /* find out the orientation of the data
      i.e columns > rows or rows > columns, or only one column!
    */
    if (ncols ==1 ) {
      sh_data = shm->data;
      memcpy(mca_d->count,&sh_data,shm->head.head.rows* sizeof(long));
      mca_d->no_ch = shm->head.head.rows;
      mca_d->last_ch = shm->head.head.rows-1 + mca_d->first_ch;
    } else if (shm->head.head.rows == 1) {
      memcpy(mca_d->count,&sh_data,ncols* sizeof(long));
      mca_d->no_ch = shm->head.head.cols;
      mca_d->last_ch = shm->head.head.cols-1 + mca_d->first_ch;
    } else if (shm->head.head.cols > shm->head.head.rows) {
      for (i = 0;i<shm->head.head.cols;i++) {
        idx = (long ) (* (long *)(&sh_data + i));
	mca_d->count[idx] = (long ) (* (long *)(&sh_data + i + shm->head.head.cols ));
      }
      mca_d->no_ch = shm->head.head.cols;
      mca_d->last_ch = shm->head.head.cols-1 + mca_d->first_ch;
    } else {
      for (i = 0;i<shm->head.head.rows;i++) {
        idx = (long ) (* (long *)(&sh_data + i*ncols));
        mca_d->count[idx] = (long ) (* (long *)(&sh_data + i*ncols + 1));
      }
      mca_d->no_ch = shm->head.head.rows;
      mca_d->last_ch = shm->head.head.rows-1 + mca_d->first_ch;
    }

    interp->result = "1";
  } else {
    interp->result = "0";
  }
  shmdt((char *)shaddr);
  return TCL_OK;
}


int getShmPtr(int id)
{
  int addr;
  struct shm_header *shm;
  struct shmid_ds info;
 
  if ((addr = (int) shmat(id,(char*) 0,SHM_RDONLY)) == -1) {
    return (int )NULL;
  }

  shm = (struct shm_header *)addr;
  if (shm->head.head.magic != SHM_MAGIC) {
    fprintf(stderr,"Shared mem (id %d) is not a spec shared mem\n",id);
    shmdt((char *)addr);
    return (int )NULL;
  }

  return addr;
}
