#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include "tk.h"
#include "../mca.h"
#include "../spec_shm.h"

#define SHM_CREATE 0
#define SHM_DELETE 1
#define SHM_PUT 2
#define SHM_GET 3

#define debug

unsigned long *profile = NULL;
unsigned long total_counts;
long build_buf[MAX_CHANNELS];
struct shm_header *shm_ptr;


int user_code(Tcl_Interp *,int ,int , int , int ,char *, char *);

/*----------------------------------------------------------------------------
  Name  : set_shm_par
  Input : shared memory id, a list of numbers
  Output: sucess or failure
 ............................................................................*/

int tcl_set_shm_par(ClientData clientData, Tcl_Interp *interp, 
		int argc, char *argv[]) {
  int shm_id, list_count,i;
  char **param_list;
  char *shmptr,*addr;
  struct shm_header *shm_p;
  double tcl_double, *dval;
  Tcl_DString str_out;

  Tcl_DStringInit(&str_out);

  if (argc != 3) {
    interp->result = "wrong # args: should be set_shm <shm id> <data list>";
    return TCL_ERROR;
  }
  if ( Tcl_GetInt(interp,argv[1],&shm_id) == TCL_ERROR) return TCL_ERROR; 

  if ((addr = shmat(shm_id,(char*) 0,0)) == (char*) -1) {
    sprintf(interp->result,"Could not attach shared mem (id %d)\n",shm_id);
    return TCL_ERROR;
  }

  if (Tcl_SplitList(interp, argv[2], &list_count, &param_list) == TCL_ERROR)
    return TCL_ERROR;
  
  shm_p = (struct shm_header *)addr;

  /* limit list length to the size of the actual shared memory */
  if (shm_p->head.head.rows > shm_p->head.head.cols) {
    if (list_count > shm_p->head.head.rows ) list_count = shm_p->head.head.rows;
  } else {
    if (list_count > shm_p->head.head.cols) list_count = shm_p->head.head.cols;
  }

  for(i=0; i< list_count; i++) {
    if (Tcl_ExprDouble(interp,param_list[i],&tcl_double) == TCL_ERROR)
                  return TCL_ERROR;
    dval = (double *)(&shm_p->data);
    dval[i] = tcl_double;
  }
  (shm_p->head.head.utime)++;
  shmdt(addr);
  return TCL_OK;
}

/*----------------------------------------------------------------------------
  Name  : read_shm_par
  Input : shared memory id, a list of numbers
  Output: sucess or failure
 ............................................................................*/

int tcl_read_shm_par(ClientData clientData, Tcl_Interp *interp, 
		int argc, char *argv[]) {
  int shm_id, list_count,i;
  char **param_list;
  char *shmptr,*addr;
  struct shm_header *shm_p;
  double tcl_double, *dval;
  Tcl_DString str_out;
  static char str_prep_out[1000];

  Tcl_DStringInit(&str_out);

  if (argc != 2) {
    interp->result = "wrong # args: should be read_shm <shm id>";
    return TCL_ERROR;
  }
  if ( Tcl_GetInt(interp,argv[1],&shm_id) == TCL_ERROR) return TCL_ERROR; 

  if ((addr = shmat(shm_id,(char*) 0,0)) == (char*) -1) {
    sprintf(interp->result,"Could not attach shared mem (id %d)\n",shm_id);
    return TCL_ERROR;
  }

  shm_p = (struct shm_header *)addr;

  for(i=0; i< 3; i++) {
    dval = (double *)(&shm_p->data);
    sprintf(str_prep_out, "%g", dval[i]);
    Tcl_DStringAppendElement(&str_out,str_prep_out);
  }
  shmdt(addr);
  Tcl_DStringResult(interp,&str_out);
  return TCL_OK;
}

/*----------------------------------------------------------------------------
 Name   : tcl_create_shm
 Input  : Creates an area of shared memory
 Output : sucess or failure
 ............................................................................*/

int tcl_shm(ClientData clientData, Tcl_Interp *interp, int argc, char *argv[]) {
  char *name, *version;
  int flag=0 , size =0, key=-1, elems =1;
  int points =0, op=0, ret_id=0, id=0;
  register char   *p;
  p = *argv++;
  argc--;
  while (argc--) {
    p = *argv++;
    if (*p == '-') {
      /* switch on options */
      switch(*++p) {
      case 's':
	/* The size is given in number of doubles */
	size = sizeof(double)*atoi(p+1);
	break;
      case 'p':
	/* The number of points in data group */
	points = atoi(p+1);
	break;
      case 'e':
	/* The number of elements in data group */
	elems = atoi(p+1);
	break;
      case 'k':
	/* A key can be given */
	key = atoi(p+1);
	break;
      case 'c':
	/* We want to create the shared mem */
	op = SHM_CREATE;
	if ( *argv[0] != '-' ) {
	  name = *argv++;
          argc--;
	} else version = "sim";
	if ( *argv[0] != '-' )  {
	  version = *argv++;
          argc--;
        } else name = "sim";
	break;
      case 'd':
	/* We want to delete the shared mem */
	op = SHM_DELETE;
	break;
      case 'w':
	/* We want to write into shared mem */
	op = SHM_PUT;
	break;
      case 'r':
	/* We want to read from shared mem */
	op = SHM_GET;
	break;
      case 'i':
	/* We want to use shm_id */
	id = atoi(p+1);
	break;
      default:
	sprintf(interp->result,"Unknown option %c\n",*p);
	return TCL_ERROR;
	/*NOTREACHED*/
      }
    } else {
      sprintf(interp->result,
	 "usage: -s<size> -p<points> -e<elems> -i<id> -k<key> -c -w -r -d ");
      return TCL_ERROR;
    }
  }

  if (size == 0) {
    size = elems * points;
  }
  
  ret_id = user_code(interp,op,size,elems,id, name, version);
  if ( ret_id == TCL_ERROR ) {
    return TCL_ERROR;
  } else {
    return TCL_OK;
  }
}

int user_code(Tcl_Interp *interp,int op,int size, int cols, int id, char *name, char *version) {
  register int    i;
  int points;
  char *shmptr,*addr;
      struct shmid_ds info;
  int flag=0 , key=-1, put=0, get=0;
    int dtdt = 0;
  points = size/cols;

  if (op == SHM_CREATE) {
    if (size == 0) {
      fprintf(stderr,"You have to give a size of no of points\n");
      return TCL_ERROR;
    }
    flag = 0644;
    if (key == -1) 
      key = IPC_PRIVATE;
    else 
      flag |= IPC_CREAT;
    size = size * sizeof(long) + (sizeof (struct shm_header))*2;
    id = shmget((key_t) key, (size_t) size, flag);
   /*
    * now put the structure into the shared memory
    * Remember to attach to it first
    */
    fprintf(stderr,"shm id %d created\n",id);
    if ((addr = shmat(id,(char*) 0,0)) == (char*) -1) {
      sprintf(interp->result,"Could not attach shared mem (id %d)\n",id);
      return TCL_ERROR;
    } else {
      printf("Attached to %d, address %x\n",id,addr);
    }
    /*
     * copy in the values
     */
    shm_ptr = (struct shm_header *)addr;
    shm_ptr->head.head.magic = SHM_MAGIC;
    shm_ptr->head.head.type = 0;
    shm_ptr->head.head.version = 0;
    shm_ptr->head.head.rows = points;
    shm_ptr->head.head.cols = cols;
    shm_ptr->head.head.utime = 0;
    strcpy((char *)(shm_ptr->head.head.name),name);
    strcpy(shm_ptr->head.head.spec_version,version);
    get_data(&(shm_ptr->data),points,cols); 
    sprintf(interp->result,"%d",id);
    return TCL_OK;
  } else if ( op == SHM_PUT || op == SHM_GET ) {
    if (id ==0) {
      sprintf(interp->result,"Not a valid id %d\n",id);
      return TCL_ERROR;
    }
    
    if ((shmptr = shmat(id,(char*) 0,0)) == (char*) -1) {
      sprintf(interp->result,"Could not attach shared mem (id %d)\n",id);
      return TCL_ERROR;
    }
    
    fprintf(stderr,"shm id %d attached (addr 0x%x)\n",id,shmptr);

    if (op == SHM_PUT) {
      get_data(shmptr,points,cols);
      fprintf(stderr,"data written %d points %d cols\n",points,cols);
    }
    else {
      fprintf(stderr,"data read %d points %d cols\n",points,cols);
    }


    shmdt(shmptr);
    fprintf(stderr,"shm id %d detached (addr 0x%x)\n",id,shmptr);
  } else if (op == SHM_DELETE) {
    if (id <=0 ) {
      sprintf(interp->result,"Not a valid id %d\n",id);
      return TCL_ERROR;
    }
    clear_data(&(shm_ptr->data),shm_ptr->head.head.rows);
    if ( shmdt(shmptr) ) printf("Couldn't detach from %d error %d\n",id,shmdt(shmptr));
    else {
      shmctl(id,IPC_STAT, &info);
      fprintf(stderr,"shm id %d detached (addr 0x%x), nattch %d\n",id,shmptr,info.shm_nattch);
    }
    while ( info.shm_nattch == 1 && dtdt < 100) { 
      shmctl(id,IPC_STAT, &info);
      shmdt(shmptr);
      dtdt++; 
   }
   if (dtdt>99) printf("tried 100 times to detach from shared memory!\n");
    if ( shmctl(id, IPC_RMID , (struct shmid_ds *)NULL) != 0) {
      interp->result = "Error: couldn't delete shared memory\n";
      return TCL_ERROR;
    } else {
      fprintf(stderr,"shm id %d deleted\n",id);
    }
  } else {
    sprintf(interp->result,"Unrecognised command '%d",op);
    return TCL_ERROR;
  }
  return 0;
}

/******************************************************************************

  Name:		tcl_build_sample

  Description:	build the sample data in shared memory so that the MCA
		application can poll it and reflect its state.

.............................................................................*/

int tcl_build_sample(ClientData clientData, Tcl_Interp *interp, int argc, char *argv[]) {
  int i, *timeptr;
  extern struct shm_header *shm_ptr;
  int no_counts, points,shm_id;
  if (argc != 4) {
    interp->result = "wrong # args: should be build_sample <no_counts> <no channels> <shm_id>";
    return TCL_ERROR;
  }
  if ( Tcl_GetInt(interp,argv[1],&no_counts) == TCL_ERROR) return TCL_ERROR; 
  if ( Tcl_GetInt(interp,argv[2],&points) == TCL_ERROR) return TCL_ERROR; 
  if ( Tcl_GetInt(interp,argv[3],&shm_id) == TCL_ERROR) return TCL_ERROR; 
  for(i=0; i< no_counts; i++) {
    add_count(&(shm_ptr->data),points);
  }
  (shm_ptr->head.head.utime)++;
  return TCL_OK;
}

/******************************************************************************

  Name:		make_profile

  Description:	builds a profile using the original data where each value for
		a channel is the sum of all counts from 0 to that channel
		uses the mca_load array to build the profile.

.............................................................................*/

unsigned long make_profile( points ) {
  int i;
  extern struct mca_data_type *mca_load;
  unsigned long c=0; /* counter for graph */

  profile = malloc (points * sizeof(unsigned long) +1);
 /*
  * Generate the profile for random generation of loaded sample
  */
  for(i=0; i< points; i++) {
    c += mca_load->count[i];
    profile[i] = c;
  }
  total_counts = profile[i-1];
  return total_counts;
}

int tcl_make_profile(ClientData clientData, Tcl_Interp *interp, int argc, char *argv[]) {
  int points;
  unsigned long max_value;
  if (argc != 2) {
    interp->result = "wrong # args: should be make_profile <points>";
    return TCL_ERROR;
  }
  if ( Tcl_GetInt(interp,argv[1],&points) == TCL_ERROR) return TCL_ERROR; 
  max_value = make_profile(points);
  sprintf(interp->result,"%d",max_value);
  return TCL_OK;
}

/******************************************************************************

  Name:	add_count

  Description:	Adds a count to a random position in the plot according to 
		a profile which contains a sum of the counts of the channel

  Global Variables:	profile - the profile curve created by make_profile
			mca_load - the original data loaded from a file
			shm  - the shared memory id.
..............................................................................*/

int add_count(unsigned long *shm_data, int nchnls) {
  int n, position;
  unsigned long val;
  extern struct mca_data_type *mca_load;
  int dividor = 2;
  n = drand48() * total_counts;
/*
  position = nchnls/2;
  while (dividor <= nchnls) {
    dividor *= 2;
    if(n > profile[position]) position = position + nchnls/dividor;
      else position = position - nchnls/dividor;
  }
*/
  position=0;
  while ( profile[position] < n ) position++;
 /*
  * only add one if the maximum n.o of counts is not reached for that channel
  */
  build_buf[position]++;
  (long )(*(shm_data +position*3+1))++;
}

int clear_data(long *shm_data, int nchnls) {
  int i;
  extern struct shm_header *shm_ptr;
  for(i=0; i< nchnls; i++) {
    build_buf[i] = 0;
    *(shm_data +i*3+0) = 0;
    *(shm_data +i*3+1) = 0;
    *(shm_data +i*3+2) = 0;
  }
}

int get_data(x,pts,wid) 
register void *x;
long pts;
long wid;
{
  long *x_ptr;
  register long    i;
  register unsigned long  *lp;
  extern struct mca_data_type *mca_load;
  lp = build_buf;
  i = 0;
  x_ptr = (long *)x;
  while (i < wid*pts) {
    x_ptr[i] = i/wid;
    x_ptr[i+1] = lp[i/wid];
    x_ptr[i+2] = 0;
    i+= wid;
  }
}
