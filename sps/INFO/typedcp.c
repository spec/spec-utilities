#include "spec_shm.h"

#define ONECP(tfrom,tto) \
{\
 register tfrom *p = (tfrom *) f ; \
 register tto *v = (tto *) t; \
 for (i = 0; i < np; i++, p ++, v++) \
   *v = *p; \
}


int typedcp (f,ft,t,tt,np)
     void *f,*t;
     int tt,ft,np;
    
{
  register int i;
  int sizef=typedsize(ft), sizet=typedsize(tt);

  if (!sizef || ! sizet) 
    return 1;
  
  if (ft == tt) {
    memcpy(t,f,np*sizef);
    return 0;
  }
  
  switch (ft) {
  case SHM_LONG:
    switch (tt) {
      case SHM_DOUBLE:
        ONECP (long,double)
	break;	
      case SHM_FLOAT:
        ONECP (long,float)
	break;	
      case SHM_ULONG:
        ONECP (long,unsigned long)
	break;	
      case SHM_USHORT:
        ONECP (long,unsigned short)
	break;	
      case SHM_UCHAR:
        ONECP (long,unsigned char)
	break;	
      case SHM_CHAR:
        ONECP (long,char)
	break;	
      case SHM_SHORT:
        ONECP (long,short)
	break;	
    }
    break;
  case SHM_ULONG:
    switch (tt) {
      case SHM_DOUBLE:
        ONECP (unsigned long,double)
	break;	
      case SHM_FLOAT:
        ONECP (unsigned long,float)
	break;	
      case SHM_USHORT:
        ONECP (unsigned long,unsigned short)
	break;	
      case SHM_UCHAR:
        ONECP (unsigned long,unsigned char)
	break;	
      case SHM_CHAR:
        ONECP (unsigned long,char)
	break;	
      case SHM_SHORT:
        ONECP (unsigned long,short)
	break;	
      case SHM_LONG:
        ONECP (unsigned long,long)
	break;	
    }
    break;
  case SHM_USHORT:
    switch (tt) {
      case SHM_DOUBLE:
        ONECP (unsigned short,double)
	break;	
      case SHM_FLOAT:
        ONECP (unsigned short,float)
	break;	
      case SHM_ULONG:
        ONECP (unsigned short,unsigned long)
	break;	
      case SHM_UCHAR:
        ONECP (unsigned short,unsigned char)
	break;	
      case SHM_CHAR:
        ONECP (unsigned short,char)
	break;	
      case SHM_SHORT:
        ONECP (unsigned short,short)
	break;	
      case SHM_LONG:
        ONECP (unsigned short,long)
	break;	
    }
    break;
  case SHM_UCHAR:
    switch (tt) {
      case SHM_DOUBLE:
        ONECP (unsigned char,double)
	break;	
      case SHM_FLOAT:
        ONECP (unsigned char,float)
	break;	
      case SHM_ULONG:
        ONECP (unsigned char,unsigned long)
	break;	
      case SHM_USHORT:
        ONECP (unsigned char,unsigned short)
	break;	
      case SHM_CHAR:
        ONECP (unsigned char,char)
	break;	
      case SHM_SHORT:
        ONECP (unsigned char,short)
	break;	
      case SHM_LONG:
        ONECP (unsigned char,long)
	break;	
    }
    break;
  case SHM_SHORT:
    switch (tt) {
      case SHM_DOUBLE:
        ONECP (short,double)
	break;	
      case SHM_FLOAT:
        ONECP (short,float)
	break;	
      case SHM_ULONG:
        ONECP (short,unsigned long)
	break;	
      case SHM_USHORT:
        ONECP (short,unsigned short)
	break;	
      case SHM_UCHAR:
        ONECP (short,unsigned char)
	break;	
      case SHM_CHAR:
        ONECP (short,char)
	break;	
      case SHM_LONG:
        ONECP (short,long)
	break;	
    }
    break;
  case SHM_CHAR:
    switch (tt) {
      case SHM_DOUBLE:
        ONECP (char,double)
	break;	
      case SHM_FLOAT:
        ONECP (char,float)
	break;	
      case SHM_ULONG:
        ONECP (char,unsigned long)
	break;	
      case SHM_USHORT:
        ONECP (char,unsigned short)
	break;	
      case SHM_UCHAR:
        ONECP (char,unsigned char)
	break;	
      case SHM_SHORT:
        ONECP (char,short)
	break;	
      case SHM_LONG:
        ONECP (char,long)
	break;	
    }
    break;
  case SHM_FLOAT:
    switch (tt) {
      case SHM_DOUBLE:
        ONECP (float,double)
	break;	
      case SHM_ULONG:
        ONECP (float,unsigned long)
	break;	
      case SHM_USHORT:
        ONECP (float,unsigned short)
	break;	
      case SHM_UCHAR:
        ONECP (float,unsigned char)
	break;	
      case SHM_CHAR:
        ONECP (float,char)
	break;	
      case SHM_SHORT:
        ONECP (float,short)
	break;	
      case SHM_LONG:
        ONECP (float,long)
	break;	
    }
    break;
  case SHM_DOUBLE:
    switch (tt) {
      case SHM_FLOAT:
        ONECP (double,float)
	break;	
      case SHM_ULONG:
        ONECP (double,unsigned long)
	break;	
      case SHM_USHORT:
        ONECP (double,unsigned short)
	break;	
      case SHM_UCHAR:
        ONECP (double,unsigned char)
	break;	
      case SHM_CHAR:
        ONECP (double,char)
	break;	
      case SHM_SHORT:
        ONECP (double,short)
	break;	
      case SHM_LONG:
        ONECP (double,long)
	break;	
    }
    break;
  }
}

int typedsize(typ) 
{
  switch (typ) {
  case SHM_USHORT:
    return sizeof(unsigned short);
  case SHM_ULONG:
    return sizeof(unsigned long);
  case SHM_SHORT:
    return sizeof(short);
  case SHM_LONG:
    return sizeof(long);
  case SHM_UCHAR:
    return sizeof(unsigned char);
  case SHM_CHAR:
    return sizeof(char);
  case SHM_DOUBLE:
    return sizeof(double);
  case SHM_FLOAT:
    return sizeof(float);
  default :
    return 0;
  }
}




