#include <stdio.h>
#include <string.h>
#include <strings.h>

struct pmem {
  void *data;
  long int size;
  char *file;
  int line;
  struct pmem *next;
} ;

struct pmem *plist = NULL;

void *_pmalloc (int size, char *file, int line)
{
 struct pmem *priv_mem;
  
 void *ptr = (void *)malloc(size);
 priv_mem = (struct pmem *)malloc(sizeof(struct pmem));
 priv_mem->data = ptr;
 priv_mem->size = size;
 if (file != NULL) {
   priv_mem->file = (char *)malloc(strlen(file)+1);
   strcpy(priv_mem->file, file);
 } else
   priv_mem->file = NULL;
 priv_mem->line = line;
 priv_mem->next = plist;
 plist = priv_mem;
 
 if (size == 32 || size == 112) {
   fprintf(stderr, "size=%d file=%s line=%d\n", size, file, line);
 }
 return ptr;
}

void _pfree (void *ptr, char *file, int line)
{
 struct pmem *priv_mem, **mem_mem;

 for (mem_mem=&plist,priv_mem=plist;priv_mem!=NULL;priv_mem=priv_mem->next) {
   if (priv_mem->data == ptr) {
     *mem_mem = priv_mem->next;
     free(ptr);
     free(priv_mem->file);
     free(priv_mem);
     return ;
   }
   mem_mem = &(priv_mem->next);
 }
 free(ptr);  
}

void mempr(char *str)
{
  struct pmem *priv_mem;
  printf("----------------------------------------\n");
  for (priv_mem=plist;priv_mem != NULL;priv_mem=priv_mem->next) {
    if (str != (char *)NULL) {
      if (priv_mem->file && (strstr(priv_mem->file, str) != (char *)NULL))
        printf("  ptr=0x%x (%d) size=%d allocated in file %s line %d\n", 
                priv_mem->data, priv_mem->data, priv_mem->size, 
                priv_mem->file, priv_mem->line);
    } else
      printf("  ptr=0x%x (%d) size=%d allocated in file %s line %d\n", 
              priv_mem->data, priv_mem->data, priv_mem->size, 
              priv_mem->file, priv_mem->line);
  }
  printf("----------------------------------------\n");
}
