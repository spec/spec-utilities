#include <sps.h>
#include <sps_lut.h>
#include <stdio.h>

struct timeval {
  unsigned long	tv_sec;		/* seconds */
  long		tv_usec;	/* and microseconds */
};

struct timezone {
  int	tz_minuteswest;	/* minutes west of Greenwich */
  int	tz_dsttime;	/* type of dst correction */
};

/* Benchmark our routines */
static bench(char *str)
{
  static struct timeval start, stop;
  struct timezone tzp;

  if (str == (char *)NULL) {
    gettimeofday(&start, &tzp);
  }
  else {
    gettimeofday(&stop, &tzp);
    printf("Time in %s : %10.3f\n", str, 
	   (double)(stop.tv_sec-start.tv_sec) + 
	   (double)(stop.tv_usec-start.tv_usec) * (double)0.000001);
    start.tv_sec = stop.tv_sec;
    start.tv_usec = stop.tv_usec;
  }
}


main(int argc, char *argv[]) 
{
  int rows,cols;
  int i,j,k;
  int pal_entries;
  int reduc, fastreduc, meth, autoscale, mapmin, mapmax, palette_code;
  double gamma, min, max, minplus;
  int prows, pcols;
  int mapbytes;
  int type;
  char type_str[512];
  char meth_str[512];
  char palette_str[512];
  int databytes;

  struct id {
    int i;
    char *d;
  } ;
  
  struct id type_id[] = {
    {SPS_DOUBLE, "SPS_DOUBLE"},
    {SPS_FLOAT, "SPS_FLOAT"},
    {SPS_DOUBLE, "SPS_DOUBLE"},
    {SPS_FLOAT, "SPS_FLOAT"},
    {SPS_LONG, "SPS_LONG"},
    {SPS_ULONG, "SPS_ULONG"},
    {SPS_SHORT, "SPS_SHORT"},
    {SPS_USHORT, "SPS_USHORT"},
    {SPS_CHAR, "SPS_CHAR"},
    {SPS_UCHAR, "SPS_UCHAR"}};
  
  struct id meth_id[] = {
    {SPS_LINEAR, "SPS_LINEAR"},
    {SPS_LOG, "SPS_LOG"},
    {SPS_GAMMA, "SPS_GAMMA"}};
  
  struct id palette_id[] = {
    {SPS_GREYSCALE, "SPS_GREYSCALE"},
    {SPS_TEMP, "SPS_TEMP"},
    {SPS_RED, "SPS_RED"},
    {SPS_GREEN, "SPS_GREEN"},
    {SPS_BLUE, "SPS_BLUE"},
    {SPS_REVERSEGREY, "SPS_REVERSEGREY"},
    {SPS_MANY, "SPS_MANY"}};
  
  int palbytes;

  /* Different types */
  void *arr;

  unsigned char *palette;
  unsigned char *outp;

  if (argc != 15) {
    printf("rows cols type reduc fastreduc meth gamma autoscale mapmin mapmax mapbytes palette_code, min, max");
    exit(-1);
  }

  sscanf(argv[1],"%d",&rows);
  sscanf(argv[2],"%d",&cols);
  sscanf(argv[3],"%s",&type_str);
  sscanf(argv[4],"%d",&reduc);
  sscanf(argv[5],"%d",&fastreduc);
  sscanf(argv[6],"%s",&meth_str);
  sscanf(argv[7],"%lf",&gamma);
  sscanf(argv[8],"%d",&autoscale);
  sscanf(argv[9],"%d",&mapmin);
  sscanf(argv[10],"%d",&mapmax);
  sscanf(argv[11],"%d",&mapbytes);
  sscanf(argv[12],"%s",&palette_str);
  sscanf(argv[13],"%lf",&min);
  sscanf(argv[14],"%lf",&max);
  
  for (type = -1, i = 0; i < sizeof (type_id) / sizeof (struct id); i++)
    if (strcmp(type_id[i].d, type_str) == 0)
      type = type_id[i].i;
  if (type == -1) {
    printf("type: ");
    for (i = 0; i < sizeof (type_id) / sizeof (struct id); i++)
      printf("%s ",type_id[i].d);
    printf("\n");
    exit(-1);
  }

  palbytes = (mapbytes == 3) ? 4 : mapbytes;
  databytes = (mapbytes == 0) ? 1 : mapbytes;
  
  for (meth = -1, i = 0; i < sizeof (meth_id) / sizeof (struct id); i++)
    if (strcmp(meth_id[i].d, meth_str) == 0)
      meth = meth_id[i].i;
  if (meth == -1) {
    printf("meth: ");
    for (i = 0; i < sizeof (meth_id) / sizeof (struct id); i++)
      printf("%s ",meth_id[i].d);
    printf("\n");
    exit(-1);
  }

  for (palette_code = -1, i = 0; i < sizeof (palette_id) / sizeof (struct id);
       i++)
    if (strcmp(palette_id[i].d, palette_str) == 0)
      palette_code = palette_id[i].i;
  if (palette_code == -1) {
    printf("palette_code: ");
    for (i = 0; i < sizeof (palette_id) / sizeof (struct id); i++)
      printf("%s ",palette_id[i].d);
    printf("\n");
    exit(-1);
  }
  
  arr = (void *) malloc (rows * cols * SPS_Size(type));
  
  for (i = 0;  i < rows; i++)
    for (j = 0;  j < cols; j++) {
      double val = (double) i * 100 + j - 20;
      if ((val < 0.0) && ( type == SPS_ULONG || type == SPS_UCHAR || 
			type == SPS_USHORT))
	val = 0;
      if (val <= 0.0 && meth != SPS_LINEAR )
	val = 1;
      if (val > 250 && type == SPS_CHAR)
	val = 250;
      if (val > 120 && type == SPS_UCHAR)
	val = 120;
      if (val > 10000) 
	val = 10000;
      SPS_PutZdata(arr, type, cols, rows, j, i, val);
    }
  printf("min max minplus %f %f %f\n",min,max,minplus);
  
  bench (NULL);
  outp = SPS_PaletteArray (arr, type, cols, rows, 
          reduc, fastreduc, meth, gamma, autoscale, 
	  mapmin, mapmax, mapbytes, palette_code,
	  &min, &max, &prows, &pcols,
	  (void*)&palette, &pal_entries);
  bench ("SPS_PaletteArray");
  outp = SPS_PaletteArray (arr, type, cols, rows, 
          reduc, fastreduc, meth, gamma, autoscale, 
	  mapmin, mapmax, mapbytes, palette_code,
	  &min, &max, &prows, &pcols,
	  (void*)&palette, &pal_entries);
  bench ("SPS_PaletteArray 2nd run");

  if (palette) {
    printf ("PALETTE: 0 .. %d\n", pal_entries);
    for (i = 0; i < pal_entries && i < 10; i++) { 
      printf ("%5d : 0x%02x ", i, *(palette + palbytes * i));
      for (k = 1 ; k < palbytes ; k++) 
	printf ("0x%02x ", *(palette + palbytes * i + k));
      printf ("\n");
    }
    printf("\n");
    i = pal_entries - 10 ; if (i <0) i =0;
    for (; i < pal_entries; i++) {
      printf ("%5d : 0x%02x ", i, *(palette + palbytes * i));
      for (k = 1 ; k < palbytes ; k++) 
	printf ("0x%02x ", *(palette + palbytes * i + k));
      printf ("\n");
    }
    printf("\n");
  }
  
  printf("min max minplus %f %f %f prows pcols %d %d\n",min,max,minplus, prows, pcols);
  
  for (i = 0;  i < prows && i < 5; i++)
    for (j = 0;  j < pcols && j < 5; j++) {
      printf ("%3d %3d : %4.0lf 0x%02x ", i, j, 
	      SPS_GetZdata(arr, type, cols, rows, j, i),
	      *(outp + databytes * (pcols * i + j)));
      for (k = 1 ; k < databytes ; k++) 
	printf ("0x%02x ", *(outp + databytes * (pcols * i + j) + k));
      printf ("\n");
    }
}

