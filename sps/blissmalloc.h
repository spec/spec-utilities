#if MALLOC_DEBUG

struct pmem {
  void *data;
  long int size;
  char *file;
  int line;
  struct pmem *next;
} ;


#define malloc(N) _pmalloc(N,__FILE__,__LINE__)
#define free(N) _pfree(N,__FILE__,__LINE__)

#endif
