#include <stdio.h>
#include <stdlib.h>
#include <tk.h>
#include <sps.h>
#include <blt.h>
#include <varargs.h>

int Sps_getspeclist (ClientData, Tcl_Interp *, int , char *argv[]);
int Sps_getarraylist (ClientData , Tcl_Interp *, int , char *argv[]);
int Sps_getspecstate (ClientData , Tcl_Interp *, int , char *argv[]);
int Sps_isupdated (ClientData , Tcl_Interp *, int , char *argv[]);
int Sps_updatecounter (ClientData , Tcl_Interp *, int , char *argv[]);
int Sps_getdatarow (ClientData , Tcl_Interp *, int , char *argv[]);
int Sps_getdatacol (ClientData , Tcl_Interp *, int , char *argv[]);
int Sps_getrows (ClientData , Tcl_Interp *, int , char *argv[]);
int Sps_getcols (ClientData , Tcl_Interp *, int , char *argv[]);
int Sps_getenvstr (ClientData , Tcl_Interp *, int , char *argv[]);
int Sps_putenvstr (ClientData , Tcl_Interp *, int , char *argv[]);
int Sps_getdata (ClientData , Tcl_Interp *, int , char *argv[]);

static void printf_result(va_alist)
     va_dcl
{
  char 	   *format;
  va_list   args;
  char buf[512];
  Tcl_Interp *interp;

  va_start(args);
  interp = va_arg(args, void*);
  format = va_arg(args, char*);
  
  vsprintf(buf, format, args);
  Tcl_SetResult (interp, buf, TCL_VOLATILE); 
  va_end(args);
} 

int
Sps_Init(interp)
     Tcl_Interp *interp;		/* Interpreter for application. */
{
  /*
   * Call Tcl_CreateCommand for application-specific commands, if
   * they weren't already created by the init procedures called above.
   */
  
  Tcl_CreateCommand(interp, "SPS_GetSpecList", Sps_getspeclist,
		    (ClientData) NULL, (Tcl_CmdDeleteProc *) NULL);
  Tcl_CreateCommand(interp, "SPS_GetArrayList", Sps_getarraylist,
		    (ClientData) NULL, (Tcl_CmdDeleteProc *) NULL);
  Tcl_CreateCommand(interp, "SPS_GetSpecState", Sps_getspecstate,
		    (ClientData) NULL, (Tcl_CmdDeleteProc *) NULL);
  Tcl_CreateCommand(interp, "SPS_IsUpdated", Sps_isupdated,
		    (ClientData) NULL, (Tcl_CmdDeleteProc *) NULL);
  Tcl_CreateCommand(interp, "SPS_UpdateCounter", Sps_updatecounter,
		    (ClientData) NULL, (Tcl_CmdDeleteProc *) NULL);
  Tcl_CreateCommand(interp, "SPS_GetDataRow", Sps_getdatarow,
		    (ClientData) NULL, (Tcl_CmdDeleteProc *) NULL);
  Tcl_CreateCommand(interp, "SPS_GetDataCol", Sps_getdatacol,
		    (ClientData) NULL, (Tcl_CmdDeleteProc *) NULL);
  Tcl_CreateCommand(interp, "SPS_GetRows", Sps_getrows,
		    (ClientData) NULL, (Tcl_CmdDeleteProc *) NULL);
  Tcl_CreateCommand(interp, "SPS_GetCols", Sps_getcols,
		    (ClientData) NULL, (Tcl_CmdDeleteProc *) NULL);
  Tcl_CreateCommand(interp, "SPS_GetEnvStr", Sps_getenvstr,
		    (ClientData) NULL, (Tcl_CmdDeleteProc *) NULL);
  Tcl_CreateCommand(interp, "SPS_PutEnvStr", Sps_putenvstr,
		    (ClientData) NULL, (Tcl_CmdDeleteProc *) NULL);
  Tcl_CreateCommand(interp, "SPS_GetData", Sps_getdata,
		    (ClientData) NULL, (Tcl_CmdDeleteProc *) NULL);
  return TCL_OK;
}

int 
Sps_getspeclist (ClientData clientData, Tcl_Interp *interp, 
		 int argc, char *argv[])
{
  int i;
  Tcl_DString speclist;
  char *spec_version;

  if (argc != 1) {
    printf_result (interp, "wrong # args: %s takes no arguments", argv[0]);
    return TCL_ERROR;
  }

  Tcl_DStringInit(&speclist);

  for (i=0; spec_version = SPS_GetNextSpec (i) ; i++) {
    Tcl_DStringAppendElement(&speclist, spec_version);
  }

  Tcl_DStringResult(interp, &speclist);
  Tcl_DStringFree(&speclist);
  return TCL_OK;
}


int 
Sps_getarraylist (ClientData clientData, Tcl_Interp *interp, 
		      int argc, char *argv[])
{
  int i;
  Tcl_DString speclist;
  char *spec_version, *array;

  if (argc != 2) {
    printf_result (interp, "wrong # args: %s <version/*>", argv[0]);
    return TCL_ERROR;
  }
  
  if (*argv[1] == '*') {
    spec_version = NULL;
  } else {
    spec_version = argv[1];
  }
  
  Tcl_DStringInit(&speclist);

  for (i=0; array = SPS_GetNextArray (spec_version,i) ; i++) {
    Tcl_DStringAppendElement(&speclist, array);
  }

  Tcl_DStringResult(interp, &speclist);
  Tcl_DStringFree(&speclist);
  return TCL_OK;
}

int 
Sps_getspecstate (ClientData clientData, Tcl_Interp *interp, 
		      int argc, char *argv[])
{
  long state;
  
  if (argc != 2) {
    printf_result (interp, "wrong # args: %s <version>", argv[0]);
    return TCL_ERROR;
  }
  
  state = SPS_GetSpecState(argv[1]);
  printf_result (interp, "%d", state);
  
  if (state == -1) 
    return TCL_ERROR;
  else
    return TCL_OK;
}

int 
Sps_isupdated (ClientData clientData, Tcl_Interp *interp, 
		    int argc, char *argv[])
{
  char *spec_version , *array;
  int state;
  
  if (argc != 2 && argc != 3) {
    printf_result (interp, "wrong # args: %s <version/*> <array>", argv[0]);
    return TCL_ERROR;
  }

  if (*argv[1] == '*') {
    spec_version = NULL;
  } else {
    spec_version = argv[1];
  }
  array = argv[2];

  state = SPS_IsUpdated(spec_version, array);
  printf_result (interp, "%d", state);
  
  return TCL_OK;
}

int 
Sps_updatecounter (ClientData clientData, Tcl_Interp *interp, 
		    int argc, char *argv[])
{
  char *spec_version , *array;
  int state;
  
  if (argc != 3) {
    printf_result (interp, "wrong # args: %s <version/*> <array>", argv[0]);
    return TCL_ERROR;
  }
  
  if (*argv[1] == '*') {
    spec_version = NULL;
  } else {
    spec_version = argv[1];
  }
  array = argv[2];
  
  state = SPS_UpdateCounter(spec_version, array);
  printf_result (interp, "%d", state);
  
  return TCL_OK;
}

int 
Sps_getdata (ClientData clientData, Tcl_Interp *interp, 
	     int argc, char *argv[])
{
  Tk_PhotoHandle photo;
  Tk_PhotoImageBlock block;
  char *array, *spec_version, *photo_str, *row_str, *col_str;
  double row, col;
  double *ptr, *malloc_data;
  int spec_rows, spec_cols, p_rows, p_cols;
  void *data;
  int sub_row, sub_col, zoom_row, zoom_col;
  
  /* Check number of input arguments and asign general parameters */
  if (argc != 6) {
    printf_result (interp, 
      "wrong # args: %s <version/*> <array> <photo> <row> <cols>", argv[0]);
    return TCL_ERROR;
  }

  if (*argv[1] == '*') {
    spec_version = NULL;
  } else {
    spec_version = argv[1];
  }
  array = argv[2];
  photo_str = argv[3]; 
  row_str = argv[4];
  col_str = argv[5];
  
  /* Check type of input arguments */
  if (Tcl_GetDouble(interp, row_str, &row) != TCL_OK) {
    printf_result(interp, 
		  "wrong arg type: %s . <row> . must be an integer", argv[0]);
    return TCL_ERROR;
  }
  
  else if (Tcl_GetDouble(interp, col_str, &col) != TCL_OK) {
    printf_result(interp, 
	 "wrong arg type: %s . <cols> . must be an integer", argv[0]);
    return TCL_ERROR;
  }
  
  if ((photo = Tk_FindPhoto(interp, photo_str)) == NULL)  {
    printf_result(interp, "can not find photo %s: %s", photo_str, argv[0]);
    return TCL_ERROR;
  }
  
  if (SPS_GetArrayInfo (spec_version, array, &spec_rows, &spec_cols, NULL,
			NULL)) {
    printf_result(interp, "can not get information: %s:%s not available", 
		  spec_version, array);
    return TCL_ERROR;
  }
  
  if ((data = SPS_GetDataCopy (spec_version, array, SPS_LONG, NULL, NULL)) ==
      NULL) {
    printf_result(interp, "can not get data: %s:%s not available", 
		  spec_version, array);
    return TCL_ERROR;
  }
  
  block.pixelPtr = data;
  block.width = spec_rows;
  block.height = spec_cols;
  block.pitch = spec_rows * sizeof(long);
  block.pixelSize = sizeof(long);
  block.offset[0] = 0;
  block.offset[1] = 2;
  block.offset[2] = 1;
  
  if (row > 1) {
    zoom_row = (int) row;
    sub_row  = 1;
    spec_rows *= zoom_row;  
  } else if (row > 0) {
    zoom_row = 1; 
    sub_row  = (int) 1/row;
    spec_rows /= sub_row;  
  } else {
    zoom_row = 1; 
    sub_row  = 1;
  }


  if (col > 1) {
    zoom_col = (int) col;
    sub_col  = 1;
    spec_cols *= zoom_col;  
  } else if (col > 0) {
    zoom_col = 1; 
    sub_col  = (int) 1/col;
    spec_cols /= sub_col;  
  } else {
    zoom_col = 1; 
    sub_col  = 1;
  }

  
  Tk_PhotoPutZoomedBlock (photo, &block, 0, 0, spec_rows, spec_cols, zoom_row,
			  zoom_col, sub_row, sub_col);
  
  return TCL_OK;
}

int 
Sps_getdatarow (ClientData clientData, Tcl_Interp *interp, 
	     int argc, char *argv[])
{
  Blt_Vector *vector;
  char *array, *spec_version, *vector_str, *row_str, *col_str;
  int row, col;
  double *ptr, *malloc_data;
  int spec_rows, spec_cols;

  /* Check number of input arguments and asign general parameters */
  if (argc != 6) {
    printf_result (interp, 
      "wrong # args: %s <version/*> <array> <vector> <row> <cols>", argv[0]);
    return TCL_ERROR;
  }

  if (*argv[1] == '*') {
    spec_version = NULL;
  } else {
    spec_version = argv[1];
  }
  array = argv[2];
  vector_str = argv[3]; 
  row_str = argv[4];
  col_str = argv[5];
  
  /* Check type of input arguments */
  if (Tcl_GetInt(interp, row_str, &row) != TCL_OK) {
    printf_result(interp, 
		  "wrong arg type: %s . <row> . must be an integer", argv[0]);
    return TCL_ERROR;
  }
  
  if (*col_str == '*') 
    col = 0;
  else if (Tcl_GetInt(interp, col_str, &col) != TCL_OK) {
    printf_result(interp, 
           "wrong arg type: %s . <cols> . must be an integer or *", argv[0]);
    return TCL_ERROR;
  }
  
  if (Blt_VectorExists(interp, vector_str))  {
    if (Blt_GetVector(interp, vector_str, &vector) != TCL_OK) {
      printf_result(interp, "can not get vector %s: %s", vector_str, argv[0]);
      return TCL_ERROR;
    }
  } else {
    if (Blt_CreateVector(interp, vector_str, 0, &vector) != TCL_OK) {
      printf_result(interp,"can not create vector %s: %s",vector_str,argv[0]);
      return TCL_ERROR;
    }
  }
  
  if (SPS_GetArrayInfo (spec_version, array, &spec_rows, &spec_cols, NULL,
			NULL)) {
    printf_result(interp, "can not get information: %s:%s not available", 
		  spec_version, array);
    return TCL_ERROR;
  }
  
  if (row >= spec_rows || row < 0) {
    printf_result(interp, "rows wrong: allowed range for rows is [0..%d]", 
		  spec_rows-1);
    return TCL_ERROR;
  }

  if (col >= spec_cols || col <= 0) 
    col = spec_cols;
  
  if (Blt_VecSize(vector) < spec_cols) {
    if ((malloc_data = (double *) malloc (sizeof(double) * spec_cols)) 
	== NULL) {
      printf_result(interp, "can not malloc space for %d double prec. values", 
		    spec_cols);
      return TCL_ERROR;
    }
  } else
    malloc_data = Blt_VecData (vector);
  
  if (SPS_CopyRowFromShared (spec_version, array, malloc_data, SPS_DOUBLE,
			     row, col, NULL)) {
    printf_result(interp, "can not copy: %s:%s not available", 
		  spec_version, array);
    return TCL_ERROR;
  }
  
  if (Blt_ResetVector(vector, malloc_data , col, spec_cols,
		      TCL_DYNAMIC) != TCL_OK) {
    printf_result(interp, "couldn't copy memory: %d columns ", spec_cols);
    return TCL_ERROR;
  }
  
  return TCL_OK;
}

int 
Sps_getdatacol (ClientData clientData, Tcl_Interp *interp, 
	     int argc, char *argv[])
{
  Blt_Vector *vector;
  char *array, *spec_version, *vector_str, *row_str, *col_str;
  int row, col;
  double *ptr, *malloc_data;
  int spec_rows, spec_cols;

  /* Check number of input arguments and asign general parameters */
  if (argc != 6) {
    printf_result (interp, 
      "wrong # args: %s <version/*> <array> <vector> <col> <rows>", argv[0]);
    return TCL_ERROR;
  }

  if (*argv[1] == '*') {
    spec_version = NULL;
  } else {
    spec_version = argv[1];
  }
  array = argv[2];
  vector_str = argv[3]; 
  col_str = argv[4];
  row_str = argv[5];
  
  /* Check type of input arguments */
  if (Tcl_GetInt(interp, col_str, &col) != TCL_OK) {
    printf_result(interp, 
		  "wrong arg type: %s . <col> . must be an integer", argv[0]);
    return TCL_ERROR;
  }
  if (*row_str == '*') 
    row = 0;
  else if (Tcl_GetInt(interp, row_str, &row) != TCL_OK) {
    printf_result(interp, 
           "wrong arg type: %s . <rows> . must be an integer or *", argv[0]);
    return TCL_ERROR;
  }

  if (Blt_VectorExists(interp, vector_str))  {
    if (Blt_GetVector(interp, vector_str, &vector) != TCL_OK) {
      printf_result(interp, "can not get vector %s: %s", vector_str, argv[0]);
      return TCL_ERROR;
    }
  } else {
    if (Blt_CreateVector(interp, vector_str, 0, &vector) != TCL_OK) {
      printf_result(interp,"can not create vector %s: %s",vector_str,argv[0]);
      return TCL_ERROR;
    }
  }
  
  if (SPS_GetArrayInfo (spec_version, array, &spec_rows, &spec_cols, NULL,
			NULL)) {
    printf_result(interp, "can not get information: %s:%s not available", 
		  spec_version, array);
    return TCL_ERROR;
  }
  
  if (col >= spec_cols || col < 0) {
    printf_result(interp, "cols wrong: allowed range for cols is [0..%d]", 
		  spec_cols-1);
    return TCL_ERROR;
  }

  if (row >= spec_rows || row <= 0) 
    row = spec_rows;
  
  
  if (Blt_VecSize(vector) < spec_rows) {
    if ((malloc_data = (double *) malloc (sizeof(double) * spec_rows)) 
	== NULL) {
      printf_result(interp, "can not malloc space for %d double prec. values", 
		    spec_rows);
      return TCL_ERROR;
    }
  } else
    malloc_data = Blt_VecData (vector);

  if (SPS_CopyColFromShared (spec_version, array, malloc_data, SPS_DOUBLE,
			     col, row, NULL)) {
    printf_result(interp, "can not copy: %s:%s not available", 
		  spec_version, array);
    return TCL_ERROR;
  }
  
  if (Blt_ResetVector(vector, malloc_data , row, spec_rows,
		      TCL_DYNAMIC) != TCL_OK) {
    printf_result(interp, "couldn't copy memory: %d rows ", spec_rows);
    return TCL_ERROR;
  }
  
  return TCL_OK;
}

int 
Sps_getrows (ClientData clientData, Tcl_Interp *interp, 
		      int argc, char *argv[])
{
  int i;
  Tcl_DString speclist;
  char *spec_version, *array;
  int rows;

  if (argc != 3) {
    printf_result(interp, "wrong # args: %s <version/*> <array>" , argv[0]);
    return TCL_ERROR;
  }

  if (*argv[1] == '*') {
    spec_version = NULL;
  } else {
    spec_version = argv[1];
  }
  array = argv[2];

  if (SPS_GetArrayInfo(spec_version, array, &rows, NULL, NULL, NULL) == 0) {
    printf_result(interp, "%d",rows);
    return TCL_OK;
  } else {
    printf_result(interp, "error reading info %s: version %s array %s",
		  argv[0], spec_version, array);
    return TCL_ERROR;
  }
}

int 
Sps_getcols (ClientData clientData, Tcl_Interp *interp, 
	     int argc, char *argv[])
{
  int i;
  Tcl_DString speclist;
  char *spec_version, *array;
  int cols;

  if (argc != 3) {
    printf_result(interp, "wrong # args: %s <version/*> <array>", argv[0]);
    return TCL_ERROR;
  }

  if (*argv[1] == '*') {
    spec_version = NULL;
  } else {
    spec_version = argv[1];
  }
  array = argv[2];

  if (SPS_GetArrayInfo(spec_version, array, NULL, &cols, NULL, NULL) == 0) {
    printf_result(interp, "%d",cols);
    return TCL_OK;
  } else {
    printf_result(interp, "error reading info %s: version %s array %s",
		  argv[0], spec_version, array);
    return TCL_ERROR;
  }
}

int 
Sps_getenvstr (ClientData clientData, Tcl_Interp *interp, 
	     int argc, char *argv[])
{
  int i;
  Tcl_DString speclist;
  char *spec_version, *array, *id;
  char *result;

  if (argc != 4) {
    printf_result(interp,"wrong # args: %s <version/*> <array> <id>",argv[0]);
    return TCL_ERROR;
  }

  if (*argv[1] == '*') {
    spec_version = NULL;
  } else {
    spec_version = argv[1];
  }
  array = argv[2];
  id = argv[3];

  if ((result = SPS_GetEnvStr(spec_version, array, id)) != NULL) {
    printf_result(interp, result);
    return TCL_OK;
  } else {
    printf_result(interp, "error reading env %s: version %s array %s id %s",
		  argv[0], spec_version, array, id);
    return TCL_ERROR;
  }
}

int 
Sps_putenvstr (ClientData clientData, Tcl_Interp *interp, 
	     int argc, char *argv[])
{
  int i;
  Tcl_DString speclist;
  char *spec_version, *array, *id, *value;

  if (argc != 5) {
    printf_result(interp,
	       "wrong # args: %s <version/*> <array> <id> <value>", argv[0]);
    return TCL_ERROR;
  }

  if (*argv[1] == '*') {
    spec_version = NULL;
  } else {
    spec_version = argv[1];
  }
  array = argv[2];
  id = argv[3];
  value = argv[4];

  if (SPS_PutEnvStr(spec_version, array, id, value) == 0) {
    return TCL_OK;
  } else {
    printf_result(interp, "error setting env %s: version %s array %s %s=%s",
		  argv[0], spec_version, array, id, value);
    return TCL_ERROR;
  }
}

int Sps_createarray (ClientData clientData, Tcl_Interp *interp, 
	     int argc, char *argv[])
{
  int i;
  char *spec_version, *array;
  char *rows_str, *cols_str;
  int rows, cols;
  int result;

  if (argc != 5) {
    printf_result(interp, "wrong # args: %s <version> <array> <rows> <cols>",
		  argv[0]);
    return TCL_ERROR;
  }

  spec_version = argv[1];
  array = argv[2];
  rows_str = argv[3];
  cols_str = argv[4];

  if (Tcl_GetInt(interp, cols_str, &cols) != TCL_OK || 
      Tcl_GetInt(interp, rows_str, &rows) != TCL_OK ) {
    printf_result(interp,"wrong arg type: %s . <row> <col> . must be integer",
		  argv [0]);
    return TCL_ERROR;
  }
  
  result = SPS_CreateArray (spec_version, array, rows, cols, 2, 2);

  if (result == 0) {
    return TCL_OK;
  } else {
    printf_result(interp, "error creating array %s: version %s array %s ",
		  argv[0], spec_version, array);
    return TCL_ERROR;
  }
}












