#!/segfs/bliss/datatcl/depot/linux/bin/wish8.1
#!/segfs/bliss/datatcl/depot/hp102/bin/wish8.1
#!/usr/bin/wish 
proc update_graph {g_name id version array envdata xcol ycols} {
global update_counter update_counter_env
set updated [SPS_UpdateCounter $version $array]
if {$updated != -1 && $updated != $update_counter($id)} {
  set nopts [getval $version $envdata nopts]  
  SPS_GetDataCol $version $array ${id}_xVec $xcol $nopts 
  foreach ycol $ycols {
    SPS_GetDataCol $version $array ${id}_yVec_${ycol} $ycol $nopts
  }
  set update_counter($id) $updated
}

set envupdated [SPS_UpdateCounter $version $envdata]
if {$envupdated != -1 && $envupdated != $update_counter_env($id) } {
  $g_name configure -title [getval $version $envdata title]
  set axistitles [getval $version $envdata axistitles]
  set colorlist [getval $version $envdata colorlist]
  foreach ycol $ycols {
    $g_name element configure ${ycol}_l -label [lindex $axistitles $ycol]
    $g_name element configure ${ycol}_l -color [lindex $colorlist $ycol]
  }
  $g_name axis configure x -title [getval $version $envdata xlabel]
  $g_name axis configure y -title [getval $version $envdata ylabel]
  set update_counter_env($id) $envupdated
}

if {$updated == -1} {
  after 1000 update_graph $g_name $id $version $array $envdata $xcol "{$ycols}"
} else {
  after 300 update_graph $g_name $id $version $array $envdata $xcol "{$ycols}"
}
}

proc online_graph {g_name id version array envdata xcol ycols} {
vector create ${id}_xVec 
global update_counter update_counter_env
set update_counter($id) 0 
set update_counter_env($id) 0
graph $g_name 
foreach ycol $ycols {
  vector create ${id}_yVec_${ycol} 
  $g_name element create ${ycol}_l -xdata ${id}_xVec -ydata ${id}_yVec_${ycol}
}
update_graph $g_name $id $version $array $envdata $xcol "$ycols"
}

proc getval {version envdata id} {
return [SPS_GetEnvStr $version $envdata $id]
}

package require BLT
namespace import blt::*
load linux/sps.so
online_graph .g g1 spec SCAN_D SCAN_D_ENV 0 2 
online_graph .h h1 spec SCAN_D SCAN_D_ENV 0 {3 4}
pack .g .h




