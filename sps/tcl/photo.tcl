#!/segfs/bliss/datatcl/depot/linux/bin/wish8.1
proc update_image {} {
  set updated [SPS_IsUpdated spec image_data]
    if {$updated == 1} {
    puts "update"
    SPS_GetData spec image_data newp 1 1
  }
  after 1000 update_image
}

package require BLT
load linux/sps.so
image create photo newp 

frame .f
pack .f -fill both -expand true
canvas .f.c
pack .f.c -fill both -expand true
 .f.c create image 0 0 -anchor nw -image newp 
after 1000 update_image



