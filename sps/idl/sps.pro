forward_function spidl_isupdated, spidl_getdata, spidl_getenvstr
pro spidl_update
for i=1,100,1 do begin
  updated = spidl_isupdated ("spec", "SCAN_D")
  print, updated
  if (updated eq 1) then begin 
    x=spidl_getdata("spec","SCAN_D")
    xlabel = spidl_getenvstr ("spec","SCAN_D_ENV","xlabel")
    ylabel = spidl_getenvstr ("spec","SCAN_D_ENV","ylabel")
    title = spidl_getenvstr ("spec","SCAN_D_ENV","title")
    nopts = spidl_getenvstr ("spec","SCAN_D_ENV","nopts")
    plot, x[0,0:nopts-1], x[1,0:nopts-1], title=title, xtitle=xlabel, ytitle=ylabel 
  end
  wait, 0.5
end
return
end

pro spidl_test 
print, "Getting and putting environment variables"
print, spidl_getenvstr ("spec", "SCAN_D_ENV", "title")
print, spidl_putenvstr ("spec", "SCAN_D_ENV", "idl","is_here")
return
end

pro spidl_define
spidl_lib = "linux/sps_idl.so"
linkimage, "spidl_getspeclist", spidl_lib, MIN_ARGS=0, MAX_ARGS=0, /FUNC   
linkimage, "spidl_getarraylist", spidl_lib, MIN_ARGS=1, MAX_ARGS=1, /FUNC   
linkimage, "spidl_getdata", spidl_lib, MIN_ARGS=2, MAX_ARGS=2, /FUNC   
linkimage, "spidl_isupdated", spidl_lib, MIN_ARGS=2, MAX_ARGS=2, /FUNC   
linkimage, "spidl_getenvstr", spidl_lib, MIN_ARGS=3, MAX_ARGS=3, /FUNC   
linkimage, "spidl_putenvstr", spidl_lib, MIN_ARGS=4, MAX_ARGS=4, /FUNC   
print, "Added functions: spidl_getspeclist, spidl_getarraylist, spidl_getdata"
print, "       spidl_isupdated, spidl_getenvstr, spidl_putenvstr "
print, "from", spidl_lib
return
end







