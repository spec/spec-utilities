#include <stdio.h>
#include <stdlib.h>
#include <sps.h>
#include <varargs.h>

#include <export.h> /* idl include */

/* works but this function is somehow wrong - how can I do that right ????
   What I want to do is to have a temp variable which I can return but 
   Gettmp doesnt seem to do.
*/   

IDL_VPTR return_int (int i)
{
  static IDL_VARIABLE ret_val;
  static IDL_ALLTYPES res;
  res.i = i;
  
  IDL_StoreScalar(&ret_val, IDL_TYP_INT, &res);
  return &ret_val;
}

store_string (IDL_VPTR v, char * str) 
{
  static IDL_ALLTYPES idl_all;
  
  IDL_StrStore(&(idl_all.str), str); 
  IDL_StoreScalar(v, IDL_TYP_STRING, &idl_all); 
}

static int conv[][2] = {{IDL_TYP_BYTE, SPS_CHAR},
			{IDL_TYP_INT, SPS_SHORT},
			{IDL_TYP_LONG, SPS_LONG},
			{IDL_TYP_FLOAT, SPS_FLOAT},
			{IDL_TYP_DOUBLE, SPS_DOUBLE}, 
			{IDL_TYP_BYTE, SPS_UCHAR},     /* idl does not have */
			{IDL_TYP_INT, SPS_USHORT},   /* unsigned types */
			{IDL_TYP_LONG, SPS_ULONG}     
};

void private_free (void *data)
{
  free(data);
  return;
}

int IDL_to_Spec_type (int idl_type)
{
  int i;
  for (i=0; i < sizeof(conv)/sizeof(int)/2; i++)
    if (idl_type == conv[i][0])
      return conv[i][1];
  return -1;
}

int Spec_to_IDL_type (int spec_type)
{
  int i;
  for (i=0; i < sizeof(conv)/sizeof(int)/2; i++)
    if (spec_type == conv[i][1])
      return conv[i][0];
  return -1;
}

/* Change the next two functions to return an array of strings */
/* IDL_STRING = stype, slen, s */ 

IDL_VPTR spidl_getspeclist (int argc, IDL_VPTR *argv)
{
  int i;
  char *spec_version;
  static IDL_STRING versions[50];
  UCHAR *data;
  static IDL_LONG dim;
  
  for (i = 0, dim = 0; spec_version = SPS_GetNextSpec (i) ; dim++, i++) {
    IDL_StrStore(&(versions[i]), spec_version);
  }

  if (dim == 0) {
    dim = 1;
    IDL_StrStore(&(versions[0]), "<NONE>");
  }
  
  if ((data = (UCHAR *) malloc (sizeof(IDL_STRING) * dim)) == NULL)
    IDL_Message (IDL_M_NAMED_GENERIC, IDL_MSG_LONGJMP, 
		 "Can not allocated memory %d bytes",sizeof(IDL_STRING)* dim);

  memcpy (data, versions, sizeof(IDL_STRING) * dim);
  
  return IDL_ImportArray(1, &dim, IDL_TYP_STRING, data, 
			 (IDL_ARRAY_FREE_CB) private_free, 0); 
}

IDL_VPTR spidl_getarraylist (int argc, IDL_VPTR argv[])
{
  int i;
  char *spec_version, *arr;
  static IDL_STRING arrays[100];
  UCHAR *data;
  static IDL_LONG dim;
  
  static IDL_EZ_ARG arg_struct[] = {
    { IDL_EZ_DIM_MASK(0), IDL_TYP_MASK(IDL_TYP_STRING), IDL_EZ_ACCESS_R, 
      0, 0, 0 }
  };
  
  IDL_EzCall(argc, argv, arg_struct);

  spec_version = IDL_STRING_STR (&(argv[0]->value.str));
  if (strcmp(spec_version,"*") == NULL)
    spec_version = NULL;
  
  for (i = 0, dim = 0; arr = SPS_GetNextArray (spec_version, i); dim++, i++) {
    IDL_StrStore(&(arrays[i]), arr);
  }
  
  if (dim == 0) {
    dim = 1;
    IDL_StrStore(&(arrays[0]), "<NONE>");
  }

  if ((data = (UCHAR *) malloc (sizeof(IDL_STRING) * dim)) == NULL)
    IDL_Message (IDL_M_NAMED_GENERIC, IDL_MSG_LONGJMP, 
		 "Can not allocated memory %d bytes",sizeof(IDL_STRING)* dim);

  memcpy (data, arrays, sizeof(IDL_STRING) * dim);
  
  IDL_EzCallCleanup (argc, argv, arg_struct);
  return IDL_ImportArray(1, &dim, IDL_TYP_STRING, data, 
			 (IDL_ARRAY_FREE_CB) private_free, 0); 
}

IDL_VPTR spidl_getspecstate (int argc, IDL_VPTR argv[])
{
  int i;
  char *spec_version, *arr;
  
  static IDL_EZ_ARG arg_struct[] = {
    { IDL_EZ_DIM_MASK(0), IDL_TYP_MASK(IDL_TYP_STRING), IDL_EZ_ACCESS_R, 
      0, 0, 0 }
  };
  
  IDL_EzCall(argc, argv, arg_struct);

  spec_version = IDL_STRING_STR (&(argv[0]->value.str));
  if (strcmp(spec_version,"*") == NULL)
    spec_version = NULL;
  
  IDL_EzCallCleanup (argc, argv, arg_struct);
  return return_int(SPS_GetSpecState(spec_version));
}

private_detach (void *data)
{
  SPS_ReturnDataPointer(data);
}

IDL_VPTR spidl_getdata (int argc, IDL_VPTR argv[], char *argk)
{
  int rows, cols;
  char *spec_version, *array;
  IDL_LONG array_dim[2];
  void *data;
  int type, idl_type, spec_type, flags;
  static IDL_LONG attachflag;
  IDL_ARRAY_FREE_CB freeCB;
  IDL_VPTR plain_args[20]; /*Need space for plain args as written in doc */
  static IDL_LONG attflag;
  int argp;

  static IDL_EZ_ARG arg_struct[] = {
    { IDL_EZ_DIM_MASK(0), IDL_TYP_MASK(IDL_TYP_STRING), IDL_EZ_ACCESS_R, 
      0, 0, 0 },
    { IDL_EZ_DIM_MASK(0), IDL_TYP_MASK(IDL_TYP_STRING), IDL_EZ_ACCESS_R, 
      0, 0, 0 }
  };
  
  static IDL_KW_PAR kw_pars[] = {
    { "ATTACH", IDL_TYP_LONG, 1, IDL_KW_ZERO, 0, IDL_CHARA(attflag)},
    { NULL } 
  };
  
  argp = IDL_KWGetParams (argc, argv, argk, kw_pars, plain_args, 1);
  IDL_EzCall(argp, plain_args, arg_struct);
  
  spec_version = IDL_STRING_STR (&(argv[0]->value.str));
  if (strcmp(spec_version,"*") == NULL)
    spec_version = NULL;
  
  array = IDL_STRING_STR (&(argv[1]->value.str));

  if (SPS_GetArrayInfo (spec_version, array, &rows, &cols, &type, &flags))
    IDL_Message (IDL_M_NAMED_GENERIC, IDL_MSG_LONGJMP, 
		 "Can not connect to %s:%s",spec_version, array);
  
  idl_type = Spec_to_IDL_type(type);
  spec_type = IDL_to_Spec_type (idl_type); /* Is different of type for */
  /* unsigned types - not in idl */   
  
  if (!attflag) {
    if ((data = (void *) malloc (rows * cols * SPS_Size(spec_type))) == NULL)
      IDL_Message (IDL_M_NAMED_GENERIC, IDL_MSG_LONGJMP, 
        "Can not alloc memory %dx%dx%d ",rows,cols, SPS_Size(spec_type));
    if (SPS_CopyFromShared (spec_version, array, data, spec_type, rows *cols))
      IDL_Message (IDL_M_NAMED_GENERIC, IDL_MSG_LONGJMP, 
		   "Can not copy from %s:%s",spec_version, array);
    freeCB = (IDL_ARRAY_FREE_CB) private_free;
  } else {
    struct private_detach_info *pdi;
    /* problem here for unsigned types - fix later */
    if ((data = SPS_GetDataPointer (spec_version, array, 1)) == NULL)
      IDL_Message (IDL_M_NAMED_GENERIC, IDL_MSG_LONGJMP, 
		   "Can not attach to %s:%s",spec_version, array);
    freeCB = (IDL_ARRAY_FREE_CB) private_detach; 
  }
  
  array_dim[0] = cols; 
  array_dim[1] = rows; 
  
  IDL_KWCleanup (IDL_KW_CLEAN_ALL);
  IDL_EzCallCleanup (argp, plain_args, arg_struct);
  return  IDL_ImportArray (2, array_dim, idl_type, data, freeCB, NULL);
}

void spidl_getarrayinfo (int argc, IDL_VPTR argv[], char *argk)
{
  int rows, cols, type, flags;
  char *spec_version, *array;
  IDL_LONG array_dim[2];
  IDL_VPTR plain_args[20]; /*Need space for plain args as written in doc */
  int argp;
  static IDL_VPTR idlv_rows, idlv_cols, idlv_type, idlv_flags;
  static IDL_ALLTYPES newval;

  static IDL_EZ_ARG arg_struct[] = {
    { IDL_EZ_DIM_MASK(0), IDL_TYP_MASK(IDL_TYP_STRING), IDL_EZ_ACCESS_R, 
      0, 0, 0 },
    { IDL_EZ_DIM_MASK(0), IDL_TYP_MASK(IDL_TYP_STRING), IDL_EZ_ACCESS_R, 
      0, 0, 0 }
  };
  
  static IDL_KW_PAR kw_pars[] = {
    IDL_KW_FAST_SCAN,
    { "COLS",IDL_TYP_UNDEF,1,IDL_KW_OUT|IDL_KW_ZERO,0,IDL_CHARA(idlv_cols)},
    { "FLAGS",IDL_TYP_UNDEF,1,IDL_KW_OUT|IDL_KW_ZERO,0,IDL_CHARA(idlv_flags)},
    { "ROWS",IDL_TYP_UNDEF,1,IDL_KW_OUT|IDL_KW_ZERO,0,IDL_CHARA(idlv_rows)},
    { "TYPE",IDL_TYP_UNDEF,1,IDL_KW_OUT|IDL_KW_ZERO,0,IDL_CHARA(idlv_type)},
    { NULL } 
  };
  
  argp = IDL_KWGetParams (argc, argv, argk, kw_pars, plain_args, 1);
  IDL_EzCall(argp, plain_args, arg_struct);
  
  spec_version = IDL_STRING_STR (&(argv[0]->value.str));
  if (strcmp(spec_version,"*") == NULL)
    spec_version = NULL;
  
  array = IDL_STRING_STR (&(argv[1]->value.str));

  if (SPS_GetArrayInfo (spec_version, array, &rows, &cols, &type, &flags))
    IDL_Message (IDL_M_NAMED_GENERIC, IDL_MSG_LONGJMP, 
		 "Can not connect to %s:%s",spec_version, array);

  if (idlv_rows) { 
    newval.l = rows; 
    IDL_StoreScalar(idlv_rows, IDL_TYP_LONG, &newval);
  }

  if (idlv_cols) { 
    newval.l = cols; 
    IDL_StoreScalar(idlv_cols, IDL_TYP_LONG, &newval);
  }

  if (idlv_type) { 
    newval.l = Spec_to_IDL_type(type); 
    IDL_StoreScalar(idlv_type, IDL_TYP_LONG, &newval);
  }

  if (idlv_flags) { 
    newval.l = flags; 
    IDL_StoreScalar(idlv_flags, IDL_TYP_LONG, &newval);
  }

  IDL_KWCleanup (IDL_KW_CLEAN_ALL);
  IDL_EzCallCleanup (argp, plain_args, arg_struct);
  return;
}

void spidl_putdata (int argc, IDL_VPTR argv[])
{
  int rows, cols;
  char *spec_version, *array;
  static char buf[512]; 
  IDL_LONG array_dim[2];
  void *data, *idl_data;
  IDL_VPTR idl_array;
  int idl_type, idl_cols, idl_rows;
  int spec_type, idl_items;

  static IDL_EZ_ARG arg_struct[] = {
    { IDL_EZ_DIM_MASK(0), IDL_TYP_MASK(IDL_TYP_STRING), IDL_EZ_ACCESS_R, 
      0, 0, 0 },
    { IDL_EZ_DIM_MASK(0), IDL_TYP_MASK(IDL_TYP_STRING), IDL_EZ_ACCESS_R, 
      0, 0, 0 },
    { IDL_EZ_DIM_MASK(1)|IDL_EZ_DIM_MASK(2), IDL_TYP_B_ALL, IDL_EZ_ACCESS_R, 
      0, 0, 0 }
  };
  
  IDL_EzCall(argc, argv, arg_struct);

  spec_version = IDL_STRING_STR (&(argv[0]->value.str));
  if (strcmp(spec_version,"*") == NULL)
    spec_version = NULL;
  
  array = IDL_STRING_STR (&(argv[1]->value.str));
  idl_array = argv[2];

  idl_data = (void *) idl_array->value.arr->data;
  idl_cols = idl_array->value.arr->dim[0];
  if (idl_array->value.arr->n_dim == 2) 
    idl_rows = idl_array->value.arr->dim[1];
  else
    idl_rows = 1;
  idl_type = idl_array->type;
  idl_items = idl_array->value.arr->n_elts;
  spec_type = IDL_to_Spec_type (idl_type);

  if (SPS_CopyToShared(spec_version, array, idl_data, spec_type, idl_items))
    IDL_Message (IDL_M_NAMED_GENERIC, IDL_MSG_LONGJMP, 
		 "Can not connect to %s:%s",spec_version, array);
  
  IDL_EzCallCleanup (argc, argv, arg_struct);
  return;
}

void spidl_createarray (int argc, IDL_VPTR argv[])
{
  int rows, cols;
  char *spec_version, *array;
  static char buf[512]; 
  IDL_LONG array_dim[2];
  void *data, *idl_data;
  IDL_VPTR idl_array;
  int idl_type, idl_cols, idl_rows;
  int spec_type, idl_items;

  static IDL_EZ_ARG arg_struct[] = {
    { IDL_EZ_DIM_MASK(0), IDL_TYP_MASK(IDL_TYP_STRING), IDL_EZ_ACCESS_R, 
      0, 0, 0 },
    { IDL_EZ_DIM_MASK(0), IDL_TYP_MASK(IDL_TYP_STRING), IDL_EZ_ACCESS_R, 
      0, 0, 0 },
    { IDL_EZ_DIM_MASK(1)|IDL_EZ_DIM_MASK(2), IDL_TYP_B_ALL, IDL_EZ_ACCESS_R, 
      0, 0, 0 }
  };
  
  IDL_EzCall(argc, argv, arg_struct);

  spec_version = IDL_STRING_STR (&(argv[0]->value.str));
  if (strcmp(spec_version,"*") == NULL)
    spec_version = NULL;
  
  array = IDL_STRING_STR (&(argv[1]->value.str));
  idl_array = argv[2];

  idl_data = (void *) idl_array->value.arr->data;
  idl_cols = idl_array->value.arr->dim[0];
  if (idl_array->value.arr->n_dim == 2) 
    idl_rows = idl_array->value.arr->dim[1];
  else
    idl_rows = 1;
  idl_type = idl_array->type;
  idl_items = idl_array->value.arr->n_elts;
  spec_type = IDL_to_Spec_type (idl_type);

  
  if (SPS_CreateArray(spec_version, array, idl_rows, idl_cols, spec_type, 2)) 
    IDL_Message (IDL_M_NAMED_GENERIC, IDL_MSG_LONGJMP, 
		 "Can not create array %s:%s",spec_version, array);
  
  if (SPS_CopyToShared(spec_version, array, idl_data, spec_type, idl_items))
    IDL_Message (IDL_M_NAMED_GENERIC, IDL_MSG_LONGJMP, 
		 "Can not connect to %s:%s",spec_version, array);
  
  IDL_EzCallCleanup (argc, argv, arg_struct);
  return;
}

void spidl_cleanup (int argc, IDL_VPTR argv[])
{
  SPS_CleanUpAll();
  return;
}

IDL_VPTR spidl_isupdated (int argc, IDL_VPTR argv[])
{
  int rows, cols;
  char *spec_version, *array;
 
  static IDL_EZ_ARG arg_struct[] = {
    { IDL_EZ_DIM_MASK(0), IDL_TYP_MASK(IDL_TYP_STRING), IDL_EZ_ACCESS_R, 
      0, 0, 0 },
    { IDL_EZ_DIM_MASK(0), IDL_TYP_MASK(IDL_TYP_STRING), IDL_EZ_ACCESS_R, 
      0, 0, 0 }
  };
  
  IDL_EzCall(argc, argv, arg_struct);
  
  spec_version = IDL_STRING_STR (&(argv[0]->value.str));
  if (strcmp(spec_version,"*") == NULL)
    spec_version = NULL;
  
  array = IDL_STRING_STR (&(argv[1]->value.str));

  IDL_EzCallCleanup (argc, argv, arg_struct);
  return return_int (SPS_IsUpdated (spec_version, array));
}

IDL_VPTR spidl_updatecounter (int argc, IDL_VPTR argv[])
{
  int rows, cols;
  char *spec_version, *array;
 
  static IDL_EZ_ARG arg_struct[] = {
    { IDL_EZ_DIM_MASK(0), IDL_TYP_MASK(IDL_TYP_STRING), IDL_EZ_ACCESS_R, 
      0, 0, 0 },
    { IDL_EZ_DIM_MASK(0), IDL_TYP_MASK(IDL_TYP_STRING), IDL_EZ_ACCESS_R, 
      0, 0, 0 }
  };
  
  IDL_EzCall(argc, argv, arg_struct);
  
  spec_version = IDL_STRING_STR (&(argv[0]->value.str));
  if (strcmp(spec_version,"*") == NULL)
    spec_version = NULL;
  
  array = IDL_STRING_STR (&(argv[1]->value.str));

  IDL_EzCallCleanup (argc, argv, arg_struct);
  return return_int (SPS_UpdateCounter (spec_version, array));
}

void spidl_updatedone (int argc, IDL_VPTR argv[])
{
  int rows, cols;
  char *spec_version, *array;
 
  static IDL_EZ_ARG arg_struct[] = {
    { IDL_EZ_DIM_MASK(0), IDL_TYP_MASK(IDL_TYP_STRING), IDL_EZ_ACCESS_R, 
      0, 0, 0 },
    { IDL_EZ_DIM_MASK(0), IDL_TYP_MASK(IDL_TYP_STRING), IDL_EZ_ACCESS_R, 
      0, 0, 0 }
  };
  
  IDL_EzCall(argc, argv, arg_struct);
  
  spec_version = IDL_STRING_STR (&(argv[0]->value.str));
  if (strcmp(spec_version,"*") == NULL)
    spec_version = NULL;
  
  array = IDL_STRING_STR (&(argv[1]->value.str));

  IDL_EzCallCleanup (argc, argv, arg_struct);
  SPS_UpdateDone (spec_version, array);
  return;
}

void spidl_getenvinfo (int argc, IDL_VPTR *argv, char *argk)
{
  char *spec_version, *array, *spec_array;
  static IDL_VPTR plain_args[20]; /*space for plain args as written in doc */
  int argp;
  static IDL_VPTR idlv_nopts, idlv_title, idlv_xl, idlv_yl, idlv_axis,
    idlv_xmin, idlv_xmax;
  static IDL_ALLTYPES newval;
  void *value;
  static IDL_STRING idl_str;
  int rows, cols, i;
  char tempbuf [700];
  double xmin = 0;

  static IDL_EZ_ARG arg_struct[] = {
    { IDL_EZ_DIM_MASK(0), IDL_TYP_MASK(IDL_TYP_STRING), IDL_EZ_ACCESS_R, 
      0, 0, 0 },
    { IDL_EZ_DIM_MASK(0), IDL_TYP_MASK(IDL_TYP_STRING), IDL_EZ_ACCESS_R, 
      0, 0, 0 }
  };
  
  static IDL_KW_PAR kw_pars[] = {
    {"AXISTITLES",IDL_TYP_STRING,1,IDL_KW_OUT|IDL_KW_ZERO,0,
                                                       IDL_CHARA(idlv_axis)},
    {"NOPTS",IDL_TYP_UNDEF,1,IDL_KW_OUT|IDL_KW_ZERO,0,IDL_CHARA(idlv_nopts)},
    {"TITLE",IDL_TYP_STRING,1,IDL_KW_OUT|IDL_KW_ZERO,0,IDL_CHARA(idlv_title)},
    {"XLABEL",IDL_TYP_STRING,1,IDL_KW_OUT|IDL_KW_ZERO,0,IDL_CHARA(idlv_xl)},
    {"XMAX",IDL_TYP_STRING,1,IDL_KW_OUT|IDL_KW_ZERO,0,IDL_CHARA(idlv_xmax)},
    {"XMIN",IDL_TYP_STRING,1,IDL_KW_OUT|IDL_KW_ZERO,0,IDL_CHARA(idlv_xmin)},
    {"YLABEL",IDL_TYP_STRING,1,IDL_KW_OUT|IDL_KW_ZERO,0,IDL_CHARA(idlv_yl)},
    { NULL } 
  };
  
  argp = IDL_KWGetParams (argc, argv, argk, kw_pars, plain_args, 1);
  IDL_EzCall(argp, plain_args, arg_struct);
  
  spec_version = IDL_STRING_STR (&(argv[0]->value.str));
  if (strcmp(spec_version,"*") == NULL)
    spec_version = NULL;
  
  /* Append _ENV to the array name to get the name of the env shared array */
  spec_array = IDL_STRING_STR (&(argv[1]->value.str));
  if ((array = malloc (strlen (spec_array) + 1 + strlen ("_ENV"))) == 0) 
    IDL_Message (IDL_M_NAMED_GENERIC, IDL_MSG_LONGJMP, "No memory left <100");
  strcpy (array,spec_array);
  strcat (array, "_ENV");
  
  if (idlv_nopts) { 
    if ((value = SPS_GetEnvStr (spec_version, array, "nopts")) != NULL) 
      newval.l = atoi (value); 
    else {
      if (SPS_GetArrayInfo (spec_version, spec_array, &rows, NULL,NULL,NULL))
	newval.l = -1;
      else
	newval.l = rows;
    }
    IDL_StoreScalar(idlv_nopts, IDL_TYP_LONG, &newval);
  }

  if (idlv_xmin) { 
    if ((value = SPS_GetEnvStr (spec_version, array, "xmin")) != NULL) 
      sscanf(value, "%lf", &newval.d);
    else {
      newval.d = 0;
    }
    xmin = newval.d;
    IDL_StoreScalar(idlv_xmin, IDL_TYP_DOUBLE, &newval);
  }

  if (idlv_xmax) { 
    if ((value = SPS_GetEnvStr (spec_version, array, "xmax")) != NULL) 
      sscanf(value, "%lf", &newval.d);
    else {
      if (idlv_xmin == 0) 
	newval.d = 0;
      else
	newval.d = xmin;
    }
    IDL_StoreScalar(idlv_xmax, IDL_TYP_DOUBLE, &newval);
  }
  
  if (idlv_title) { 
    if ((value = SPS_GetEnvStr (spec_version, array, "title")) != NULL) { 
      store_string (idlv_title, value); 
    } else 
      store_string (idlv_title, spec_array);
  }
  
  if (idlv_xl) { 
    if ((value = SPS_GetEnvStr (spec_version, array, "xlabel")) != NULL) { 
      store_string (idlv_xl, value); 
    } else 
      store_string (idlv_xl, "X");
  }

  if (idlv_yl) { 
    if ((value = SPS_GetEnvStr (spec_version, array, "ylabel")) != NULL) { 
      store_string (idlv_yl, value); 
    } else 
      store_string (idlv_yl, "Y");
  }

  if (idlv_axis) { 
    if ((value = SPS_GetEnvStr (spec_version, array, "axistitles")) != NULL){ 
      store_string (idlv_axis, value); 
    } else { 
      if (SPS_GetArrayInfo (spec_version, spec_array, NULL, &cols, NULL,NULL))
	store_string (idlv_axis, "");
      else {
	strcpy(tempbuf,"Col1");
	for (i = 1; i < cols && i < 99; i++) 
	  sprintf(tempbuf, "%s  Col%d", tempbuf, i+1);
	store_string (idlv_axis, tempbuf);
      }
    }
  }
  
  IDL_KWCleanup (IDL_KW_CLEAN_ALL);
  IDL_EzCallCleanup (argp, plain_args, arg_struct);
  return;
}

IDL_VPTR spidl_getenvstr (int argc, IDL_VPTR argv[])
{
  char *spec_version, *array, *id, *value;
  static char buf[512];

  static IDL_EZ_ARG arg_struct[] = {
    { IDL_EZ_DIM_MASK(0), IDL_TYP_MASK(IDL_TYP_STRING), IDL_EZ_ACCESS_R, 
      0, 0, 0 },
    { IDL_EZ_DIM_MASK(0), IDL_TYP_MASK(IDL_TYP_STRING), IDL_EZ_ACCESS_R, 
      0, 0, 0 },
    { IDL_EZ_DIM_MASK(0), IDL_TYP_MASK(IDL_TYP_STRING), IDL_EZ_ACCESS_R, 
      0, 0, 0 }
  };
  
  IDL_EzCall(argc, argv, arg_struct);

  spec_version = IDL_STRING_STR (&(argv[0]->value.str));
  if (strcmp(spec_version,"*") == NULL)
    spec_version = NULL;
  
  array = IDL_STRING_STR (&(argv[1]->value.str));
  id = IDL_STRING_STR (&(argv[2]->value.str));

  if ((value = SPS_GetEnvStr (spec_version, array, id)) == NULL) {
    strcpy(buf, "<undef>");
  } else {
    strncpy(buf, value, 512);
  }

  IDL_EzCallCleanup (argc, argv, arg_struct);
  return IDL_StrToSTRING(buf);
}  

void spidl_putenvstr (int argc, IDL_VPTR argv[])
{
  char *spec_version, *array, *id, *value;
  int res;

  static IDL_EZ_ARG arg_struct[] = {
    { IDL_EZ_DIM_MASK(0), IDL_TYP_MASK(IDL_TYP_STRING), IDL_EZ_ACCESS_R, 
      0, 0, 0 },
    { IDL_EZ_DIM_MASK(0), IDL_TYP_MASK(IDL_TYP_STRING), IDL_EZ_ACCESS_R, 
      0, 0, 0 },
    { IDL_EZ_DIM_MASK(0), IDL_TYP_MASK(IDL_TYP_STRING), IDL_EZ_ACCESS_R, 
      0, 0, 0 },
    { IDL_EZ_DIM_MASK(0), IDL_TYP_MASK(IDL_TYP_STRING), IDL_EZ_ACCESS_R, 
      0, 0, 0 }
  };
  
  IDL_EzCall(argc, argv, arg_struct);

  spec_version = IDL_STRING_STR (&(argv[0]->value.str));
  if (strcmp(spec_version,"*") == NULL)
    spec_version = NULL;
  
  array = IDL_STRING_STR (&(argv[1]->value.str));
  id = IDL_STRING_STR (&(argv[2]->value.str));
  value = IDL_STRING_STR (&(argv[3]->value.str));

  res = SPS_PutEnvStr (spec_version, array, id, value);
  IDL_EzCallCleanup (argc, argv, arg_struct);

  return;
}  

/* This does not work if we call it here - still I maintain this method too */
int spidl_xxx (int argc, IDL_VPTR *argv)
{
  static IDL_SYSFUN_DEF new_pros[] = {
    {(IDL_FUN_RET) spidl_getspeclist, "spidl_getspeclist", 1, 1} ,
    {(IDL_FUN_RET) spidl_getarraylist, "spidl_getarraylist", 1, 1} ,
    {(IDL_FUN_RET) spidl_getdata, "spidl_getdata", 2, 2 } ,
    {(IDL_FUN_RET) spidl_isupdated, "spidl_isupdated", 2, 2} ,
    {(IDL_FUN_RET) spidl_getenvstr, "spidl_getenvstr", 3, 3}
  };

  if (!IDL_AddSystemRoutine(new_pros, FALSE, 4)) {
    IDL_Message (IDL_M_GENERIC, IDL_MSG_RET, 
		 "SPIDL: Error adding system routines");
  }
  return 0;
}























































