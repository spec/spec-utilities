Pro arrows
; 
; HP/UX: Why don't the arrow keys work properly?
;
; Sometimes on HPUX machines, the arrow keys will not work correctly in
; IDL, making it hard for the user to use command recall or edit text 
; on the command line.
;
; To fix this, the user should add the following lines to their IDL_STARTUP 
; file. These calls to DEFINE_KEY will cause IDL to recognize the 
; non-standard key codes as the arrow keys, restoring command recall 
; functionality.
;
DEFINE_KEY, ESC=string([27B, 79B, 68B]), 'ALT_L', /BACK_CHAR
DEFINE_KEY, ESC=string([27B, 79B, 67B]), 'ALT_R', /FORWARD_CHAR
DEFINE_KEY, ESC=string([27B, 79B, 65B]), 'ALT_UP', /PREVIOUS_LINE
DEFINE_KEY, ESC=string([27B, 79B, 66B]), 'ALT_DN', /NEXT_LINE
end

