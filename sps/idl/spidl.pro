pro spidl
spidl_lib = "linux/sps_idl.so"
linkimage, "spidl_getspeclist", spidl_lib, MIN_ARGS=0, MAX_ARGS=0, /FUNC   
linkimage, "spidl_getarraylist", spidl_lib, MIN_ARGS=1, MAX_ARGS=1, /FUNC   
linkimage, "spidl_getdata", spidl_lib, MIN_ARGS=2, MAX_ARGS=2,/FUNC,/KEYWORDS 
linkimage, "spidl_isupdated", spidl_lib, MIN_ARGS=2, MAX_ARGS=2, /FUNC   
linkimage, "spidl_updatecounter", spidl_lib, MIN_ARGS=2, MAX_ARGS=2, /FUNC   
linkimage, "spidl_getenvinfo", spidl_lib, MIN_ARGS=2, MAX_ARGS=2, /KEYWORDS   
linkimage, "spidl_getenvstr", spidl_lib, MIN_ARGS=3, MAX_ARGS=3, /FUNC   
linkimage, "spidl_getspecstate", spidl_lib, MIN_ARGS=1, MAX_ARGS=1, /FUNC
linkimage, "spidl_putenvstr", spidl_lib, MIN_ARGS=4, MAX_ARGS=4
linkimage, "spidl_putdata", spidl_lib, MIN_ARGS=3, MAX_ARGS=3
linkimage, "spidl_createarray", spidl_lib, MIN_ARGS=3, MAX_ARGS=3
linkimage, "spidl_getarrayinfo", spidl_lib, MIN_ARGS=2, MAX_ARGS=2, /KEYWORDS
linkimage, "spidl_cleanup", spidl_lib, MIN_ARGS=0, MAX_ARGS=0
linkimage, "spidl_updatedone", spidl_lib, MIN_ARGS=2, MAX_ARGS=2
print,"Added functions: spidl_getspeclist, spidl_getarraylist, spidl_getdata"
print," spidl_isupdated, spidl_getenvstr, spidl_putenvstr, spidl_getarrayinfo"
print," spidl_putdata, spidl_createarray, spidl_cleanup, spidl_updatecounter" 
print," spidl_updatedone, spidl_getenvinfo"
print, " from:  ", spidl_lib
return
end















