#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <math.h>

#define OUTFORMAT "%f"  

#define MAXLLEN 1000
#define MAXELEMENTS 100
#define MAXMOT 100
#define MAXNAME 100
#define MAXPERLINE 6
#define MAXCOLUMNS 100

#define TRUE 1
#define FALSE 0

#define WFLAG 1
#define SFLAG 2
#define EFLAG 4
#define TFLAG 8
#define AFLAG 16

typedef struct {
  int	optind;	      /* index of which argument is next	*/ 
  char   *optarg;     /* pointer to argument of current option */
  int	opterr;	      /* allow error message	*/
} ANAOPTSTRUCT;

static ANAOPTSTRUCT gos = {1,"\0",0};

typedef struct {
  unsigned int	flag;
  int from;
  int to;
  char filen[MAXLLEN];
  char columns[MAXLLEN];
  char format[MAXLLEN];
} ARGSTRUCT;

/* Keep it simple - the info is one structure */

typedef struct {
  char o_arr[MAXMOT][MAXNAME];
  float p_arr[MAXMOT];
  int o_cnt;
  int p_cnt;
  int d_cnt;
} INFOSTRUCT;

typedef struct LINESTRUCT {
  char* line;
  int len;
  struct LINESTRUCT *next;
} LINESTRUCT ;

char * basen( char *);

main(argc , argv )
int argc;
char *argv[];
{
  char lbuf[MAXLLEN],buf[MAXLLEN];
  char *elem[MAXELEMENTS];
  int ii,no=-1,lastno = -1;
  FILE *fd;
  
  ARGSTRUCT as = {0,0,100000,"","all",OUTFORMAT}; /*default options*/
  INFOSTRUCT is;

  is.o_cnt=is.p_cnt=0;

  /* get default options from name */
  if (strcmp(basen(argv[0]),"waf") == 0)
    as.flag = WFLAG;
  if (strcmp(basen(argv[0]),"lscans") == 0)
    as.flag = SFLAG;
  if (strcmp(basen(argv[0]),"extract") == 0)
    as.flag = EFLAG;
  if (strcmp(basen(argv[0]),"extract_scan") == 0)
    as.flag = AFLAG;
  if (strcmp(basen(argv[0]),"extract_tab") == 0)
    as.flag = EFLAG & TFLAG;
  
  if (readarg (argc,argv,&as) == TRUE) { /* Error or help in command line */
    fprintf (stderr,
	     "Usage: %s [-f <sno>] [-t <sno>] [-wes] -[j] [-c xx] [-o xx] [filename]\n",
	     argv[0]);
    fprintf (stderr,"   -f : from scanno (default 0)\n");
    fprintf (stderr,"   -t : to scanno (default <fromscanno> or last)\n");
    fprintf (stderr,"   -w : wa motor positions of selected scans\n");
    fprintf (stderr,"   -e : extract data\n");
    fprintf (stderr,"   -s : print scanno\n");
    fprintf (stderr,"   -a : extract whole scan\n");
    fprintf (stderr,"   -j : jump - insert tab\n");
    fprintf (stderr,"   -c : column 'all' or comma separated list (0,6,9)\n");
    fprintf (stderr,"   -o : output format (default %f)\n");
    fprintf (stderr,"      : filename (default stdin)\n");
    exit(-1);
  }
  
  /* Read the file */
  if (*as.filen == '\0')
    fd = stdin;
  else
    if ((fd = fopen(as.filen,"r")) == (FILE *) NULL) {
      fprintf (stderr, "Could not open %s for reading\n",as.filen);
      exit(-1);
    }
  
  while (fgets(lbuf,MAXLLEN,fd) != (char*) NULL) {
    if (strncmp(lbuf,"#O",2) == 0) {
      int ne = split(lbuf,elem,"  ",1);
      if (*(lbuf+2) == '0') 
	is.o_cnt =0; 
      for (ii=1;ii<ne;ii++) 
	strncpy(is.o_arr[is.o_cnt++],elem[ii],MAXNAME-1);
    } 
    if (strncmp(lbuf,"#P",2) == 0) {
      int ne = split(lbuf,elem," ",1);
      for (ii=1;ii<ne;ii++) 
	sscanf(elem[ii],"%f",&is.p_arr[is.p_cnt++]);
    }
    if (strncmp(lbuf,"#S",2) == 0) {
      sscanf(lbuf,"%*s %d",&no);
      printone (lastno,&is,&as); 
      lastno = no;
      is.p_cnt=0;
    }
    saveline(lbuf,no,&as);
  }
  printone(lastno,&is,&as);
  fclose(fd); 
  exit(0);
}

char *basen(in) 
char *in;
{
  char *out=in+strlen(in);
  while (out>=in && *out != '/' && *out != ':')
    out--;
  return (out+1);
}

LINESTRUCT *firstline=(LINESTRUCT*) NULL;
LINESTRUCT *lastline=(LINESTRUCT*) NULL;

int printone(no,is,as)
     int no;
     ARGSTRUCT *as;
     INFOSTRUCT *is;
{
  int lc,ii,sav;
  if (no != -1 && no >= as->from && no <= as->to) {
    if (as->flag&WFLAG) {
      printf("\nScan %d:\n",no);
      for (ii=0;ii<is->o_cnt;) { 
	for (lc=0,sav=ii;(lc<MAXPERLINE) && (ii<is->o_cnt);ii++,lc++) 
	  printf("%11.11s ",is->o_arr[ii]);
	printf("\n");
	for (lc=0,ii=sav;(lc<MAXPERLINE) && (ii<is->o_cnt);ii++,lc++) 
	  printf("%11.9g ",is->p_arr[ii]);
	printf("\n");
      }
    }
    if (as->flag&EFLAG || as->flag&AFLAG) {
      printline(as);
    }
    if (as->flag&SFLAG) 
      printf("%d ",no);
  }
}

  
int saveline(str,no,as)
     char *str;
     int no;
     ARGSTRUCT *as;
{
  LINESTRUCT *line;
  char *cp;

  if (!(as->flag&EFLAG) && !(as->flag&AFLAG))
    return;

  if (no >= as->from && no <= as->to) {
    if (!(as->flag&AFLAG)) {
      /* Ignore Comments and empty line if a flag not set */
      if (*str == '#') {
	return;
      }
      for (cp=str;isspace(*cp);cp++) ;
      if (!*cp)
	return;
    }
    if (((line = (LINESTRUCT *) malloc(sizeof(LINESTRUCT))) == 
	 (LINESTRUCT *) NULL) || ((line->line = 
	    (char *) malloc((line->len=strlen(str))+1)) == (char*) NULL)) {
      fprintf(stderr,"Can not allocate memory\n");
      exit(1);
    }
    line->next = (LINESTRUCT*) NULL;
    memcpy(line->line,str,line->len+1);
    if (!firstline)
      firstline=line;
    if (lastline)
      lastline->next=line;
    lastline=line;
  }
}

int printline(as)
ARGSTRUCT *as;
{
  LINESTRUCT *line;
  for (line = firstline; line != (LINESTRUCT *) NULL; line=line->next) 
    printoneline(line->line,as);
  deletelines();
}

int printoneline(line,as)
     char * line;
     ARGSTRUCT *as;
{
  char *spres[MAXCOLUMNS];
  static int spind[MAXCOLUMNS];
  int nc,i,ind;
  char format[20];
  static int firsttime = 1; 
  static int allflag = 1;
  static int maxind=0;
  double dvalue;
  if (as->flag&AFLAG) {
    printf("%s",line);
  }
  else {
    if (firsttime) {
      if (strcmp(as->columns,"ALL")&&strcmp(as->columns,"all")) {
	maxind = nc = split(as->columns,spres,",",1);
	for (i=0;i<nc;i++) 
	  if (((ind=atoi(spres[i]))>=0)&&(ind <= MAXCOLUMNS))
	    spind[i] = ind;
	splitfree(spres,nc);
      } else 
	maxind=-1;
      firsttime = 0;
    }
    nc = split(line,spres," ",0);
    sprintf(format,"%s%s",as->format,as->flag&TFLAG?"\t":" ");
    if (maxind == -1) {
      for (i=0;i<nc;i++) {
	sscanf(spres[i],"%lf",&dvalue);
	printf(format,dvalue);
      }
    } else {
      for (i=0;i<maxind;i++) {
	if ((spind[i]>=0)&& (spind[i]<=nc)) {
	  sscanf(spres[spind[i]],"%lf",&dvalue);
	  printf(format,dvalue);
	}
      }
    }
    printf("\n");
    splitfree(spres,nc);
  }
}

int deletelines()
{
  LINESTRUCT *line,*next=(LINESTRUCT*) NULL;
  for (line = firstline; line != (LINESTRUCT *) NULL;) {
    next = line->next;
    free(line->line);
    free(line);
    line = next;
  }
  firstline=lastline=(LINESTRUCT*) NULL;
}
static char *resspace=(char*) NULL;

int split(str, arr, sep, ignws)
     char *str;   /* The string to be splited */
     char *arr[]; /* The array of separated strings - string space malloced here */
     char *sep;   /* The speparator string */
     int ignws;   /* Should we ignore whitespaces */
{
  char *s;
  int i;
  char *cp,*beg,*oldp,oldc;
  int len,onesp;
  char *rp;
  /* Get twice the length of current string to be sure to have the */
  /* necess. space */
  if ((rp=resspace = (char*) malloc(2*strlen(str)+2)) == (char*) NULL) {
    fprintf(stderr,"Can not allocate mem in split function\n");
    exit(1);
  }
  if (!sep || !*sep) 
    s = " ";
  else
    s = sep;
  len = strlen(s);
  onesp = (len == 1 && *s == ' ') || ignws;
  if (onesp)
    while (isspace(*str))
      str++;
  for (beg = str,i=0; *str ; str++) {
    if (*str != *s || (len > 1 && strncmp(str,s , len)))
      continue;
    oldp = str; oldc = *str; /*Save*/
    *str = 0; str += len; 
    if (onesp) {
      while (isspace(*str)) {
	str++;
      }
    }
    strcpy(arr[i++]=rp,beg); rp+=strlen(beg)+1;
    beg = str;
    *oldp = oldc; /* Restore */
    if (!*str)
      break;
  }
  
  if (!*beg) {
    return i;
  }

  if (onesp) {
    for (cp=beg+strlen(beg);(cp>=beg) && (isspace(*cp));) cp--;
    oldp = ++cp; oldc= *cp;
    *cp='\0'; 
  } else
    oldp = (char*) NULL;
  if (*beg)
    strcpy(arr[i++]=rp,beg); rp+=strlen(beg)+1;
  if (oldp) 
    *oldp=oldc;
  
  return i;
}

int splitfree(arr,no)
char *arr[];
int no;
{
  if (resspace) free(resspace);
  resspace = (char*) NULL;
}

static	char   *letP = NULL;	/* remember next option char's location */
static	char	SW = '-';	/* switch character*/

/*
  Parse the command line options, System V style.
 
  Standard option syntax is:
 
    option ::= - [optLetter]* [argLetter space* argument]
 
  where
    - there is no space before any optLetter or argLetter.
    - opt/arg letters are alphabetic, not punctuation characters.
    - optLetters, if present, must be matched in optionS.
    - argLetters, if present, are found in optionS followed by ':'.
    - argument is any white-space delimited string.  Note that it
      can include the SW character.
    - upper and lower case letters are distinct.
 
  There may be multiple option clusters on a command line, each
  beginning with a SW, but all must appear before any non-option
  arguments (arguments not introduced by SW).  Opt/arg letters may
  be repeated: it is up to the caller to decide if that is an error.
 
  The character SW appearing alone as the last argument is an error.
  The lead-in sequence SWSW ("--" ) causes itself and all the
  rest of the line to be ignored (allowing non-options which begin
  with the switch char).

  The string *optionS allows valid opt/arg letters to be recognized.
  argLetters are followed with ':'.  Anaopt () returns the value of
  the option character found, or EOF if no more options are in the
  command line.	 If option is an argLetter then the global optarg is
  set to point to the argument string (having skipped any white-space).
 
  The global optind is initially 1 and is always left as the index
  of the next argument of argv[] which anaopt has not taken.  Note
  that if "--" or "//" are used then optind is stepped to the next
  argument before anaopt() returns EOF.
 
  If an error occurs, that is an SW char precedes an unknown letter,
  then anaopt() will return a '?' character and normally prints an
  error message via perror().  If the global variable opterr is set
  to false (zero) before calling anaopt() then the error message is
  not printed.
 
  For example, 
 
    *optionS == "A:F:PuU:wXZ:"
 
  then 'P', 'u', 'w', and 'X' are option letters and 'F', 'U', 'Z'
  are followed by arguments.  A valid command line may be:
 
    aCommand  -uPFPi -X -A L someFile
 
  where:
    - 'u' and 'P' will be returned as isolated option letters.
    - 'F' will return with "Pi" as its argument string.
    - 'X' is an isolated option.
    - 'A' will return with "L" as its argument.
    - "someFile" is not an option, and terminates anaopt.  The
      caller may collect remaining arguments using argv pointers.
*/

int	anaopt(int argc, char *argv[], char *optionS, ANAOPTSTRUCT *pgos)
{
	unsigned char ch;
	char *optP;
 
	if (argc > pgos->optind) {
		if (letP == NULL) {
			if ((letP = argv[pgos->optind]) == NULL || 
				*(letP++) != SW)  goto gopEOF;
			if (*letP == SW) {
				(pgos->optind)++;  goto gopEOF;
			}
		}
		if (0 == (ch = *(letP++))) {
			pgos->optind++;  goto gopEOF;
		}
		if (':' == ch  ||  (optP = strchr(optionS, ch)) == NULL)  
			goto gopError;
		if (':' == *(++optP)) {
			pgos->optind++;
			if (0 == *letP) {
				if (argc <= pgos->optind)  goto  gopError;
				letP = argv[pgos->optind++];
			}
			pgos->optarg = letP;
			letP = NULL;
		} else {
			if (0 == *letP) {
				(pgos->optind)++;
				letP = NULL;
			}
			pgos->optarg = NULL;
		}
		return ch;
	}
gopEOF:
	pgos->optarg = letP = NULL;  
	return EOF;
 
gopError:
	pgos->optarg = NULL;
	if (pgos->opterr)
		perror ("get command line option");
	return ('?');
}

int readarg (int argc, char * argv[], ARGSTRUCT *as)

{
  int option;
  
  while ((option = anaopt(argc,argv,"o:c:f:t:wasej",&gos)) != EOF) {
    switch (option) { /*Put flag option with parameter in memory*/
    case 'w': /* User want to see a wa */
      as->flag &= !WFLAG&!EFLAG&!SFLAG&!AFLAG;
      as->flag |= WFLAG;
      break;
    case 'e': /* extract */
      as->flag &= !WFLAG&!EFLAG&!SFLAG&!AFLAG;
      as->flag |= EFLAG;
      break;
    case 's': /* User wants SCANNO */
      as->flag &= !WFLAG&!EFLAG&!SFLAG&!AFLAG;
      as->flag |= SFLAG;
      break;
    case 'a': /* User wants all */
      as->flag &= !WFLAG&!EFLAG&!SFLAG&!AFLAG;
      as->flag |= AFLAG;
      break;
    case 'f': /* From scan number */
      sscanf(gos.optarg,"%d",&(as->from));
      if (as->to == 100000)
	as->to=as->from;
      break;
    case 't': /* From scan number */
      sscanf(gos.optarg,"%d",&(as->to));
      break;
    case 'c': /* Columns - comma separated numbers or all */
      strcpy(as->columns,gos.optarg);
      break;
    case 'o': /* output format */
      strcpy(as->format,gos.optarg);
      break;
    case 'j': /* Jump - insert tabs */
      as->flag |= TFLAG;
      break;
    default:  
      return (TRUE);
    } /* End switch option */
  } /* End while option not equal EOF */
  if (gos.optind <= argc) 
    strcpy(as->filen,argv[gos.optind]);
  else
    return(TRUE);
  return (FALSE);
} /* end function readargs */





