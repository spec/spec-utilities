#include <stdio.h>

#define N_MOT   1024

struct  sav_mot {
	int     sm_pos;         /* Current position */
	float   sm_off;         /* Current user/dial offset */
	double  sm_low;         /* Software low limit */
	double  sm_high;        /* Software high limit */
};

struct  motor_stuff {
	struct  sav_mot sav_mot;
	char    cntl[132];
	double  steps;
	int     sign;
	int     slew;
	int     base;
	int     lash;
	int     accel;
	int     corr;
	int     flag;
	char    mne[132];
	char    name[132];
} ms[N_MOT];


char    filename[128];
char    *direc = ".";

do_usage() {
	fprintf(stderr, "Usage:  set2asc [settings_directory]\n");
	exit(1);
}

main(argc, argv)
char    **argv;
{
	register char   *p;
	int     i, num, n, fd, mot;
	struct  sav_mot *s, sav_mot;

	while (--argc) {
		p = *++argv;
		if (*p == '-') {
			if (!p[1])
				do_usage();
			while (*++p) switch(*p) {
				default:
					do_usage();
					break;
			}
		} else {
			argc--;
			direc = p;
			break;
		}
	}
	if (argc)
		do_usage();
	sprintf(filename, "%s/settings", direc);
	if ((fd = open(filename, 0)) < 0) {
		perror("Can't open settings file");
		exit(1);
	}
	for (i = 0; i < N_MOT; i++) {
		s = &ms[i].sav_mot;
		n = read(fd, s, sizeof(*s));
		if (n == 0)
			break;
		if (n < 0 || n != sizeof(*s)) {
			perror("Error reading settings files");
			exit(1);
		}
		printf("%3d %10d %16.8g %16.10g %16.10g\n", i, s->sm_pos,
			s->sm_off, s->sm_low, s->sm_high);
	}
	close(fd);
	exit(0);
}
