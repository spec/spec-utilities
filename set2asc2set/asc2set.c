#include <stdio.h>

#define N_MOT   1024

struct  sav_mot {
	int     sm_pos;         /* Current position */
	float   sm_off;         /* Current user/dial offset */
	double  sm_low;         /* Software low limit */
	double  sm_high;        /* Software high limit */
};

struct  motor_stuff {
	struct  sav_mot sav_mot;
	char    cntl[132];
	double  steps;
	int     sign;
	int     slew;
	int     base;
	int     lash;
	int     accel;
	int     corr;
	int     flag;
	char    mne[132];
	char    name[132];
} ms[N_MOT];


char    filename[128];
char    *direc = ".";

do_usage() {
	fprintf(stderr, "Usage:  asc2set [settings_directory]\n");
	exit(1);
}

main(argc, argv)
char    **argv;
{
	register char   *p;
	int     i, num, n, fd, mot;
	struct  sav_mot *s, sav_mot;
	char    buf[1024];

	while (--argc) {
		p = *++argv;
		if (*p == '-') {
			if (!p[1])
				do_usage();
			while (*++p) switch(*p) {
				default:
					do_usage();
					break;
			}
		} else {
			argc--;
			direc = p;
			break;
		}
	}
	if (argc)
		do_usage();
	sprintf(filename, "%s/settings", direc);
	close(creat(filename, 0666));
	if ((fd = open(filename, 1)) < 0) {
		perror("Can't open settings file");
		exit(1);
	}
	num = 0;
	while (fgets(buf, sizeof(buf), stdin)) {
		s = &ms[num++].sav_mot;
		if (sscanf(buf, "%*d %d %f %lf %lf", &s->sm_pos, &s->sm_off,
						&s->sm_low, &s->sm_high) != 4)
		break;

	}
	for (i = 0; i < num; i++) {
		n = write(fd, &ms[i].sav_mot, sizeof(struct sav_mot));
		if (n < 0 || n != sizeof(struct sav_mot)) {
			perror("Error writing settings files");
			exit(1);
		}
	}
	close(fd);
	exit(0);
}
